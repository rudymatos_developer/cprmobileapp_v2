//
//  ProductOptionCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/3/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol DisplayOptionsDetailsDelegate:class{
    func goToSelectedItem(option: Section.Option)
}

class ProductWithOptionCell: UITableViewCell {

    weak var delegate : DisplayOptionsDetailsDelegate?
    private var options = [Section.Option]()
    @IBOutlet weak var productWithOptionsCV: UICollectionView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        registerCell()
        productWithOptionsCV?.isHidden = true
    }
    
    func set(options: [Section.Option]){
        self.options = options
        DispatchQueue.main.async {
            self.productWithOptionsCV?.isHidden = false
            self.productWithOptionsCV?.reloadData()
        }
    }
    
    private func registerCell(){
        productWithOptionsCV?.register(UINib(nibName: "ItemDescriptionCell", bundle: Bundle.main), forCellWithReuseIdentifier: CPRConstants.CustomGOCellIdentifiers.itemDescriptionCell)
    }
}

extension ProductWithOptionCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedOption = options[indexPath.item]
        delegate?.goToSelectedItem(option: selectedOption)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 140, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let selectedOption = options[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CPRConstants.CustomGOCellIdentifiers.itemDescriptionCell, for: indexPath) as! ItemDescriptionCell
        cell.optionIV.image = nil
        cell.option = selectedOption
        return cell
    }
    
}

