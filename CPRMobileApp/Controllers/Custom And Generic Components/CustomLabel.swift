//
//  UILabel+Extension.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/12/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class CustomLabel: UILabel{
    @IBInspectable
    var characterSpacing: CGFloat = 1.0{
        didSet{
            let attributtedString = NSMutableAttributedString(string: self.text!)
            attributtedString.addAttributes([.kern: characterSpacing], range: NSRange(location: 0, length: attributtedString.length - 1))
            attributedText = attributtedString
        }
    }
}
