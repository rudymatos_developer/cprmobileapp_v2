//
//  BindiableTextView.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/1/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class CPRTextView: UITextView {
    
    @IBOutlet var nextField: UIView?
    
    func isEmpty() -> Bool {
        return text == nil || text?.trimmingCharacters(in: CharacterSet.whitespaces) == ""
    }
    
    func getText() -> String?{
        guard !isEmpty() else {return nil}
        return text
    }
    
    
}
