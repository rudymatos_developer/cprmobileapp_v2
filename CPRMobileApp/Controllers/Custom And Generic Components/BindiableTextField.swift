//
//  BindiableTextField.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class BindiableTextField: UITextField {

    var callback: (String) -> Void = { _ in }
    @IBOutlet var nextField: UIView?
    
    func isEmpty() -> Bool {
        return text == nil || text?.trimmingCharacters(in: CharacterSet.whitespaces) == ""
    }
    
    func bind(callback: @escaping (String)->Void) {
        self.callback = callback
        addTarget(self, action: #selector(BindiableTextField.setNewText(_:)), for: UIControl.Event.editingChanged)
        addTarget(self, action: #selector(BindiableTextField.setNewText(_:)), for: UIControl.Event.editingDidEnd)
    }
    
    @objc func setNewText(_ textField: UITextField) {
        if let text = textField.text {
            self.callback(text)
        }
    }
}
