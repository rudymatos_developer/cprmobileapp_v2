//
//  BindiableButton.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/31/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class BindiableButton: UIButton {

    var callback : () -> Void = {  }
    
    func bind(_ callback : @escaping () -> Void) {
        self.callback = callback
        addTarget(self, action: #selector(BindiableButton.fireAction), for: UIControl.Event.touchUpInside)
    }
    
    @objc func fireAction() {
        callback()
    }
    
}
