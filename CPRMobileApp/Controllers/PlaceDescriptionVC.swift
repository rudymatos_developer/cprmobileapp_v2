//
//  PlaceDescriptionVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class PlaceDescriptionVC: UIViewController, ViewLoadingType, StoryboardInitializer {
    
    @IBOutlet weak var placeIV: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var loadingScreen : LoadingScreen!
    
    var viewModel: PlaceDescriptionVM!
    private var headerView = UIView()
    var placeMainImage: UIImage?
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configureView()
    }
    
    private func registerCell(){
        tableView.register(UINib(nibName: "ViewContainer", bundle: Bundle.main), forCellReuseIdentifier: "ViewContainer")
        tableView.register(UINib(nibName: "PlaceNameCell", bundle: Bundle.main), forCellReuseIdentifier: "PlaceNameCell")
        tableView.register(UINib(nibName: "LikeAndSaveForLaterActionsCell", bundle: Bundle.main), forCellReuseIdentifier: "LikeAndSaveForLaterActionsCell")
    }
    
    @objc private func clickingOnImage() {
        displayImage()
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func configureHeaderView() {
        headerView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: placeIV.frame.height - 200)
        headerView.backgroundColor = .clear
        let tap = UITapGestureRecognizer(target: self, action: #selector(PlaceDescriptionVC.clickingOnImage))
        headerView.addGestureRecognizer(tap)
    }
    
    private func configureView(){
        showLoadingScreen()
        viewModel.getPlaceAdditionalInformation{ [weak self] result in
            self?.removeLoadingScreen()
            switch result{
            case .success(let data):
                DispatchQueue.main.async {
                    self?.configureHeaderView()
                    self?.tableView.reloadData()
                    guard let data = data, let image = UIImage(data: data) else {return}
                    self?.placeIV.image = image
                    self?.placeMainImage = image
                }
            case .error(let error):
                //Default image is loaded by default
                print(error)
            }
        }
    }
}

extension PlaceDescriptionVC: DocumentIterable{
    func displayImage() {
         showLoadingScreen()
        guard let currentImage = placeMainImage, let path = saveTempFileAndGetURL(image: currentImage) else {removeLoadingScreen();return}
        let documentIteractor = UIDocumentInteractionController()
        documentIteractor.delegate = self
        documentIteractor.url = path
        documentIteractor.uti = path.typeIdentifier
        documentIteractor.name = path.localizedName
        removeLoadingScreen()
        documentIteractor.presentPreview(animated: true)
    }
}

extension PlaceDescriptionVC: UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        if let nav = self.navigationController{
            return nav
        }
        return self
    }
}

extension PlaceDescriptionVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOrRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeight(forRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerView.frame.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceNameCell", for: indexPath) as? PlaceNameCell else{
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.set(placeName: viewModel.getPlaceName())
            return cell
        }else if indexPath.row == 1{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LikeAndSaveForLaterActionsCell", for: indexPath) as? LikeAndSaveForLaterActionsCell else{
                return UITableViewCell()
            }
            cell.favoriteAction = viewModel.getFavoriteAction()
            cell.saveForLaterAction = viewModel.getSaveForLaterAction()
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ViewContainer", for: indexPath) as? ViewContainer else{
                return UITableViewCell()
            }
            let view = viewModel.getView(forRow: indexPath.row)
            viewModel.setDelegate(view: view, vc: self)
            cell.set(titleAndDescription: viewModel.getTitleAndDescription(forRow: indexPath.row))
            if let imageName = viewModel.getImage(forRow: indexPath.row){
                cell.set(imageName: imageName)
            }
            cell.set(view: view)
            return cell
        }
    }
}

extension PlaceDescriptionVC: LikeAndSaveForLaterActionsCellDelegate{
    func toFavorite(action: LASFLAction) {
        viewModel.to(collection: .favoritePlaces, withAction: action)
    }
    func toSaveForLater(action: LASFLAction) {
        viewModel.to(collection: .saveForLaterPlaces, withAction: action)
    }
}

extension PlaceDescriptionVC: SingleActionDelegate{
    func execute(object: SingleActionObject) {
        viewModel.createNewReview()
    }
}

extension PlaceDescriptionVC: ReviewsViewerDelegate{
    func viewDetails(fromReview review: CPRReview) {
        viewModel.displayReviewDetails(review: review)
    }
}

extension PlaceDescriptionVC: PlaceContactInfoDelegate{
    
    func contactPlace() {
        guard let url = viewModel.getPlacePhoneNumber() else {
            displaySimpleAlertMessage(title: "Phone Number Unavailable", message: "Data for this place does not contain Phone Number")
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func goToWebsite() {
        guard let url = viewModel.getPlaceWebsite() else {
            displaySimpleAlertMessage(title: "Website Unavailable", message: "Data for this place does not contain Website")
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}

