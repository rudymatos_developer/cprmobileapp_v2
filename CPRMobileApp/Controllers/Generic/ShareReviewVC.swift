//
//  ShareReviewVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 9/19/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ShareReviewVC: UIViewController, ViewLoadingType, StoryboardInitializer {
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var placeLBL: UILabel!
    @IBOutlet weak var addressLBL: UILabel!
    @IBOutlet weak var ratingLBL: UILabel!
    
    var loadingScreen: LoadingScreen!
    var viewModel: ShareReviewVM!
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.2) {
            self.blurView.alpha = 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerCells()
    }
    
    private func configureView(){
        placeLBL.text = viewModel.getReviewPlace()
        addressLBL.text = viewModel.getReviewAddress()
        ratingLBL.text = viewModel.getReviewRating()
    }
    
    @IBAction func shareReview(_ sender: UIButton) {
        showLoadingScreen()
        viewModel.shareReview { [weak self] in
            self?.removeLoadingScreen()
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func doNotReview(_ sender: UIButton) {
        viewModel.cancelSharing { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
}

extension ShareReviewVC: UITableViewDelegate, UITableViewDataSource{
    
    private func registerCells(){
        tableView.register(UINib(nibName: SocialInfoCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: SocialInfoCell.identifier)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80 
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SocialInfoCell", for: indexPath) as! SocialInfoCell
        if let socialProfile = viewModel.getSocialProfile(byIndex: indexPath.row){
            cell.socialProfile =  socialProfile
            cell.delegate = self
        }
        return cell
    }
}


extension ShareReviewVC: SocialInfoCellDelegate{
    func loginTo(socialProfile: SocialProfile) {
        viewModel.login(socialNetwork: socialProfile.socialNetwork) { [weak self] result in
            switch result{
            case .success:
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            case .error(let error):
                self?.displaySimpleAlertMessage(title: "Error login to \(socialProfile.socialNetwork.rawValue.capitalized)", message: error.message)
            }
        }
    }
}
