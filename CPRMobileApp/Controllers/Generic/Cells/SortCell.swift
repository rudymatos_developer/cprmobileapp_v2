//
//  SortCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/17/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class SortCell: UITableViewCell{
    
    @IBOutlet weak var displayLBL: UILabel!
    @IBOutlet weak var selectedIV: UIImageView!

    func configureCell(with sorter: SortConfiguration.Sorter){
        displayLBL.text = sorter.displayName
        selectedIV.image = sorter.selected ? UIImage(named: "option_selector_checked") : nil
    }
    
}
