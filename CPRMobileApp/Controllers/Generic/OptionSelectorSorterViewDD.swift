//
//  OptionSelectorSorter.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/17/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class OptionSelectorSorterViewDD: NSObject{
    private var sortConfiguration: SortConfiguration
    init(sortConfiguration: SortConfiguration){
        self.sortConfiguration = sortConfiguration
    }
}

extension OptionSelectorSorterViewDD: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortConfiguration.sorters.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        sortConfiguration.swapSelected(newIndex: indexPath.row)
        DispatchQueue.main.async {
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectedSorter = sortConfiguration.sorters[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "sortCell", for: indexPath) as! SortCell
        cell.selectionStyle = .none
        cell.configureCell(with: selectedSorter)
        return cell
    }
    
}
