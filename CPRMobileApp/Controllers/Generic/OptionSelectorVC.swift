//
//  OptionSelectorVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 9/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol SelectionOptionDelegate: class{
    func select(option: OptionSelectorConfiguration.Option)
}

class OptionSelectorVC: UIViewController, CardCreator, StoryboardInitializer{

    @IBOutlet weak var loadingVEV: UIVisualEffectView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var resultsCountLBL: UILabel!
    @IBOutlet weak var sortingView: UIView!
    
    @IBOutlet weak var resultsTV: UITableView!
    @IBOutlet weak var sortTV: UITableView!
    private var sorterDelegate : OptionSelectorSorterViewDD!
    
    var viewModel: OptionSelectorVM!
    
    deinit{
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        navigationController?.dismiss(animated: true, completion: nil)
        dismissCompletion?()
    }
    
    var dismissCompletion: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        configureView()
        viewModel.executeRequest()
    }
    
    @IBAction func doneSorting(_ sender: UIButton) {
        viewModel.sortData()
        UIView.animate(withDuration: 0.5, animations: {
            self.sortingView.transform = CGAffineTransform(translationX: 0, y: self.sortingView.frame.height)
        }) { _ in
            self.sortingView.alpha = 0
            self.sortingView.transform = CGAffineTransform.identity
        }
    }
    
    @IBAction func showSorting(_ sender: UIButton) {
        sortingView.transform = CGAffineTransform(translationX: 0, y: sortingView.frame.height)
        sortingView.alpha = 1
        UIView.animate(withDuration: 0.5) {
            self.sortingView.transform = CGAffineTransform.identity
        }
    }
    
}

extension OptionSelectorVC{
    
    private func configureView(){
        titleLBL.text = viewModel.getTitle()
        descriptionLBL.text = viewModel.getDescription()
        resultsCountLBL.text = viewModel.getResultsCounter()
        searchIcon.image = UIImage(named: viewModel.getIcon())
        addBorder(toView: cardView, width: 0.2)
        createCard(shadowView: shadowView, mainView: cardView, cornerRadius: 10, offSet: CGSize(width: 3, height: -5))
        sortingView.alpha = 0
        viewModel.dataLoadCompleted = { [weak self] in
            DispatchQueue.main.async {
                self?.resultsCountLBL.text = self?.viewModel.getResultsCounter()
                self?.setDelegatesAndDataSources()
                UIView.animate(withDuration: 1.0, animations: {
                    self?.loadingVEV.alpha = 0
                })
            }
        }
        viewModel.sortDataCompleted = { [weak self] in
            DispatchQueue.main.async {
                self?.searchBar.text = ""
                self?.reloadTables()
            }
        }
    }
    
    private func setDelegatesAndDataSources(){
        resultsTV.delegate = self
        resultsTV.dataSource = self
        sorterDelegate = viewModel.getOptionSelectorSorter()
        sortTV.delegate = sorterDelegate
        sortTV.dataSource = sorterDelegate
        reloadTables()
    }
    
    private func reloadTables(){
        resultsTV.reloadData()
        sortTV.reloadData()
    }
    
}

extension OptionSelectorVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filterBy(text: searchText)
        resultsCountLBL.text = viewModel.getResultsCounter()
        DispatchQueue.main.async {
            self.resultsTV.reloadData()
        }
    }
}

extension OptionSelectorVC: UITableViewDelegate, UITableViewDataSource{
    
    private func registerCells(){
        resultsTV.register(UINib(nibName: "ReviewCell", bundle: Bundle.main), forCellReuseIdentifier: "selectionOptionCell")
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getCount()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectOptionOn(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let selectedOption = viewModel.getOptionBy(index : indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectionOptionCell", for: indexPath) as! ReviewCell
        if indexPath.row % 2 == 0{
            cell.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        }
        cell.configureView(selectionOption: selectedOption)
        return cell
    }
    
}
