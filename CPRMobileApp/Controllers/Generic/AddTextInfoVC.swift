//
//  AddDisplayNameToPlace.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/17/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

enum TextSetterValidator {
    case email
    case website
    case none
}

struct TextSetterUIProperties {
    var title: String
    var description: String
    var placeHolder: String
    var validator: TextSetterValidator
    var shouldBeAbleToCancel: Bool
}

protocol TextSetter: class {
    func processTextValue(_ text: String)
}

class AddTextInfoVC: UIViewController, CardCreator, CPRDataValidator,StoryboardInitializer {
    
    @IBOutlet weak var normalViewHeight: NSLayoutConstraint!
    @IBOutlet weak var viewHeightWithError: NSLayoutConstraint!
    @IBOutlet weak var valueView: UIView!
    @IBOutlet weak var dismissBTN: UIButton!
    @IBOutlet weak var saveBTNView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var saveBTN: UIButton!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var errorMessageLBL: UILabel!
    @IBOutlet weak var valueTF: UITextField!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    weak var delegate: TextSetter?
    var textSetterUIProperties: TextSetterUIProperties?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    deinit{
         print("💀 im out from \(self.classForCoder.description())")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 1) {
            self.blurView.alpha = 1
        }
    }
    
    private func configureView() {
        if let textSetterUIProperties = textSetterUIProperties {
            mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AddTextInfoVC.dismissKeyboard)))
            titleLBL.text = textSetterUIProperties.title
            descriptionLBL.text = textSetterUIProperties.description
            valueTF.placeholder = textSetterUIProperties.placeHolder
            applyCornerRadius(toView: saveBTNView, cornerRadius: 10)
            applyCornerRadius(toView: valueView, cornerRadius: 10)
            saveBTNView.backgroundColor = UIColor.disabledColor
            saveBTN.isEnabled = false
            if textSetterUIProperties.shouldBeAbleToCancel {
                dismissBTN.isEnabled = true
            }
        }
    }
    
    @objc private func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @IBAction func textEditingChanged(_ sender: UITextField) {
        guard !isEmpty(sender.text) else {
            saveBTNView.backgroundColor = UIColor.disabledColor
            saveBTN.isEnabled = false
            return
        }
        saveBTNView.backgroundColor = UIColor.cprPinkColor
        saveBTN.isEnabled = true
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func showErrorMessage(message: String, comment: String){
        normalViewHeight.priority = UILayoutPriority(rawValue: 1)
        viewHeightWithError.priority = UILayoutPriority(rawValue: 999)
        UIView.animate(withDuration: 0.2) {
            self.errorMessageLBL.isHidden = false
            self.errorMessageLBL.text = NSLocalizedString(message, comment: comment)
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func saveDisplayName(_ sender: UIButton) {
        guard let text = valueTF.text, !isEmpty(valueTF.text) else {
            showErrorMessage(message: "Invalid String", comment: "Invalid String")
            return
        }
        if let tsp = textSetterUIProperties, tsp.validator != .none {
            switch tsp.validator {
            case .email:
                guard isValidEmail(email: text) else {
                    showErrorMessage(message: "Invalid Email Address", comment: "Invalid Email Address")
                    return
                }
            case .website:
                guard isValidWebSite(website: text) else {
                    showErrorMessage(message: "Invalid WebSite (URL Format)", comment: "Invalid WebSite")
                    return
                }
            default:
                print("do nothing")
            }
        }
        self.dismiss(animated: true) {
            self.delegate?.processTextValue(text)
        }
    }
}
