//
//  AdditionalUserInformationVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class AdditionalUserInformationVC: UIViewController,CPRDataFormatter, CPRDataValidator, ViewLoadingType, CardCreator, StoryboardInitializer {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var displayNameTF: BindiableTextField!
    @IBOutlet weak var birthdayTF: BindiableTextField!
    @IBOutlet weak var countryCodeTF: BindiableTextField!
    @IBOutlet weak var mobileNumberTF: BindiableTextField!
    @IBOutlet weak var genderTF: BindiableTextField!
    @IBOutlet weak var maritalStatusTF: BindiableTextField!
    var loadingScreen: LoadingScreen!
    
    private var currentTextField: BindiableTextField?
    var viewModel: AdditionalUserInformationVM!
    
    private let datePickerKeyboard = DatePickerKeyboard(frame: CGRect(x: 0, y: 0, width: 0, height: 250))
    private let countryCodePicker = OptionPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 250))
    private let genderPicker = OptionPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 250))
    private let maritalStatusPicker = OptionPickerView(frame: CGRect(x: 0, y: 0, width: 0, height: 250))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerBindings()
        configureView()
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    @IBAction func updateProfile(_ sender: UIButton) {
        displayConfirmationDialog(title: "Updating Data", message: "Do you really want to update your profile with this data?") { [weak self] _ in
            self?.viewModel.updateProfile()
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion:nil)
    }
    
    private func registerBindings(){
        
        viewModel.displayName.bindAndFire { [weak self] in
            self?.displayNameTF.text = $0
        }
        viewModel.birthday.bindAndFire { [weak self] in
            self?.birthdayTF.text = $0
        }
        viewModel.countryCode.bindAndFire { [weak self] in
            self?.countryCodeTF.text = $0
        }
        
        viewModel.mobileNumber.bindAndFire { [weak self] in
            self?.mobileNumberTF.text = $0
        }
        
        viewModel.gender.bindAndFire { [weak self] in
            self?.genderTF.text = $0
        }
        
        viewModel.maritalStatus.bindAndFire { [weak self] in
            self?.maritalStatusTF.text = $0
        }
        
        displayNameTF.bind{ [weak self] in
            self?.viewModel.displayName.value = $0
        }
        
        birthdayTF.bind{ [weak self] in
            self?.viewModel.birthday.value = $0
        }
        
        countryCodeTF.bind{ [weak self] in
            self?.viewModel.countryCode.value = $0
        }
        
        mobileNumberTF.bind{ [weak self] in
            self?.viewModel.mobileNumber.value = $0
        }
        
        genderTF.bind{ [weak self] in
            self?.viewModel.gender.value = $0
        }
        
        maritalStatusTF.bind{ [weak self] in
            self?.viewModel.maritalStatus.value = $0
        }
    }

    private func configureView(){
        birthdayTF.inputView = datePickerKeyboard
        countryCodePicker.configurator = OptionPickerConfigurator(type: .countryCode)
        genderPicker.configurator = OptionPickerConfigurator(type: .gender)
        maritalStatusPicker.configurator = OptionPickerConfigurator(type: .maritalStatus)
        countryCodeTF.inputView = countryCodePicker
        genderTF.inputView = genderPicker
        maritalStatusTF.inputView = maritalStatusPicker
        countryCodePicker.delegate = self
        datePickerKeyboard.delegate = self
        genderPicker.delegate = self
        maritalStatusPicker.delegate = self
        createCard(shadowView: shadowView, mainView: cardView)
    }

}

extension AdditionalUserInformationVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField as? BindiableTextField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let tf = textField as? BindiableTextField {
            if tf.tag == 1, !isEmpty(tf.text), let currentText = tf.text {
                tf.text = currentText
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let tf = textField as? BindiableTextField {
            if tf.returnKeyType == .next {
                tf.resignFirstResponder()
                tf.nextField?.becomeFirstResponder()
            } else if tf.returnKeyType == .done {
                currentTextField?.resignFirstResponder()
            }
        }
        return true
    }
}

extension AdditionalUserInformationVC: OptionPickerViewDelegate {
    func assign(value: String, forType: OptionPickerConfigurator.OptionPickerViewType) {
        currentTextField?.text = value
        currentTextField?.resignFirstResponder()
        currentTextField?.nextField?.becomeFirstResponder()
    }
    
    func canceSelection(type: OptionPickerConfigurator.OptionPickerViewType) {
        currentTextField?.resignFirstResponder()
    }
    
    func update(withValue: String?, forType: OptionPickerConfigurator.OptionPickerViewType) {
        if forType == .countryCode{
            mobileNumberTF.text = withValue
        }
    }
}

extension AdditionalUserInformationVC: DatePickerKeyboardDelegate {
    func useDate() {
        birthdayTF.text = convert(date: datePickerKeyboard.datePickerView.date)
        currentTextField?.nextField?.becomeFirstResponder()
    }
    func cancel() {
        currentTextField?.resignFirstResponder()
    }
}

