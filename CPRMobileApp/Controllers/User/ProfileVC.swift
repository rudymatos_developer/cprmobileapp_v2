//
//  ProfileVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, ViewLoadingType, CardCreator, StoryboardInitializer {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profilePicIV: UIImageView!
    @IBOutlet weak var userDisplayNameLBL: UILabel!
    @IBOutlet weak var usernameLBL: UILabel!
    @IBOutlet weak var reviewCountLBL: UILabel!
    @IBOutlet weak var favoritePlacesCountLBL: UILabel!
    @IBOutlet weak var sflCountLBL: UILabel!
    @IBOutlet weak var userBadgeIV: UIImageView!
    @IBOutlet weak var badgeLBL: UILabel!
    private let imagePickerController = UIImagePickerController()
    
    var viewModel: ProfileVM!
    var loadingScreen: LoadingScreen!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerBindings()
        updateUserInfo()
    }
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    @IBAction func showFavorites(_ sender: UITapGestureRecognizer) {
        viewModel.showItemsBy(category: .myFavoritePlaces)
    }
    
    @IBAction func showSFLPlaces(_ sender: UITapGestureRecognizer) {
        viewModel.showItemsBy(category: .saveForLaterPlaces)
    }
    
    @IBAction func showReviews(_ sender: UITapGestureRecognizer) {
        viewModel.showItemsBy(category: .myReviews)
    }
    
    func configureView() {
        applyCircleCornerRadius(toView: profilePicIV)
        applyCornerRadius(toView: mainView, cornerRadius: 10)
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        profilePicIV.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileVC.updateProfilePicture)))
    }
    
    func updateUserInfo() {
        profilePicIV.loadImage(withURLString: viewModel.getProfilePictureURL(), orLoadDefaultImageName: CPRConstants.logoName)
        profilePicIV.backgroundColor = UIColor.clear
    }
    
    @IBAction func changeProfilePicture(_ sender: UIButton) {
        updateProfilePicture()
    }
    
    @IBAction func addAdditionalinformation(_ sender: UIButton) {
        viewModel.showAddAdditionalInformationVC()
    }
    
    
    @IBAction func logout(_ sender: UIButton) {
        displayConfirmationDialog(title: "Log Out", message: "Are you sure?", okTitle: "Yes", okCompletion: { [weak self] _ in
            self?.viewModel.logout()
        })
    }
}

extension ProfileVC{
    
    private func registerBindings() {
        viewModel.displayName.bindAndFire { [weak self] in
            self?.userDisplayNameLBL.text = $0
        }
        viewModel.email.bindAndFire { [weak self] in
            self?.usernameLBL.text = $0
        }
        viewModel.numberOfReviews.bindAndFire { [weak self] in
            self?.reviewCountLBL.text = "\($0)"
        }
        
        viewModel.favoritePlacesCount.bindAndFire { [weak self] in
            self?.favoritePlacesCountLBL.text = "\($0)"
        }
        
        viewModel.sflPlaceCount.bindAndFire { [weak self] in
            self?.sflCountLBL.text = "\($0)"
        }
        
        viewModel.badge.bindAndFire { [weak self] in
            self?.userBadgeIV.image = $0.getImage()
            self?.badgeLBL.text = $0.getTitle()
            self?.badgeLBL.textColor = $0.getFontColor()
        }
    }
    
}


extension ProfileVC{
    @objc private func updateProfilePicture() {
        let selectSource = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            } else {
                self.displaySimpleAlertMessage(title: "No Camera", message: "There is no camera available on this device")
            }
        }
        let cameraRoll = UIAlertAction(title: "Camera Roll", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        selectSource.addAction(takePhoto)
        selectSource.addAction(cameraRoll)
        selectSource.addAction(cancel)
        
        if let popOverController = selectSource.popoverPresentationController{
            popOverController.sourceView = userDisplayNameLBL
            popOverController.sourceRect = CGRect(x: userDisplayNameLBL.frame.midX, y: userDisplayNameLBL.frame.midY, width: 0, height: 0)
        }
        
        present(selectSource, animated: true, completion: nil)
    }
}

extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        dismiss(animated: true, completion: nil)
        showLoadingScreen()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            viewModel?.assignNewProfilePicture(withImage: editedImage) { [weak self] updatedUser, error in
                guard let updatedUser = updatedUser, error == nil else{
                    DispatchQueue.main.async {
                        self?.removeLoadingScreen()
                        self?.displaySimpleAlertMessage(title: "Error assigning picture", message: error!.message)
                    }
                    return
                }
                
                self?.viewModel.updateUserInfo(user: updatedUser)
                self?.profilePicIV.image = editedImage
                self?.profilePicIV.contentMode = .scaleToFill
                self?.profilePicIV.backgroundColor = UIColor.clear
                self?.removeLoadingScreen()
                
            }
        }
    }
    
}
