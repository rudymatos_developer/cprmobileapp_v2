//
//  ShopVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/3/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit
import GooglePlaces

class UserHomeVC: UIViewController,ViewLoadingType, StoryboardInitializer {
    
    var viewModel : UserHomeVM!
    
    @IBOutlet weak var tableView: UITableView!
    private var tableHeaderHeight = (UIScreen.main.bounds.height / 3) + 30
    private var tableHeader : MainHeader!
    
    var loadingScreen: LoadingScreen!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func loadView() {
        super.loadView()
        registerCells()
        configureView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotifications()
        showLoadingScreen()
        viewModel.loadItemCompletion = {[weak self] result in
            self?.removeLoadingScreen()
            switch result{
            case .success:
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            case .error(let error):
                DispatchQueue.main.async {
                    self?.displaySimpleAlertMessage(title: "Error loading Places", message: error.message)
                }
                return
            }
        }
        viewModel.getUserCurrentLocation()
    }
    
    private func createImageHeader(){
        tableHeader = MainHeader(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: tableHeaderHeight))
        tableHeader.setUserInfo(user: viewModel.getCurrentUser())
        tableHeader.delegate = self
    }
    
    private func configureView(){
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        createImageHeader()
    }
    
    private func registerCells(){
        tableView.register(UINib(nibName: "TitleCell", bundle: Bundle.main), forCellReuseIdentifier: CPRConstants.CustomGOCellIdentifiers.titleCell)
        tableView.register(UINib(nibName: "MainHeader", bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: CPRConstants.CustomGOCellIdentifiers.mainHeader)
    }
    
}

extension UserHomeVC{
    
    private func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(UserHomeVC.refreshUserReviews(_:)), name: .refreshUserReviews, object: nil)
    }
    
    @objc private func refreshUserReviews(_ notification: Notification){
        viewModel.loadData()
    }
    
}


extension UserHomeVC: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return section == 0 ? tableHeader : nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? tableHeaderHeight : 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetY = scrollView.contentOffset.y
        if contentOffsetY < 0{
            let height = tableHeader.frame.height - contentOffsetY
            tableHeader.contentView.frame = CGRect(x: 0, y: contentOffsetY, width: tableHeader.frame.width, height: height)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getNumberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.getHeight(forRow: indexPath.row))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let section = viewModel.getSection(byIndex: indexPath.section), (indexPath.row == viewModel.TITLE_INDEX || indexPath.row == viewModel.CONTENT_INDEX) else {return UITableViewCell()}
        switch indexPath.row {
        case viewModel.TITLE_INDEX:
            let cell = tableView.dequeueReusableCell(withIdentifier: CPRConstants.CustomGOCellIdentifiers.titleCell, for: indexPath) as! TitleCell
            cell.setCellInfo(section: section)
            cell.selectionStyle = .none
            cell.delegate = self
            return cell
        case viewModel.CONTENT_INDEX:
            let cell = tableView.dequeueReusableCell(withIdentifier: "productWithOptionsCell", for: indexPath) as! ProductWithOptionCell
            cell.set(options: section.options)
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        default:
            print("it should never get into this point")
            return UITableViewCell()
        }
    }
}

extension UserHomeVC: DisplayOptionsDetailsDelegate{
    func goToSelectedItem(option: Section.Option){
        viewModel.goToSelectedItem(option: option)
    }
}

extension UserHomeVC: MainHeaderDelegate{
    func showWhereToGoView() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    func goTo(category: MainCategory){
        viewModel.goToMainOption(selectionType: category.getOptionSelectorConfigurator())
    }
    
}

extension UserHomeVC: SeeAllDelegate{
    func seeAll(section: Section) {
        viewModel.goToSeeAll(selectionType: section.getOptionSelectorConfigurator())
    }
}


extension UserHomeVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        let placeToAdd = CPRPlace(place: place)
        viewModel.set(place: placeToAdd)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
