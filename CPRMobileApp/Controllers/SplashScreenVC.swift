//
//  SplashScreenVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/9/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class SplashScreenVC: UIViewController,StoryboardInitializer {
    
    @IBOutlet weak var border: UIImageView!
    @IBOutlet weak var clickLBL: UILabel!
    @IBOutlet weak var picLBL: UILabel!
    @IBOutlet weak var reviewLBL: UILabel!
    @IBOutlet weak var clickIV: UIImageView!
    @IBOutlet weak var picIV: UIImageView!
    @IBOutlet weak var reviewIV: UIImageView!
    private var userIsLoggedIn = false
    
    var splashScreenAnimationCompletion: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        noAnimate()
        animateView()
    }
    
    deinit{
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    private func noAnimate(){
        border.alpha = 1
        clickIV.alpha = 1
        clickLBL.alpha = 1
        picIV.alpha = 1
        picLBL.alpha = 1
        reviewIV.alpha = 1
        reviewLBL.alpha = 1
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.splashScreenAnimationCompletion?()
        }
    }
    
    private func animateView() {
        border.alpha = 0.4
        clickIV.alpha = 0
        clickLBL.alpha = 0
        picIV.alpha = 0
        picLBL.alpha = 0
        reviewIV.alpha = 0
        reviewLBL.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: [UIView.AnimationOptions.repeat, UIView.AnimationOptions.autoreverse, UIView.AnimationOptions.curveEaseIn], animations: {
            self.border.alpha = 1
        }, completion: nil)
        UIView.animate(withDuration: 1, animations: {
            self.clickLBL.alpha = 1
            self.clickIV.alpha = 1
        }) { (completed) in
            UIView.animate(withDuration: 1, animations: {
                self.picIV.alpha = 1
                self.picLBL.alpha = 1
            }, completion: { (completed) in
                UIView.animate(withDuration: 1, animations: {
                    self.reviewIV.alpha = 1
                    self.reviewLBL.alpha = 1
                }, completion: { [weak self] (completed) in
                   self?.splashScreenAnimationCompletion?()
                })
            })
        }
    }
}
