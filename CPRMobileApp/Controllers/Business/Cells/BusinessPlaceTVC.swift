//
//  BusinessPlaceTVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol PlaceManagerDelegate: class {
    func removePlace(place: CPRPlace)
}

class BusinessPlaceTVC: UITableViewCell {
    
    @IBOutlet weak var placeLogo: UIImageView!
    @IBOutlet weak var placeAddress: UILabel!
    @IBOutlet weak var placeDisplayName: UILabel!
    
    var place: CPRPlace? {
        didSet {
            configureView()
        }
    }
    weak var delegate: PlaceManagerDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func deletePlace(_ sender: UIButton) {
        if let place = place {
            delegate?.removePlace(place: place)
        }
    }
    
    func configureView() {
        if let place = place {
            placeDisplayName.text = place.displayName
            placeAddress.text = place.address
            if let website = place.website, let url = URL(string: "https://logo.clearbit.com/\(website)") {
                let queue = DispatchQueue(label: "businessPicture", qos: DispatchQoS.userInitiated)
                queue.async {
                    URLSession.shared.dataTask(with: url, completionHandler: { [unowned self] (data, response, _) in
                        DispatchQueue.main.async {
                            if let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200 {
                                self.placeLogo.image = UIImage(data: data)
                                self.placeLogo.contentMode = .scaleToFill
                            } else {
                                self.placeLogo.image = UIImage(named: "logov2")
                                self.placeLogo.contentMode = .scaleAspectFit
                            }
                        }
                    }).resume()
                }
            }
        }
    }
}
