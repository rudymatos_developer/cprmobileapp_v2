//
//  BusinessReviewTVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class BusinessReviewTVC: UITableViewCell,CPRDataFormatter {
    
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var ratingImageIV: UIImageView!
    @IBOutlet weak var reviewDateLBL: UILabel!
    @IBOutlet weak var userDisplayNameLBL: UILabel!
    @IBOutlet weak var placeDisplayNameLBL: UILabel!
    @IBOutlet weak var userProfileImageIV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userProfileImageIV.layer.cornerRadius = userProfileImageIV.frame.height / 2
        userProfileImageIV.image = UIImage(named: "logov2")
        userProfileImageIV.contentMode = .scaleAspectFit
        userProfileImageIV.clipsToBounds = true
    }
    
    func configureView(review: CPRReview) {
//        if let review = review  as? CPRReview{
//            ratingLBL.text = review.getRatingText()
//            reviewDateLBL.text = convert(date: review.when, usingPattern: .full)
//            userDisplayNameLBL.text = review.userDisplayName
//            placeDisplayNameLBL.text = review.businessDisplayName
//            ratingImageIV.image = UIImage(named: "review_\(review.rating)_stars")
//            if let profilePic = review.userProfilePictureURL, let url = URL(string: profilePic) {
//                userProfileImageIV.loadImage(withURLString: review.userProfilePictureURL)
//                userProfileImageIV.contentMode = .scaleToFill
//            } else {
//                DispatchQueue.main.async {
//                    self.userProfileImageIV.image = UIImage(named: "logov2")
//                    self.userProfileImageIV.contentMode = .scaleAspectFit
//                }
//            }
//        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
