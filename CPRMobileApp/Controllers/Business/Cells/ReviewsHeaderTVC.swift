//
//  ReviewsHeaderTVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ReviewsHeaderTVC: UITableViewCell {

    @IBOutlet weak var baseLBL: UILabel!
    @IBOutlet weak var detailLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureView(title: (base: String, detail: String)) {
        baseLBL.text = title.base
        detailLBL.text = title.detail
    }
    
}
