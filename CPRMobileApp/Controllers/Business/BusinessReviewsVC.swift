//
//  BusinessReviewsVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class BusinessReviewsVC: UIViewController, ViewLoadingType, CardCreator {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var reviewsTV: UITableView!
    @IBOutlet weak var noReviewsMessage: UIImageView!
    @IBOutlet weak var filterBTN: UIButton!
    var loadingScreen: LoadingScreen!
    
    private var viewModel = ReviewListVM(reviews: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotifications()
        configureView()
    }
    
    func configureView() {
        self.navigationController?.navigationBar.isHidden = true
        self.view.setNeedsLayout()
        noReviewsMessage.alpha = 0
        filterBTN.alpha = 0
        reviewsTV.indexDisplayMode = .alwaysHidden
        applyCornerRadius(toView: mainView, cornerRadius: 10)
        loadBusinessReviews()
    }
    
    @objc func loadBusinessReviews() {
//        showLoadingScreen()
//        loadUserInformation { [weak self] (error) in
//            if let error = error {
//                self?.removeLoadingScreen()
//                self?.displaySimpleAlertMessage(title: "Error gettig user data", message: error.message)
//                return
//            }
////            if let cpruser = self?.appConfig.currentUser {
////                self?.viewModel = ReviewListViewModel(reviews: cpruser.businessReviews)
////                self?.registerBindings()
////            }
//            self?.removeLoadingScreen()
//            self?.loadReviewFromNotification()
//            DispatchQueue.main.async {
//                self?.reviewsTV.reloadData()
//                self?.displayComponentsBasedOnTotalReviews()
//                
//            }
//        }
    }
    
    private func loadReviewFromNotification() {
//        if let firebaseId = userDefaultsService.getFirebaseIDFromNotification(), let review = viewModel.getReview(withFirebaseId: firebaseId) {
//            viewModel.selectedReview = review
//            userDefaultsService.removeFirebaseIdFromNotification()
//            displayReviewInfo()
//        }
    }
    
    private func displayComponentsBasedOnTotalReviews() {
        UIView.animate(withDuration: 0.5) {
            self.mainView.alpha = self.viewModel.shouldShowReviewTable() ? 1 : 0
            self.filterBTN.alpha = self.viewModel.shouldShowFilterButton() ? 1 : 0
            self.noReviewsMessage.alpha = self.viewModel.shouldShowNoReviewsMessage() ? 1 : 0
        }
    }
    
    private func registerBindings() {
        self.viewModel.sortBy.bindAndFire { [unowned self] _ in
            DispatchQueue.main.async {
                self.reviewsTV.reloadData()
                self.displayComponentsBasedOnTotalReviews()
            }
        }
    }
    
    // MARK: - Notifications
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessReviewsVC.loadBusinessReviews), name: Notification.Name.addPlace, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessReviewsVC.loadBusinessReviews), name: Notification.Name.removePlace, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(Notification.Name.addPlace)
        NotificationCenter.default.removeObserver(Notification.Name.removePlace)
    }
    
    // MARK: - UI METHODS
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "filterReviewSegue", let vc = segue.destination as? SortReviewsVC {
            vc.delegate = self
            vc.viewModel.sortBy = viewModel.sortBy
        }
    }
    
    @IBAction func filterReviews(_ sender: UIButton) {
        performSegue(withIdentifier: "filterReviewSegue", sender: nil)
    }
    
    private func displayReviewInfo() {
//        if let vc = storyboard?.instantiateViewController(withIdentifier: "reviewDetail") as? ReviewDetailVC {
//            vc.myReview = viewModel.selectedReview
//            self.navigationController?.navigationBar.isHidden = false
//            navigationController?.pushViewController(vc, animated: true)
//        }
    }
}

extension BusinessReviewsVC: SortingDelegate {
    func sort(by: Sort) {
        viewModel.sortReviews(by: by)
    }
}

extension BusinessReviewsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedReview = viewModel.getReview(byIndex: indexPath.row, andSection: indexPath.section)
        tableView.deselectRow(at: indexPath, animated: true)
        displayReviewInfo()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let view = tableView.dequeueReusableCell(withIdentifier: "headerView") as? ReviewsHeaderTVC {
            let title = viewModel.getSectionTitle(byIndex: section)
            view.configureView(title: title)
            return view
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.totalOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalOfReviews(bySection: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "businessReview", for: indexPath) as? BusinessReviewTVC, let currentBusinesReviewViewModel = viewModel.getReview(byIndex: indexPath.row, andSection: indexPath.section) {
            cell.configureView(review: currentBusinesReviewViewModel)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
}
