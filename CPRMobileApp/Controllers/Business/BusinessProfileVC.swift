//
//  BusinessProfileVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/14/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class BusinessProfileVC: UIViewController, ViewLoadingType {
    
    @IBOutlet weak var businessProfileIV: UIImageView!
    @IBOutlet weak var businessDisplayNameLBL: UILabel!
    @IBOutlet weak var placesInBusinessLBL: UILabel!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var cprAVGLBL: UILabel!
    @IBOutlet weak var googleAVGLBL: UILabel!
    @IBOutlet weak var numberOfREviewsLBL: UILabel!
    
    private let imagePickerController = UIImagePickerController()
    var loadingScreen: LoadingScreen!
    private var viewModel: BusinessProfileViewModel = BusinessProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func loadProfilePicture() {
        showLoadingScreen()
//        if let currentUser = appConfig.currentUser, let imageName = currentUser.profilePicture, imageName != ""{
//            datable.downloadProfilePicture(byImageName: imageName, completion: { [unowned self] (image, error) in
//                if let error = error {
//                    self.removeLoadingScreen()
//                    self.displaySimpleAlertMessage(title: "Profile Picture Error", message: error.message)
//                    return
//                }
//                self.businessProfileIV.image = image
//                self.businessProfileIV.backgroundColor = UIColor.clear
//                self.removeLoadingScreen()
//            })
//        } else {
//            self.removeLoadingScreen()
//        }
    }
    
    private func configureView() {
        
        applyCornerRadius(toView: numberView, cornerRadius: 7)
        applyCornerRadius(toView: logoutView, cornerRadius: logoutView.frame.height / 2)
        applyCircleCornerRadius(toView: businessProfileIV)
        
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
        
        registerGesturesRecognizers()
        registerNotifications()
        
        loadProfilePicture()
        displayBusinessInfo()
        
    }
    
    private func registerGesturesRecognizers() {
        businessProfileIV.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BusinessProfileVC.changeProfilePicture)))
    }
    
    // MARK: - Notifications
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessProfileVC.reloadBusinessInfo(_:)), name: .addPlace, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BusinessProfileVC.reloadBusinessInfo(_:)), name: .removePlace, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(Notification.Name.addPlace)
        NotificationCenter.default.removeObserver(Notification.Name.removePlace)
    }
    
    @objc private func reloadBusinessInfo(_ notification: Notification) {
//        if let currentUser = appConfig.currentUser, let companyDisplayName = currentUser.companyDisplayName {
//            businessDisplayNameLBL.text = companyDisplayName
//            datable.getUserData(byUserKey: currentUser.email, completion: { [unowned self] (cpruser, _) in
////                if let cpruser = cpruser {
////                    self.numberOfREviewsLBL.text = "\(cpruser.businessReviews?.count ?? 0)"
////                    self.appConfig.currentUser = cpruser
////                }
//            })
//
//            if let userInfo = notification.userInfo, let places = userInfo["places"] as? [CPRPlace] {
//                self.viewModel.places = places
//                if let avgrs = self.viewModel.calculateCPRAndGoogleAVGRating() {
//                    self.cprAVGLBL.text = "\(avgrs.cprAVG)"
//                    self.googleAVGLBL.text = "\(avgrs.googleAVG)"
//                }
//                self.placesInBusinessLBL.text = self.viewModel.getTextForPlacesInBusiness()
//            }
//
//        }
    }
    
    private func displayBusinessInfo() {
//        if let cprUser = appConfig.currentUser, let companyDisplayName = cprUser.companyDisplayName {
//            self.businessDisplayNameLBL.text = companyDisplayName
////            self.numberOfREviewsLBL.text = "\(cprUser.businessReviews?.count ?? 0)"
//            datable.getAllPlaces(byBusinessOwner: cprUser.email, completion: { [unowned self] (places) in
//                self.viewModel.places = places
//                if let avgrs = self.viewModel.calculateCPRAndGoogleAVGRating() {
//                    self.cprAVGLBL.text = "\(avgrs.cprAVG)"
//                    self.googleAVGLBL.text = "\(avgrs.googleAVG)"
//                }
//                self.placesInBusinessLBL.text = self.viewModel.getTextForPlacesInBusiness()
//            })
//        }
    }
    
    @IBAction func changeProfilePicture(_ sender: UIButton) {
        let selectSource = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            } else {
                self.displaySimpleAlertMessage(title: "No Camera", message: "There is no camera available on this device")
            }
        }
        let cameraRoll = UIAlertAction(title: "Camera Roll", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        selectSource.addAction(takePhoto)
        selectSource.addAction(cameraRoll)
        selectSource.addAction(cancel)
        present(selectSource, animated: true, completion: nil)
    }
    
    @IBAction func logout(_ sender: UITapGestureRecognizer) {
//        displayConfirmationDialog(title: "Log Out", message: "Are you sure?", okTitle: "Yes", okCompletion: { [unowned self] (_) in
//            let user = self.appConfig.currentUser!
//            self.datable.removeNotificationToken(from: user, fcmToken: self.userDefaultsService.getDeviceFMCToken(), completion: { (error) in
//                if let error = error {
//                    print(error)
//                }
//                let storyBoard = UIStoryboard.init(name: StoryBoardIdentifiers.Welcome.displayName, bundle: nil)
//                let welcomeScreen = storyBoard.instantiateViewController(withIdentifier: StoryBoardIdentifiers.Welcome.VCs.welcome)
//                let profileViewModel = ProfileVM(user: self.appConfig.currentUser)
//                #warning("remember to fix this logout issue using the same implementation in LoginViewModel")
////                profileViewModel.logoutUser(appConfig: self.appConfig, authorizable: self.authorizable, userDefaults: self.userDefaultsService)
//                self.present(welcomeScreen, animated: true, completion: nil)
//            })
//        })
    }
}

extension BusinessProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        dismiss(animated: true, completion: nil)
        
        showLoadingScreen()
//        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
////            if let currentUser = appConfig.currentUser {
////                datable.assignNewProfileImage(email: currentUser.email, image: editedImage, completion: { (error) in
////                    if  let error = error {
////                        self.removeLoadingScreen()
////                        self.displaySimpleAlertMessage(title: "Error assigning picture", message: error.message)
////                    } else {
////                        self.datable.getUserData(byUserKey: currentUser.email, completion: { (cprUser, error) in
////                            if let error = error {
////                                self.removeLoadingScreen()
////                                self.displaySimpleAlertMessage(title: "Error getting updated data", message: error.message)
////                                return
////                            }
////                            self.appConfig.currentUser = cprUser
////                            self.businessProfileIV.image = editedImage
////                            self.businessProfileIV.contentMode = .scaleToFill
////                            self.businessProfileIV.backgroundColor = UIColor.clear
////                            self.removeLoadingScreen()
////                        })
////                    }
////                })
////            }
//        }
    }
    
}
