//
//  ManageBusinessPlacesVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit
import GooglePlaces

class ManageBusinessPlacesVC: UIViewController, ViewLoadingType, CardCreator {
    
    var loadingScreen: LoadingScreen!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var addNewPlaceView: UIView!
    @IBOutlet weak var placesTV: UITableView!
    private var googlePlaceObject: GMSPlace?
    private var places = [CPRPlace]()
    private var placeToAdd: CPRPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        applyCornerRadius(toView: mainView, cornerRadius: 10)
        applyCornerRadius(toView: addNewPlaceView, cornerRadius: addNewPlaceView.frame.height / 2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadAllPlacesData()
    }
    
    private func loadAllPlacesData() {
//        showLoadingScreen()
//        if let email = appConfig.currentUser?.email {
//            datable.getAllPlaces(byBusinessOwner: email) { [unowned self] (places) in
//                self.places = places
//                DispatchQueue.main.async {
//                    self.placesTV.reloadData()
//                    self.removeLoadingScreen()
//                }
//            }
//        }
    }
    
    @IBAction func addNewPlace(_ sender: UITapGestureRecognizer) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addPlaceDisplayName", let vc = segue.destination as? AddTextInfoVC {
            vc.delegate = self
            let tsp = TextSetterUIProperties(title: "Add Display Name for this Place", description: "Display name is used to identify more easly a place", placeHolder: "Enter a Display Name", validator: .none, shouldBeAbleToCancel: false)
            vc.textSetterUIProperties = tsp
        }
    }
}

extension ManageBusinessPlacesVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        placeToAdd = CPRPlace(place: place)
        dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "addPlaceDisplayName", sender: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension ManageBusinessPlacesVC: PlaceManagerDelegate {
    func removePlace(place: CPRPlace) {
        let alert = UIAlertController(title: "Removing Place", message: "Do you want to delete \(place.displayName)", preferredStyle: .alert)
        let delete = UIAlertAction(title: "Yes", style: .destructive) { [unowned self] (_) in
            self.showLoadingScreen()
//            if let user = self.appConfig.currentUser {
//                self.datable.removePlace(fromUser: user, placeToRemove: place) {  [unowned self] in
//                    self.datable.getAllPlaces(byBusinessOwner: user.email) { [unowned self] (places) in
//                        self.places = places
//                        let placesDictionary = ["places": places]
//                        NotificationCenter.default.post(name: .removePlace, object: nil, userInfo: placesDictionary)
//                        DispatchQueue.main.async {
//                            self.placesTV.reloadData()
//                            self.removeLoadingScreen()
//                        }
//                    }
//                }
//            }
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(delete)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
}

extension ManageBusinessPlacesVC: TextSetter {
    func processTextValue(_ text: String) {
//        if let placeToAdd = placeToAdd, let user = appConfig.currentUser {
//          placeToAdd.displayName = text
//            showLoadingScreen()
//            datable.addPlace(toUser: user, placeToAdd: placeToAdd) { [unowned self] (error) in
//                if let error = error {
//                    self.removeLoadingScreen()
//                    self.displaySimpleAlertMessage(title: "Error adding Place", message: error.message)
//                    return
//                }
//                self.datable.getAllPlaces(byBusinessOwner: user.email) { [unowned self] (places) in
//                    self.places = places
//                    let placesDictionary = ["places": places]
//                    NotificationCenter.default.post(name: .addPlace, object: nil, userInfo: placesDictionary)
//                    DispatchQueue.main.async {
//                        self.removeLoadingScreen()
//                        self.placesTV.reloadData()
//                    }
//                }
//            }
//        }
    }
}

extension ManageBusinessPlacesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "businessPlaceTVC", for: indexPath) as? BusinessPlaceTVC {
            cell.delegate = self
            cell.place = places[indexPath.row]
            cell.configureView()
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
}
