//
//  SortReviewsVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/15/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class SortReviewsVC: UIViewController, CardCreator {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var sortByDateView: UIView!
    @IBOutlet weak var sortByRatingView: UIView!
    @IBOutlet weak var sortByPlaceNameView: UIView!
    @IBOutlet weak var ascendingView: UIView!
    @IBOutlet weak var saveView: UIView!
    @IBOutlet weak var descendingView: UIView!
    
    @IBOutlet weak var sortByDateBTN: UIButton!
    @IBOutlet weak var sortByRatingBTN: UIButton!
    @IBOutlet weak var sortByPlaceNameBTN: UIButton!
    @IBOutlet weak var ascendingBTN: UIButton!
    @IBOutlet weak var descendingBTN: UIButton!
    
    var viewModel = SortReviewVM()
    weak var delegate: SortingDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        applyCornerRadius(toView: mainView, cornerRadius: 10)
        addBorder(toView: mainView, color: .gray, width: 0.5)
        applyCornerRadius(toView: sortByDateView, cornerRadius: 7)
        applyCornerRadius(toView: sortByRatingView, cornerRadius: 7)
        applyCornerRadius(toView: sortByPlaceNameView, cornerRadius: 7)
        applyCornerRadius(toView: ascendingView, cornerRadius: 7)
        applyCornerRadius(toView: descendingView, cornerRadius: 7)
        applyCornerRadius(toView: saveView, cornerRadius: 7)
        updateButtons()
    }
    
    private func updateButtons() {
        sortByDateBTN.setTitleColor(viewModel.isSortFieldEquals(to: .date) ? UIColor.cprPinkColor : UIColor.disabledColor, for: UIControl.State.normal)
        sortByRatingBTN.setTitleColor(viewModel.isSortFieldEquals(to: .rating) ? UIColor.cprPinkColor: UIColor.disabledColor, for: UIControl.State.normal)
        sortByPlaceNameBTN.setTitleColor(viewModel.isSortFieldEquals(to: .businessDisplayName) ? UIColor.cprPinkColor : UIColor.disabledColor, for: UIControl.State.normal)
        ascendingBTN.setTitleColor(viewModel.isSortOrderingEquals(to: .ascending) ? UIColor.cprPinkColor: UIColor.disabledColor, for: UIControl.State.normal)
        descendingBTN.setTitleColor(viewModel.isSortOrderingEquals(to: .descending) ? UIColor.cprPinkColor : UIColor.disabledColor, for: UIControl.State.normal)
    }
    
    @IBAction func sort(_ sender: UIButton) {
        if let label = sender.accessibilityLabel {
            if let sortField = SortField(rawValue: label) {
                viewModel.setSortField(to: sortField)
                updateButtons()
            } else if let sortOrdering = SortField.SortOrdering(rawValue: label) {
                viewModel.setSortOrdering(to: sortOrdering)
                updateButtons()
            }
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        delegate?.sort(by: viewModel.sortBy.value)
        dismiss(animated: true, completion: nil)
    }
    
}
