//
//  NewReview.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit
import GooglePlaces

class NewReviewVC: UIViewController, ViewLoadingType,CPRDataFormatter, StoryboardInitializer{
    
    var loadingScreen: LoadingScreen!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reviewIV: UIImageView!
    private let imagePickerController = UIImagePickerController()
    
    var viewModel: NewReviewVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("🏀 initializing \(self.classForCoder.description())")
        registerCell()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    func configureView() {
        imagePickerController.allowsEditing = true
        imagePickerController.delegate = self
    }
    
    private func registerCell(){
        tableView.register(UINib(nibName: "ViewContainer", bundle: Bundle.main), forCellReuseIdentifier: "ViewContainer")
        tableView.register(UINib(nibName: "PlaceNameCell", bundle: Bundle.main), forCellReuseIdentifier: "PlaceNameCell")
        tableView.register(UINib(nibName: "DateSelectorCell", bundle: Bundle.main), forCellReuseIdentifier: "DateSelectorCell")
        tableView.register(UINib(nibName: "SingleActionCell", bundle: Bundle.main), forCellReuseIdentifier: "SingleActionCell")
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension NewReviewVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.getSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows(forSection: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeight(forIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
        return reviewIV.frame.height - 200
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            let transparentView = UIView(frame: CGRect(x: 0, y: 0, width: reviewIV.frame.width, height: reviewIV.frame.height))
            transparentView.backgroundColor = .clear
            return transparentView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let newReviewSection = NewReviewSection(rawValue: indexPath.section) else {return UITableViewCell()}
        switch newReviewSection{
        case .place:
            if viewModel.doesReviewHasAPlaceAssigned(){
                switch indexPath.row {
                case 0:
                    return getPlaceNameCell(tableView: tableView, indexPath: indexPath)
                case 1..<3:
                    return getContainerCell(tableView:tableView, indexPath:indexPath)
                default:
                    return UITableViewCell()
                }
            }
            return getContainerCell(tableView:tableView, indexPath:indexPath)
        case .photos:
            return getContainerCell(tableView:tableView, indexPath:indexPath)
        case .additionalInfo:
            switch indexPath.row{
            case 0:
                guard let cell = getDateSelectorCell(tableView:tableView, indexPath: indexPath) as? DateSelectorCell else {return UITableViewCell()}
                cell.delegate = self
                return cell
            case 1..<4:
                let cell = getContainerCell(tableView:tableView, indexPath:indexPath)
                return cell
            default:
                return getSaveCell(tableView:tableView,indexPath:indexPath)
            }
        }
    }
}

extension NewReviewVC{
    
    private func getSaveCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SingleActionCell", for: indexPath) as? SingleActionCell else{
            return UITableViewCell()
        }
        cell.delegate = self
        cell.object = SingleActionObject(title: "Save Review", color: UIColor.cprGreenColor)
        cell.backgroundColor = .clear
        return cell
    }
    
    private func getDateSelectorCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DateSelectorCell", for: indexPath) as? DateSelectorCell else{
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        return cell
    }
    
    private func getPlaceNameCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceNameCell", for: indexPath) as? PlaceNameCell else{
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.set(placeName: viewModel.getPlaceName())
        return cell
    }
    
    private func getContainerCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ViewContainer", for: indexPath) as? ViewContainer else{
            return UITableViewCell()
        }
        let view = viewModel.getView(forIndexPath: indexPath)
        viewModel.setDelegate(view: view, vc: self)
        cell.set(titleAndDescription: viewModel.getTitleAndDescription(forIndexPath: indexPath))
        if let imageName = viewModel.getImage(forIndexPath: indexPath){
            cell.set(imageName: imageName)
        }
        if indexPath.section == 2{ //for some reason addition info need this
            cell.layoutIfNeeded()
        }
        cell.set(view: view)
        return cell
    }
    
}

extension NewReviewVC: GenericRatingComponentDelegate{
    func setRating(_ value: Int, forType: String) {
        guard let action = NewReviewAction(rawValue: forType) else {return}
        switch action{
        case .rate:
            viewModel.setRating(value)
        case .waiting:
            viewModel.setWaiting(value)
        default:
            print("")
        }
    }
}

extension NewReviewVC: DocumentIterable, UIDocumentInteractionControllerDelegate, PhotoScrollerViewDelegate{
    
    func select(photo: UIImage){
        displayImage(image: photo)
    }
    
    func displayImage(image currentImage: UIImage) {
        guard let path = saveTempFileAndGetURL(image: currentImage) else {return}
        let documentIteractor = UIDocumentInteractionController()
        documentIteractor.delegate = self
        documentIteractor.url = path
        documentIteractor.uti = path.typeIdentifier
        documentIteractor.name = path.localizedName
        documentIteractor.presentPreview(animated: true)
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        if let nav = self.navigationController{
            return nav
        }
        return self
    }
}

extension NewReviewVC: DateSelectorCellDelegate{
    func selectDate(_ date: Date) {
        viewModel.setWhen(date)
    }
}

extension NewReviewVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        dismiss(animated: true, completion: nil)
        let placeToAdd = CPRPlace(place: place)
        viewModel.set(place: placeToAdd)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}

extension NewReviewVC: SingleActionDelegate{
    func execute(object: SingleActionObject) {
        guard let newReviewAction = NewReviewAction(rawValue: object.title) else {return}
        switch newReviewAction{
        case .addPlace, .replacePlace:
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            self.present(autocompleteController, animated: true, completion: nil)
        case .addPhoto:
            addPhotoToReview()
        case .saveReview:
            guard viewModel.isGoodToSave() else {
                displaySimpleAlertMessage(title: "Error saving Review", message: "Make you provided all the required data")
                return
            }
            displayConfirmationDialog(title: "Clip Pic Review", message: "Are you sure you want to share this review?", okTitle: "Yes", isOKDesctructive: false) { [unowned self] _ in
                self.showLoadingScreen()
                self.viewModel.save { [weak self] result in
                    self?.removeLoadingScreen()
                    switch result{
                    case .error(let error):
                        self?.displaySimpleAlertMessage(title: "Error saving Review", message: "There was an error saving your review : \(error.message)")
                        return
                    default:
                        print("done")
                    }
                }
            }
        default:
            print("working with \(newReviewAction.getTitle())")
        }
    }
}


extension NewReviewVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func addPhotoToReview() {
        let selectSource = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePickerController.sourceType = .camera
                self.present(self.imagePickerController, animated: true, completion: nil)
            } else {
                self.displaySimpleAlertMessage(title: "No Camera", message: "There is no camera available on this device")
            }
        }
        let cameraRoll = UIAlertAction(title: "Camera Roll", style: .default) { (_) in
            self.imagePickerController.sourceType = .photoLibrary
            self.present(self.imagePickerController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        selectSource.addAction(takePhoto)
        selectSource.addAction(cameraRoll)
        selectSource.addAction(cancel)
        
        if let popoverController = selectSource.popoverPresentationController{
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.tabBarController?.tabBar.frame.origin.y ?? self.view.bounds.midY , width: 0, height: 0)
        }
        
        present(selectSource, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            viewModel.add(image:editedImage)
            let image = viewModel.getImageAt(index: 0)
            DispatchQueue.main.async {
                self.reviewIV.image = image
                let indexSet : IndexSet = [NewReviewSection.photos.rawValue]
                self.tableView.reloadSections(indexSet, with: .fade)
            }
        }
        dismiss(animated: true, completion: nil)
    }
}
