//
//  CategoryTVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/12/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class CategoryTVC: UITableViewCell {

    @IBOutlet weak var categoryNameLBL: UILabel!
    @IBOutlet weak var isSelectedIV: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func toggleSelection(isSelected: Bool) {
        isSelectedIV.image = isSelected ? UIImage(named: "checkmark") : nil
    }

    func configureView(category: ReviewCategory) {
        categoryNameLBL.text = category.name
        isSelectedIV.image = category.isSelected ? UIImage(named: "checkmark") : nil
    }
    
}
