//
//  ReviewImageCVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ReviewImageCVC: UICollectionViewCell {
    
    @IBOutlet weak var imageIV: UIImageView!
    
    var image: UIImage? {
        didSet {
            configureView()
        }
    }
    
    func configureView() {
        if let image = image {
            imageIV.image = image
            imageIV.layer.cornerRadius = 10
        }
    }
    
}
