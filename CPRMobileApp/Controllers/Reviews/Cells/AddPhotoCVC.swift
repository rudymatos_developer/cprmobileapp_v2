//
//  AddPhotoCVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/21/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol PhotoRemoverType: class {
    func deleteImage(atIndex: Int)
}

class AddPhotoCVC: UICollectionViewCell, CardCreator {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var currentImage: UIImageView!
    var index: Int = 0
    weak var delegate: PhotoRemoverType?
    
    func configureView(index: Int, image: UIImage?) {
        if let image = image {
            self.index = index
            currentImage.image = image
            let longTap = UILongPressGestureRecognizer(target: self, action: #selector(deletePhoto))
            self.currentImage.addGestureRecognizer(longTap)
            mainView.layer.cornerRadius = 10
            mainView.layer.masksToBounds = true
        }
    }
    
    @objc func deletePhoto(_ sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            delegate?.deleteImage(atIndex: index)
        }
    }
    
}
