//
//  CalendarReviewCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/9/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class CalendarReviewCell: UITableViewCell {
    
    @IBOutlet weak var placeNameLBL: UILabel!
    @IBOutlet weak var reviewIV: UIImageView!
    @IBOutlet weak var ratingLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(review: CPRReview) {
        DispatchQueue.main.async {
            self.placeNameLBL.text = review.locationName
            self.ratingLBL.text = "You gave \(review.rating) out of 5 stars rating"
            self.reviewIV.loadImage(withURLString: review.imagesDownloadURLs?.randomElement(), orLoadDefaultImageName: CPRConstants.logoName)
        }
    }
}
