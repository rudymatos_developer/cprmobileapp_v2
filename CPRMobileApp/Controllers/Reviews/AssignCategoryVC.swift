//
//  AssignCategoryVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/12/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol CategoriesManager: class {
    func toggleCategory(selectedCategories: [ReviewCategory])
    func handleSearchingKeyboardBehaviour(isSearchingInCategories: Bool)
}

enum Selection: String {
    case all = "All Items"
    case selectedCategory = "Selected"
}

class AssignCategoryVC: UIViewController {
    // MARK: - UI ELEMENTS
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchIV: UIImageView!
    @IBOutlet weak var categoriesTV: UITableView!
    @IBOutlet weak var searchCategoryView: UIView!
    @IBOutlet weak var selectionLBL: UILabel!
    
    weak var delegate: CategoriesManager?
    var isUserSearching = false
    
    private var selection: Selection = .all
    private var filteredCategories = [ReviewCategory]()
    
    var categories = [ReviewCategory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        loadCategories()
    }
    
    private func configureView() {
        self.navigationController?.navigationBar.barStyle = UIBarStyle.black
        self.navigationController?.navigationBar.tintColor = UIColor.white
        categoriesTV.delaysContentTouches = false
        applyCornerRadius(toView: mainView, cornerRadius: 10)
        applyCornerRadius(toView: searchCategoryView, cornerRadius: 15)
        searchCategoryView.layer.borderColor = UIColor.gray.cgColor
        searchCategoryView.layer.borderWidth = 0.5
        selectionLBL.text = selection.rawValue
    }
    
    @IBAction func handleFiltering(_ sender: UITextField) {
        if let text = sender.text, let textCount = sender.text?.trimmingCharacters(in: .whitespacesAndNewlines).count, textCount > 0 {
            handleSearchImageVisibility(count: textCount)
            filteredCategories = categories.filter({$0.name.uppercased().contains(text.uppercased())})
        } else {
            filteredCategories = categories.sorted(by: {$0.name.uppercased() < $1.name.uppercased()})
            handleSearchImageVisibility(count: 0)
        }
        DispatchQueue.main.async {
            self.categoriesTV.reloadData()
        }
    }
    
    @IBAction func toggleItemsToView(_ sender: UITapGestureRecognizer) {
        toggleSelection()
    }
    
    private func toggleSelection() {
        selection = selection == .all ? .selectedCategory : .all
        selectionLBL.text = selection.rawValue
        searchTF.text = ""
        filterRecordsBasedOnSelection()
        self.view.endEditing(true)
    }
    
    private func loadCategories() {
        let queue = DispatchQueue(label: "categoriesQueue", qos: .utility)
        queue.async {
            do {
                if let url = Bundle.main.path(forResource: "cpr_categories", ofType: "txt") {
                    let cprCategoriesFileContent = try String(contentsOfFile: url)
                    let cprCategoriesArray = cprCategoriesFileContent.split(separator: ",")
                    self.categories = cprCategoriesArray.compactMap({ReviewCategory(name: String($0), isSelected: false)})
                    self.filteredCategories = self.categories.sorted(by: {$0.name.uppercased() < $1.name.uppercased()})
                    DispatchQueue.main.async {
                        self.categoriesTV.reloadData()
                    }
                }
            } catch let error {
                print(error)
            }
        }
    }
    
    private func filterRecordsBasedOnSelection() {
        if selection == .all {
            filteredCategories = categories.sorted(by: {$0.name.uppercased() < $1.name.uppercased()})
        } else {
            filteredCategories = categories.sorted(by: {$0.name.uppercased() < $1.name.uppercased()}).filter({$0.isSelected == true})
        }
        DispatchQueue.main.async {
            self.categoriesTV.reloadData()
        }
    }
    
    private func handleSearchImageVisibility(count: Int) {
        searchIV.isHidden = count > 0 ? true : false
    }
    
}

extension AssignCategoryVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.handleSearchingKeyboardBehaviour(isSearchingInCategories: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.handleSearchingKeyboardBehaviour(isSearchingInCategories: false)
    }
    
}

extension AssignCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedCell = tableView.cellForRow(at: indexPath) as? CategoryTVC {
            let selectedCategory = filteredCategories[indexPath.row]
            selectedCategory.isSelected = !selectedCategory.isSelected
            selectedCell.toggleSelection(isSelected: selectedCategory.isSelected)
            tableView.deselectRow(at: indexPath, animated: false)
            if selection == .selectedCategory {
                filterRecordsBasedOnSelection()
            }
            delegate?.toggleCategory(selectedCategories: categories.filter({$0.isSelected == true}))
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredCategories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as? CategoryTVC {
            let currentCategory = filteredCategories[indexPath.row]
            cell.configureView(category: currentCategory)
            return cell
        } else {
            return UITableViewCell()
        }
    }
}
