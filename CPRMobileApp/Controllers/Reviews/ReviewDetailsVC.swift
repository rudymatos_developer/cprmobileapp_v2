//
//  ReviewDetailVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ReviewDetailsVC: UIViewController, ViewLoadingType,CPRDataFormatter, StoryboardInitializer{
    
    @IBOutlet weak var reviewIV: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var loadingScreen: LoadingScreen!

    var viewModel: ReviewDetailVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("🏀 initializing \(self.classForCoder.description())")
        registerCell()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    func configureView() {
        showLoadingScreen()
        viewModel.downloadImages{ [weak self] _ in
            if let image = self?.viewModel.getRandomReviewImage(){
                DispatchQueue.main.async {
                 self?.reviewIV.image = image
                }
            }
            self?.removeLoadingScreen()
        }
    }
    
    private func registerCell(){
        tableView.register(UINib(nibName: "ViewContainer", bundle: Bundle.main), forCellReuseIdentifier: "ViewContainer")
        tableView.register(UINib(nibName: "PlaceNameCell", bundle: Bundle.main), forCellReuseIdentifier: "PlaceNameCell")
        tableView.register(UINib(nibName: "ReviewActionsCell", bundle: Bundle.main), forCellReuseIdentifier: "ReviewActionsCell")
        tableView.register(UINib(nibName: "SingleActionCell", bundle: Bundle.main), forCellReuseIdentifier: "SingleActionCell")
          tableView.register(UINib(nibName: "RatingAndWaitingCell", bundle: Bundle.main), forCellReuseIdentifier: "RatingAndWaitingCell")
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension ReviewDetailsVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOrRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeight(forRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return reviewIV.frame.height - 200
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let transparentView = UIView(frame: CGRect(x: 0, y: 0, width: reviewIV.frame.width, height: reviewIV.frame.height))
        transparentView.backgroundColor = .clear
        return transparentView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch  indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceNameCell", for: indexPath) as? PlaceNameCell else{
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.set(placeName: viewModel.getPlaceName())
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewActionsCell", for: indexPath) as? ReviewActionsCell else{
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RatingAndWaitingCell", for: indexPath) as? RatingAndWaitingCell else{
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.configureView(rating: viewModel.getRating(), waiting: viewModel.getWaiting())
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SingleActionCell", for: indexPath) as? SingleActionCell else{
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.delegate = self
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ViewContainer", for: indexPath) as? ViewContainer else{
                return UITableViewCell()
            }
            let view = viewModel.getView(forRow: indexPath.row)
            viewModel.setDelegate(view: view, vc: self)
            cell.set(titleAndDescription: viewModel.getTitleAndDescription(forRow: indexPath.row))
            if let imageName = viewModel.getImage(forRow: indexPath.row){
                cell.set(imageName: imageName)
            }
            cell.set(view: view)
            return cell
        }
    }
}

extension ReviewDetailsVC: ReviewDetailViewDelegate, DocumentIterable{
    func displayImage(onIndex: Int) {
        guard let currentImage = viewModel.getImageAt(index: onIndex), let path = saveTempFileAndGetURL(image: currentImage) else {return}
        let documentIteractor = UIDocumentInteractionController()
        documentIteractor.delegate = self
        documentIteractor.url = path
        documentIteractor.uti = path.typeIdentifier
        documentIteractor.name = path.localizedName
        documentIteractor.presentPreview(animated: true)
    }
}

extension ReviewDetailsVC: UIDocumentInteractionControllerDelegate{
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        if let nav = self.navigationController{
            return nav
        }
        return self
    }
}

extension ReviewDetailsVC: SingleActionDelegate{
    func execute(object: SingleActionObject) {
        displayConfirmationDialog(title: "Delete Review", message: "Do you really want to delete this review?", okTitle: "Delete", isOKDesctructive: true) { [weak self] (completion) in
            self?.showLoadingScreen()
            self?.viewModel.archiveReview { [weak self] results in
                self?.removeLoadingScreen()
                switch results{
                case .error(let error):
                    self?.displaySimpleAlertMessage(title: "Error Deleting Review", message: error.message)
                default:
                    print("do nothing")
                }
            }
        }
    }
}

extension ReviewDetailsVC: ReviewActionsCellDelegate{
    func share() {
        let shareActivityController = UIActivityViewController(activityItems: [viewModel.getReviewShareText()], applicationActivities: nil)
        present(shareActivityController, animated: true, completion: nil)
    }
    
    func edit() {
        print("edit")
    }
}


