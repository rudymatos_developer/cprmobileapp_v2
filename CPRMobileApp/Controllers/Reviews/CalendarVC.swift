//
//  CalendarVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/9/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarVC: UIViewController, StoryboardInitializer{
    
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var noReviewsFound: UILabel!
    @IBOutlet weak var resultsView: UIView!
    @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var reviewsOnDateTV: UITableView!
    
    var viewModel : CalendarVM!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("🏀 initializing \(self.classForCoder.description())")
        configureView()
        registerNotifications()
        registerCells()
        loadData()
    }
    
    private func configureView() {
        calendarView.layoutSubviews()
        self.navigationController?.navigationBar.isHidden = true
        applyCornerRadius(toView: selectDateView, cornerRadius: 10)
        applyCornerRadius(toView: resultsView, cornerRadius: 10)
    }
    
    @IBAction func createNewReview(_ sender: UIButton) {
        viewModel.createNewReview()
    }
    
    func loadData(){
        let selectedDate = viewModel.getSelectedDate()
        viewModel.validateReviews(onDate: selectedDate){ [weak self] hasReviews in
            if hasReviews{
                DispatchQueue.main.async {
                    self?.reviewsOnDateTV.reloadData()
                    self?.calendarView.reloadData()
                }
                self?.resultsView.alpha = 1
                self?.noReviewsFound.alpha = 0
            }else{
                self?.resultsView.alpha = 0
                self?.noReviewsFound.alpha = 1
            }
        }
    }
}

extension CalendarVC{
    
    private func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(CalendarVC.refreshUserReviews(_:)), name: .refreshUserReviews, object: nil)
    }
    
    @objc private func refreshUserReviews(_ notification: Notification){
        viewModel.loadData()
        loadData()
    }
    
}

extension CalendarVC: FSCalendarDelegate, FSCalendarDataSource {
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        return viewModel.getImage(forDate: date)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        viewModel.filterReviews(byDate: date){ [weak self] hasReviews in
            if hasReviews {
                UIView.animate(withDuration: 0.5) {
                    self?.resultsView.alpha = 1
                    self?.noReviewsFound.alpha = 0
                }
            } else {
                UIView.animate(withDuration: 0.5) {
                    self?.resultsView.alpha = 0
                    self?.noReviewsFound.alpha = 1
                }
            }
            DispatchQueue.main.async {
                self?.reviewsOnDateTV.reloadData()
            }
        }
    }
}

extension CalendarVC: UITableViewDelegate, UITableViewDataSource {
    
    private func registerCells(){
        reviewsOnDateTV.register(UINib(nibName: "ReviewCell", bundle: Bundle.main), forCellReuseIdentifier: "reviewCell")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentReview = viewModel.getReview(byRow: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: false)
        viewModel.goToSelected(review: currentReview)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let record = viewModel.getReview(byRow: indexPath.row)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as? ReviewCell else {return UITableViewCell()}
        if indexPath.row % 2 == 0{
            cell.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
        }
        cell.configureView(selectionOption: record.convertToOption())
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getReviewCount()
    }
    
}
