//
//  DisclaimerVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/30/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class DisclaimerVC: UIViewController, CardCreator {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        createCard(shadowView: shadowView, mainView: mainView)
    }

}
