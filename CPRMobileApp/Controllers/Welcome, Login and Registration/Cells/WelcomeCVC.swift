//
//  WelcomeCVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/20/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class WelcomeCVC: UICollectionViewCell, WelcomeScreenUICellPresentation {
    
    @IBOutlet weak var welcomeIV: UIImageView!
    @IBOutlet weak var welcomeTitle: UILabel!
    @IBOutlet weak var welcomeDescription: UILabel!
    @IBOutlet weak var startNowView: UIView!
    @IBOutlet weak var startNowBtn: UIButton!
    
    var startNowCompletion: (() -> Void)?
    
    func configureCell(viewModel: WelcomeScreenPresentation, startNowCompletion: (() -> Void)?) {
        welcomeIV.image = UIImage(named: viewModel.imageName)
        welcomeTitle.text = NSLocalizedString(viewModel.title, comment: "Title for the welcome screen cell")
        welcomeDescription.text = NSLocalizedString(viewModel.description, comment: "Description for the welcome screen cell")
        startNowView.isHidden = viewModel.shouldHideStartNowButton
        if !viewModel.shouldHideStartNowButton {
            self.startNowCompletion = startNowCompletion
            startNowBtn.addTarget(self, action: #selector(WelcomeCVC.startNowBtnClicked), for: .touchUpInside)
            startNowView.layer.cornerRadius = startNowBtn.frame.height  / 2
            startNowView.layer.shadowColor = UIColor.black.cgColor
            startNowView.layer.shadowRadius = 5
            startNowView.layer.shadowOffset = CGSize(width: 3.0, height: 2.0)
            startNowView.layer.shadowOpacity = 1
        }
    }
    
    @objc private func startNowBtnClicked(){
        startNowCompletion?()
    }
    
    func animateScreen(viewModel: WelcomeScreenPresentation) {
        if !viewModel.wasObjectAlreadyShown {
            welcomeTitle.alpha = 0
            welcomeDescription.alpha = 0
            welcomeIV.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            startNowView.alpha = 0
            UIView.animate(withDuration: 1, animations: {
                self.welcomeTitle.alpha = 1
            }) { (completed) in
                UIView.animate(withDuration: 1, animations: {
                    self.welcomeDescription.alpha = 1
                }) {(_) in
                    if !viewModel.shouldHideStartNowButton {
                        self.startNowView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.4, options: [UIView.AnimationOptions.curveEaseIn], animations: {
                            self.startNowView.alpha = 1
                            self.startNowView.transform = CGAffineTransform.identity
                        }, completion: nil)
                    }
                }
            }
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.4, options: [UIView.AnimationOptions.curveEaseIn], animations: {
                self.welcomeIV.transform = CGAffineTransform.identity
            }, completion: nil)
        }
    }
    
}
