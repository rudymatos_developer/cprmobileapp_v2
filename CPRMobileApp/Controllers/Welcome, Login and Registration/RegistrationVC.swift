//
//  RegistrationVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/30/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class RegistrationVC: UIViewController, CPRDataFormatter, CPRDataValidator, ViewLoadingType, CardCreator, StoryboardInitializer {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var personalProfileBTN: BindiableButton!
    @IBOutlet weak var fullNameSV: UIStackView!
    @IBOutlet weak var dateOfBirthSV: UIStackView!
    @IBOutlet weak var acceptTermIV: UIImageView!
    var loadingScreen: LoadingScreen!
    @IBOutlet weak var registerButtonView: UIView!
    @IBOutlet weak var backButtonView: UIView!
    
    // MARK: - Disclaimer Views
    @IBOutlet var disclaimerView: UIView!
    @IBOutlet weak var disclaimerShadowView: UIView!
    @IBOutlet weak var disclaimerMainView: UIView!
    
    // MARK: - Text Fields
    @IBOutlet weak var firstName: BindiableTextField!
    @IBOutlet weak var surnameTF: BindiableTextField!
    @IBOutlet weak var emailTF: BindiableTextField!
    @IBOutlet weak var passwordTF: BindiableTextField!
    @IBOutlet weak var passwordConfirmationTF: BindiableTextField!
    
    private var currentTextField: BindiableTextField?
    var viewModel : RegisterUserVM!
    private var registrationViewOrigin: CGPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        bindComponents()
        registrationViewOrigin = shadowView.frame.origin
    }
    
    deinit{
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    private func bindComponents() {
        
        firstName.bind { [weak self]  in
            self?.viewModel.firstName = $0
        }
        
        surnameTF.bind { [weak self]  in
            self?.viewModel.surname = $0
        }
        
        emailTF.bind { [weak self]  in
            self?.viewModel.email = $0
        }
        
        passwordTF.bind { [weak self]  in
            self?.viewModel.password = $0
        }
        
        passwordConfirmationTF.bind { [weak self]  in
            self?.viewModel.confirmPassword = $0
        }
    }
    
    private func configureView() {
        createCard(shadowView: shadowView, mainView: mainView)
        createCard(shadowView: disclaimerShadowView, mainView: disclaimerMainView)
        applyCornerRadius(toView: registerButtonView, cornerRadius: registerButtonView.frame.height / 2)
        applyCornerRadius(toView: backButtonView, cornerRadius: backButtonView.frame.height / 2)
        disclaimerView.frame = CGRect(x: shadowView.frame.origin.x, y: shadowView.frame.origin.y, width: mainView.frame.width, height: mainView.frame.height)
    }
    
    @IBAction func showTermAndConditions(_ sender: UIButton) {
        UIView.transition(from: self.view, to: disclaimerView, duration: 0.5, options: [UIView.AnimationOptions.curveEaseIn, UIView.AnimationOptions.transitionFlipFromLeft]) { (_) in
            UIView.animate(withDuration: 0.5, animations: {
                self.disclaimerView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            })
        }
    }
    
    @IBAction func backToRegistrationView(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, animations: {
            self.disclaimerView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }) { (_) in
            UIView.transition(from: self.disclaimerView, to: self.view, duration: 0.5, options: [UIView.AnimationOptions.curveEaseIn, UIView.AnimationOptions.transitionFlipFromRight], completion: nil)
        }
    }
    
    @IBAction func dismissKeyboardFromView(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    @IBAction func dismissKeyboard(_ sender: UIButton) {
        self.view.endEditing(true)
    }
    
    @IBAction func toggleAgreeToTerms(_ sender: UITapGestureRecognizer) {
        viewModel.toggleAgreeWithTerms()
        acceptTermIV.image = viewModel.agreeWithTerms.value == true ? UIImage(named: "checked") : UIImage(named: "unchecked")
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func registerUser() {
        self.view.endEditing(true)
        showLoadingScreen()
        self.viewModel.register { [weak self] result in
            switch result{
            case .success:
                self?.removeLoadingScreen()
                self?.viewModel.callRegistrationCompletion()
            case .error(let error):
                self?.removeLoadingScreen()
                self?.displaySimpleAlertMessage(title: "Error creating User", message: error.message)
            }
        }
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        registerUser()
    }
}

extension RegistrationVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField as? BindiableTextField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let tf = textField as? BindiableTextField {
            if tf.tag == 1, !isEmpty(tf.text), let currentText = tf.text {
                tf.text = currentText
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let tf = textField as? BindiableTextField {
            if tf.returnKeyType == .next {
                tf.resignFirstResponder()
                tf.nextField?.becomeFirstResponder()
            } else if tf.returnKeyType == .go {
                registerUser()
            }
        }
        return true
    }
}
