//
//  ViewController.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/17/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class WelcomeScreenVC: UIViewController, StoryboardInitializer {
    
    @IBOutlet weak var welcomeCV: UICollectionView!
    @IBOutlet weak var currentWelcomeScreenPageController: UIPageControl!
    var viewModel : WelcomeScreenVM!
    
    var startNowCompletion: (() -> Void)?
    
    deinit{
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentWelcomeScreenPageController.numberOfPages = viewModel.getObjectCount()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension WelcomeScreenVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.bounds.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let currentWelcomeObject = viewModel.getObject(forIndex: indexPath.row) , let cell = cell as? WelcomeCVC else { return }
        currentWelcomeScreenPageController.isHidden = !currentWelcomeObject.shouldHideStartNowButton
        currentWelcomeScreenPageController.currentPage = indexPath.row
        cell.animateScreen(viewModel: currentWelcomeObject)
        viewModel.objectWasAlreadyShow(atIndex: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getObjectCount()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "welcomeCVIdentifier", for: indexPath) as? WelcomeCVC,
            let currentWelcomeScreenObject = viewModel.getObject(forIndex: indexPath.row) else {return UICollectionViewCell()}
        cell.configureCell(viewModel: currentWelcomeScreenObject, startNowCompletion: currentWelcomeScreenObject.shouldHideStartNowButton ? nil : startNowCompletion)
        return cell
    }
}
