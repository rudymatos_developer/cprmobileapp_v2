//
//  LoginVC.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/21/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class LoginVC: UIViewController,ViewLoadingType, StoryboardInitializer {
    
    @IBOutlet weak var loginWithCPR: UIView!
    @IBOutlet weak var passwordTF: BindiableTextField!
    @IBOutlet weak var usernameTF: BindiableTextField!
    
    var loadingScreen: LoadingScreen!
    var viewModel: LoginVM!
    
    deinit{
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        registerBindings()
    }
    
    private func configureView() {
        usernameTF.attributedPlaceholder = viewModel.generateWhitePlaceholderTextAttributeString(text: usernameTF.placeholder ?? "")
        passwordTF.attributedPlaceholder = viewModel.generateWhitePlaceholderTextAttributeString(text: passwordTF.placeholder ?? "")
        applyCornerRadius(toView: loginWithCPR, cornerRadius: loginWithCPR.frame.height / 2)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard)))
    }

    private func registerBindings(){
        usernameTF.bind{[weak self] in
            self?.viewModel.username = $0
        }
        passwordTF.bind{[weak self] in
            self?.viewModel.password = $0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func showRegistration(){
        viewModel.showRegistration()
    }
    
    @IBAction func loginToCPR(_ sender: UIButton) {
        self.view.endEditing(true)
        showLoadingScreen()
        viewModel.login{ [weak self] result in
            self?.removeLoadingScreen()
            switch result{
            case .error(let error):
                self?.displaySimpleAlertMessage(title: "Error trying to login", message: error.message)
            case .success(let user):
                self?.viewModel.completeLogin(destination: user.isBusiness ? .business : .normal)
            }
        }
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        self.view.endEditing(true)
        viewModel.goToForgotPassword()
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
}

extension LoginVC: TextSetter {
    func processTextValue(_ text: String) {
        viewModel.emailToReset = text
        viewModel.resetPassword(completion: { [weak self] (error) in
            if let error = error {
                self?.displaySimpleAlertMessage(title: "Error trying to reset password", message: error.message)
                return
            }
            self?.displaySimpleAlertMessage(title: "Reset Password Succesfully", message: "Please check your email address and follow the steps to reset your password")
        })
    }
}
