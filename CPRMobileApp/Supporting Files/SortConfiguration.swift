//
//  SortConfiguration.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/17/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

class SortConfiguration{
    
    var objects: [Any]
    var sorters: [SortConfiguration.Sorter] = []
    
    init(objects: [Any]){
        self.objects = objects
    }
    
    struct Sorter{
        var displayName: String
        var sort: (() -> Void)
        var selected = false
        
        init(displayName: String, selected: Bool = false,sort: @escaping (() -> Void)){
            self.displayName = displayName
            self.selected = selected
            self.sort = sort
        }
        
    }
    
}

extension SortConfiguration{
    func swapSelected(newIndex: Int){
        if let currentIndex = sorters.firstIndex(where: {$0.selected}){
                sorters[currentIndex].selected = false
        }
        sorters[newIndex].selected = true
    }
}

extension SortConfiguration{
    
    func getSortersForReviews() -> [SortConfiguration.Sorter]{
        
        func byNameAscending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRReview), let p2 = (p2 as? CPRReview) else {return true}
                return p1.locationName < p2.locationName
            })
        }
        
        func byNameDescending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRReview), let p2 = (p2 as? CPRReview) else {return true}
                return p1.locationName > p2.locationName
            })
        }
        
        func byRatingAscending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRReview), let p2 = (p2 as? CPRReview) else {return true}
                return p1.rating < p2.rating
            })
        }
        
        func byRatingDescending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRReview), let p2 = (p2 as? CPRReview) else {return true}
                return p1.rating > p2.rating
            })
        }
        
        func byDateAscending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRReview), let p2 = (p2 as? CPRReview) else {return true}
                return p1.when < p2.when
            })
        }
        
        func byDateDescending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRReview), let p2 = (p2 as? CPRReview) else {return true}
                return p1.when > p2.when
            })
        }
        
        let placeNameAsc = SortConfiguration.Sorter(displayName: "By Place Name Ascending", selected: true, sort: byNameAscending)
        let placeNameDesc = SortConfiguration.Sorter(displayName: "By Place Name Descending", sort: byNameAscending)
        
        let ratingAsc = SortConfiguration.Sorter(displayName: "By Rating Ascending", sort: byRatingAscending)
        let ratingDesc = SortConfiguration.Sorter(displayName: "By Rating Descending", sort: byRatingDescending)
        
        let dateAsc = SortConfiguration.Sorter(displayName: "By Date Ascending", sort: byDateAscending)
        let dateDesc = SortConfiguration.Sorter(displayName: "By Date Descending", sort: byDateDescending)
        
        return [placeNameAsc, placeNameDesc,ratingAsc,ratingDesc,dateAsc,dateDesc]
    }
    
    func getSorterForPlaces() -> [SortConfiguration.Sorter]{
        
        func byNameAscending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRPlace), let p2 = (p2 as? CPRPlace) else {return true}
                return p1.name < p2.name
            })
        }
        
        func byNameDescending(){
            objects.sort(by: { p1,p2 in
                guard let p1 = (p1 as? CPRPlace), let p2 = (p2 as? CPRPlace) else {return true}
                return p1.name > p2.name
            })
        }
        
        let byPlaceNameAscending = SortConfiguration.Sorter(displayName: "By Place Name Ascending", selected: true, sort: byNameAscending)
        let byPlaceNameDescending = SortConfiguration.Sorter(displayName: "By Place Name Descending", sort : byNameDescending)
        
        return [byPlaceNameAscending, byPlaceNameDescending]
    }
    
    
    
}
