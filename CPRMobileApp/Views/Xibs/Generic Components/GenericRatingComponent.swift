//
//  GenericRatingComponent.swift
//  LikeAndWaitingComponents
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Rudy E Matos. All rights reserved.
//

import UIKit
import Lottie

protocol GenericRatingComponentDelegate:class{
    func setRating(_ value: Int, forType: String)
}

enum AnimationType: String{
    case heart = "heart"
    case star = "stars"
}

class GenericRatingComponent: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet var views: [UIView]!
    private var animatedViews : [LOTAnimationView] = []
    private lazy var feedBack = UINotificationFeedbackGenerator()
    weak var delegate: GenericRatingComponentDelegate?
    
    var forType: String = ""
    var fileName: AnimationType!{
        didSet{
            initViews()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func setRatingManually(value: Int){
        animateTo(index: value, animated: false)
    }
    
}


extension GenericRatingComponent{
    private func initViews(){
//        layoutIfNeeded()
        views.forEach { view in
            let animationView = LOTAnimationView(name: fileName.rawValue, bundle: Bundle.main)
            animationView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            view.addSubview(animationView)
            animatedViews.append(animationView)
            let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(GenericRatingComponent.tapOnView(_:)))
            view.addGestureRecognizer(tapRecognizer)
        }
        start()
    }
    
    @objc private func tapOnView(_ gesture: UIGestureRecognizer){
        guard let tag = gesture.view?.tag else {return}
        feedBack.notificationOccurred(.success)
        delegate?.setRating(tag, forType: forType)
        animateTo(index: tag)
    }
    
    private func animateTo(index: Int, animated: Bool = true){
        animatedViews.forEach({$0.stop();$0.animationProgress=0.1})
        func play(toIndex: Int){
            if toIndex < 0{
                return
            }
            if animated{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.03) {
                    self.animatedViews[index - toIndex].play()
                    play(toIndex: toIndex  - 1)
                }
            }else{
                self.animatedViews[index - toIndex].animationProgress = 1
                play(toIndex: toIndex  - 1)
            }
        }
        play(toIndex: index)
    }
    
    private func start(){
        for av in animatedViews{
            av.animationProgress = 0.1
        }
    }
    
}
