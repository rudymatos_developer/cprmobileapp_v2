//
//  GOWithImageTitleAndDescriptionCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/3/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class ItemDescriptionCell: UICollectionViewCell {
    
    @IBOutlet weak var optionIV: UIImageView!
    @IBOutlet weak var optionTitleLBL: UILabel!
    @IBOutlet weak var optionDescriptionLBL: UILabel!

    var option: Section.Option!{
        didSet{
            configureView()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        optionIV.image = nil
        optionTitleLBL.text = ""
        optionDescriptionLBL.text = ""
    }

    private func configureView(){
        backgroundColor = UIColor.black
        optionTitleLBL.text = option.title
        optionDescriptionLBL.text = option.description
        optionIV.layer.cornerRadius = 15
        if option.object is CPRPlace{
            optionIV.image = nil
            optionIV.loadGooglePlacesImage(reference: option.imagePath)
        }else{
            optionIV.loadImage(withURLString: option.imagePath, orLoadDefaultImageName: CPRConstants.logoName)
        }
    }
}


    

