//
//  GOSeeAllCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/3/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol SeeAllDelegate: class{
    func seeAll(section: Section)
}

class TitleCell: UITableViewCell {
    
    @IBOutlet weak var categoryNameBTN: UIButton!
    private var section : Section?
    weak var delegate: SeeAllDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setCellInfo(section: Section){
        guard let title = section.sectionTitle?.title else {return}
        self.section = section
        categoryNameBTN.setTitle(title, for: .normal)
        backgroundColor = UIColor.black
    }
    
    @IBAction func showAllProductFromCategory(_ sender: UIButton) {
        guard let section = section, let delegate = delegate else {return}
        delegate.seeAll(section: section)
    }
    
}
