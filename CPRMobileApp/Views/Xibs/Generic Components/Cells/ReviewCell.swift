//
//  ReviewCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/22/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell, CPRDataFormatter{
    
    @IBOutlet weak var optionIV: UIImageView!
    @IBOutlet weak var optionTitle: UILabel!
    @IBOutlet weak var optionDescriptionLBL: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var reviewDateLBL: UILabel!
    @IBOutlet weak var ratingLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView(selectionOption : OptionSelectorConfiguration.Option){
        self.accessoryType = .disclosureIndicator
        optionIV.loadImage(withURLString: selectionOption.icon, orLoadDefaultImageName: CPRConstants.logoName)
        optionTitle.text = selectionOption.name
        optionDescriptionLBL.text = selectionOption.description
        
        if let review = selectionOption.object as? CPRReview{
            ratingLBL.text = "\(review.rating) stars"
            ratingView.layer.cornerRadius = ratingView.frame.height / 2
            reviewDateLBL.text = convert(date: review.when, usingPattern: .full)
        }else{
            ratingView.isHidden = true
            reviewDateLBL.isHidden = true
        }
    }
}
