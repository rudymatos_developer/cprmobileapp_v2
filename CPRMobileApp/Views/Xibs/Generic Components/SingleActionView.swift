//
//  ReusableCreateReview.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol SingleActionDelegate:class{
    func execute(object: SingleActionObject)
}

struct SingleActionObject{
    var title: String
    var color: UIColor
}

class SingleActionView: UIView {
   
    @IBOutlet weak var actionBTN: UIButton!
    @IBOutlet weak var buttonBGView: UIView!
    @IBOutlet var contentView: UIView!
    var object: SingleActionObject!{
        didSet{
            configureUI()
        }
    }
    weak var delegate: SingleActionDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of: self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func configureUI(){
        buttonBGView.backgroundColor = object.color
        actionBTN.setTitle(object.title, for: .normal)
    }
    
    @IBAction func createReview(_ sender: UIButton) {
        delegate?.execute(object: object)
    }
    
}

