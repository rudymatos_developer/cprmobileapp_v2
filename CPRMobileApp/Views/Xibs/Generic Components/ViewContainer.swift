//
//  ReusableViewContainer.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class ViewContainer: UITableViewCell {

    @IBOutlet var roundedView: UIView!
    @IBOutlet weak var reusableVCIcon: UIImageView!
    @IBOutlet weak var reusableVCTitle: UILabel!
    @IBOutlet weak var reusableVCDescription: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    private var reusableView: UIView?

    override func awakeFromNib() {
        super.awakeFromNib()
        reusableView = nil
        roundedView.layer.cornerRadius = 15
        backgroundColor = .clear
    }
    
    func set(view: UIView){
        reusableView?.removeFromSuperview()
        reusableView = nil
        self.reusableView = view
    }
    
    func set(titleAndDescription: (title:String,description:String)){
        reusableVCTitle.text = titleAndDescription.title
        reusableVCDescription.text = titleAndDescription.description
    }
    
    func set(imageName:String){
        DispatchQueue.main.async {
            self.reusableVCIcon.image = UIImage(named: imageName)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard let reusableView = reusableView else {return}
        reusableView.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height)
        containerView.addSubview(reusableView)
    }
    
}
