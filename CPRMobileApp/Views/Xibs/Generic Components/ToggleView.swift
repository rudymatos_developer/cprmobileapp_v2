//
//  ToggleView.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/9/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol ToggleViewDelegate:class{
    func toggle(status: Bool)
}

struct ToggleViewVM{
    var enabled: Bool
    var disabledImageName: String
    var enabledImageName: String
    
    mutating func toggleStatus(){
        enabled.toggle()
    }
}

class ToggleView: UIView {

    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mainBTN: UIButton!
    weak var delegate: ToggleViewDelegate?
    lazy var feedbackGenerator = UINotificationFeedbackGenerator()
    var viewModel : ToggleViewVM!{
        didSet{
            configureView()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    private func initializeView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func configureView(){
        triggerUIChanges(enabled: viewModel.enabled)
    }
    
    private func triggerUIChanges(enabled: Bool){
        if enabled{
            feedbackGenerator.notificationOccurred(.success)
            DispatchQueue.main.async {
                self.mainBTN.setImage(UIImage(named: self.viewModel.enabledImageName), for: .normal)
            }
        }else{
            DispatchQueue.main.async {
                self.mainBTN.setImage(UIImage(named: self.viewModel.disabledImageName), for: .normal)
            }
        }
        delegate?.toggle(status: viewModel.enabled)
    }
    
    @IBAction func toggle(_ sender: UIButton) {
        viewModel.toggleStatus()
        triggerUIChanges(enabled: viewModel.enabled)

    }

}
