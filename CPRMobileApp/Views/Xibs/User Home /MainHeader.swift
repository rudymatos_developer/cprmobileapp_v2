//
//  MainHeader.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 9/26/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol MainHeaderDelegate: class{
    func showWhereToGoView()
    func goTo(category: MainCategory)
}

class MainHeader: UIView {
    
    @IBOutlet weak var roundedView: UIView!
    @IBOutlet var actionBTNS: [UIButton]!
    @IBOutlet weak var userPictureIV: UIImageView!
    @IBOutlet weak var greetingView: UIView!
    @IBOutlet weak var greetingLBL: UILabel!
    @IBOutlet weak var separatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var whereToGoView: UIView!
    @IBOutlet weak var mainIV: UIImageView!
    
    private var timer: Timer!
    private var colors = UIColor.cprColors
    
    weak var delegate: MainHeaderDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of: self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        whereToGoView.layer.cornerRadius = whereToGoView.frame.height / 2
        greetingView.layer.cornerRadius = 10
        mainIV.loadImage(withURLString: CPRURLs.Unsplash.requestURL, orLoadDefaultImageName: "main_1")
        userPictureIV.alpha = 0
        greetingView.alpha = 0
        roundedView.layer.cornerRadius = 15
        animateButtons()
    }
    
    @IBAction func showWhereToGoView(_ sender: UIButton) {
        delegate?.showWhereToGoView()
    }
    
    func setUserInfo(user: CPRUser){
        greetingLBL.text = "Welcome \(user.fullName)"
        func animateViews(){
            DispatchQueue.main.async {
                self.userPictureIV.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                self.userPictureIV.alpha = 1
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.5, options: [.curveEaseOut], animations: {
                    self.userPictureIV.transform = CGAffineTransform.identity
                }) { _ in
                    self.greetingView.alpha = 1
                    self.greetingView.transform = CGAffineTransform(translationX: 150, y: 0)
                    UIView.animate(withDuration: 0.5) {
                        self.greetingView.transform = CGAffineTransform.identity
                    }
                }
            }
        }
        userPictureIV.loadImage(withURLString: user.profilePictureURL, orLoadDefaultImageName: CPRConstants.logoName , completion: animateViews)
    }
    
    func setSeparator(height: Int){
        separatorHeightConstraint.constant = CGFloat(height)
        DispatchQueue.main.async {
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func selectOption(_ sender: UIButton) {
        guard let identifier = sender.accessibilityIdentifier, let category = MainCategory(rawValue: identifier) else{
            return
        }
        delegate?.goTo(category: category)
    }
}


extension MainHeader{
    
    private func animateButtons(){
        guard let randomSeconds = (3..<8).randomElement(), let timeInterval = TimeInterval(exactly: randomSeconds) else {return}
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: false, block: { _ in
            guard let randomBTN = self.actionBTNS.randomElement() else {return}
            self.animate(button: randomBTN)
        })
    }
    
    private func animate(button: UIButton){
        let colorAnimator = UIViewPropertyAnimator(duration: 1.5, dampingRatio: 0.9, animations: {
            let transform = CGAffineTransform(scaleX: 5, y: 5)
            button.transform = transform
        })
        colorAnimator.addAnimations({
            button.transform = CGAffineTransform.identity
        })
        
        colorAnimator.addCompletion { _ in
            self.timer.invalidate()
            self.animateButtons()
        }
        colorAnimator.startAnimation()
    }
}
