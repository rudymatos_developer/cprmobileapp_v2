//
//  DeleteReviewCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/22/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class SingleActionCell: UITableViewCell {

    @IBOutlet weak var buttonBGView: UIView!
    @IBOutlet weak var actionBTN: UIButton!
    
    weak var delegate: SingleActionDelegate?
    var object: SingleActionObject!{
        didSet{
            configureUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func configureUI(){
        buttonBGView.backgroundColor = object.color
        actionBTN.setTitle(object.title, for: .normal)
    }
    
    @IBAction func deleteReview(_ sender: UIButton) {
        delegate?.execute(object: object)
    }
    
}
