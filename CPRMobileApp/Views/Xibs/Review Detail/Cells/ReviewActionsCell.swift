//
//  ReviewActionsCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/22/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol ReviewActionsCellDelegate:class{
    func share()
    func edit()
}

class ReviewActionsCell: UITableViewCell {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    weak var delegate: ReviewActionsCellDelegate?
    @IBAction func share(_ sender: UIButton) {
        delegate?.share()
    }
    @IBAction func edit(_ sender: UIButton) {
        delegate?.edit()
    }
}
