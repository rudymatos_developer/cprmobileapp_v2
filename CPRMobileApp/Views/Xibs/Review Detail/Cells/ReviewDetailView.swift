//
//  ReviewDetailView.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/22/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol ReviewDetailViewDelegate: class{
    func displayImage(onIndex: Int)
}

class ReviewDetailView: UIView, CPRDataFormatter {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var description1LBL: UILabel!
    @IBOutlet weak var description2LBL: UILabel!
    @IBOutlet weak var imageCV: UICollectionView!
    
    weak var delegate: ReviewDetailViewDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    var review: CPRReview!{
        didSet{
            configureUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        registerCells()
        imageCV.delegate = self
        imageCV.dataSource = self
    }
    
    private func registerCells(){
        imageCV.register(UINib(nibName: "ReviewPictureCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ReviewPictureCell")
        
    }
    
    private func configureUI(){
        DispatchQueue.main.async {
            self.description1LBL.attributedText = self.getDescriptionText()
            self.description2LBL.text = self.review.comments
            self.imageCV.reloadData()
        }
    }
    
    private func getDescriptionText() -> NSAttributedString?{
        var html = "On <b>date_placeholder</b>, I went to <b>place_placeholder</b>&nbsp;on <b>address_placeholder</b> and I ordered <b>service_placeholder</b>. The reason why I was there was <b>reason_placeholder</b> and I was there with <b>companion_placeholder</b>. I got the service from <b>server_placeholder</b>"
        html = html.replacingOccurrences(of: "date_placeholder", with: convert(date: review.when, usingPattern: .full))
        html = html.replacingOccurrences(of: "place_placeholder", with: review.locationName)
        html = html.replacingOccurrences(of: "address_placeholder", with: review.address)
        html = html.replacingOccurrences(of: "service_placeholder", with: review.service)
        html = html.replacingOccurrences(of: "reason_placeholder", with: review.reasonOfVisit)
        html = html.replacingOccurrences(of: "companion_placeholder", with: review.companion)
        html = html.replacingOccurrences(of: "server_placeholder", with: review.server)
        let data = Data(html.utf8)
        return try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
    }
    
}

extension ReviewDetailView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return review.imagesDownloadURLs?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.displayImage(onIndex: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let currentImageURL = review.imagesDownloadURLs?[indexPath.item] else {return UICollectionViewCell()}
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewPictureCell", for: indexPath) as! ReviewPictureCell
        cell.setImage(urlString: currentImageURL)
        return cell
    }
}
