//
//  RatingAndWaitingCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/22/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class RatingAndWaitingCell: UITableViewCell {

    @IBOutlet weak var waitingLBL: UILabel!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var waitingView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingView.layer.cornerRadius = ratingView.frame.height / 2
        waitingView.layer.cornerRadius = waitingView.frame.height / 2
    }

    func configureView(rating: Int, waiting: Int){
        DispatchQueue.main.async {
            self.waitingLBL.text = "\(waiting)"
            self.ratingLBL.text = "\(rating)"
        }
    }
}
