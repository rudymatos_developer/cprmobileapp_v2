//
//  ReusableReviewsViewer.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol ReviewsViewerDelegate:class{
    func viewDetails(fromReview review: CPRReview)
}

class ReviewsViewer: UIView {

    @IBOutlet weak var loadingBlurView: UIVisualEffectView!
    @IBOutlet weak var reviewDetailsCV: UICollectionView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var reviewComment: UILabel!
    @IBOutlet weak var reviewNumber: UILabel!
    @IBOutlet weak var reviewsCounterPC: UIPageControl!
    
    weak var delegate: ReviewsViewerDelegate?
    var viewModel: ReviewViewerVM!{
        didSet{
            reloadData()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        registerCell()
    }
    
    @IBAction func viewReviewDetails(_ sender: UIButton) {
        delegate?.viewDetails(fromReview: viewModel.getSelectedReview())
    }
 
    private func registerCell(){
        reviewDetailsCV.register(UINib(nibName: "ReviewDetailsCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ReviewDetailsCell")
    }
    
    private func reloadData(){
        reviewsCounterPC.numberOfPages = viewModel.getNumberOfItems()
        reviewDetailsCV.delegate = self
        reviewDetailsCV.dataSource = self
        DispatchQueue.main.async {
            self.loadingBlurView.isHidden = true
            self.reviewDetailsCV.reloadData()
        }
    }
}


extension ReviewsViewer: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.getNumberOfItems()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        viewModel.set(selectedIndex: indexPath.item)
        reviewNumber.text = viewModel.getReviewNumber()
        reviewsCounterPC.currentPage = indexPath.item
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewDetailsCell", for: indexPath) as! ReviewDetailsCell
        let review = viewModel.getReview(byIndex: indexPath.row)
        cell.review = review
        return cell
    }
    
}
