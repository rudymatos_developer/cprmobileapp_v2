//
//  ReusableReviewDetailsCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/7/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class ReviewDetailsCell: UICollectionViewCell , CPRDataFormatter{
    
    @IBOutlet weak var reviewDateLBL: UILabel!
    @IBOutlet weak var reviewCommentLBL: UILabel!
    @IBOutlet weak var reviewImagesCV: UICollectionView!
    
    
    override func awakeFromNib() {
        registerCell()
    }
    
    var review: CPRReview!{
        didSet{
            configureView()
        }
    }
    
    private func configureView(){
        reviewImagesCV.delegate = self
        reviewImagesCV.dataSource = self
        DispatchQueue.main.async {
            self.reviewDateLBL.text = "Created on \(self.convert(date: self.review.when, usingPattern: .full))"
            self.reviewCommentLBL.text = self.review.comments
            self.reviewImagesCV.reloadData()
        }
    }
    
    private func registerCell(){
        reviewImagesCV.register(UINib(nibName: "ReviewPictureCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ReviewPictureCell")
    }
    
}

extension ReviewDetailsCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return review.imagesDownloadURLs?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let currentImageURL = review.imagesDownloadURLs?[indexPath.item] else {return UICollectionViewCell()}
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewPictureCell", for: indexPath) as! ReviewPictureCell
        cell.setImage(urlString: currentImageURL)
        return cell
    }
}
