//
//  LikeAndSaveForLaterActionsCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/11/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

enum LASFLAction{
    case added
    case empty
    
    func getTitle(forAction: String) -> String{
        return self == .added ? "Remove from \(forAction)" : "Add to \(forAction)"
    }
    
    mutating func toggle(){
        self = self == .added ? .empty : .added
    }
}

protocol LikeAndSaveForLaterActionsCellDelegate: class{
    func toFavorite(action: LASFLAction)
    func toSaveForLater(action: LASFLAction)
}

class LikeAndSaveForLaterActionsCell: UITableViewCell {
    
    @IBOutlet weak var favoriteBTN: UIButton!
    @IBOutlet weak var saveForLaterBTN: UIButton!
    weak var delegate: LikeAndSaveForLaterActionsCellDelegate?
    
    var favoriteAction : LASFLAction = .empty {
        didSet{
            configureView()
        }
    }
    
    var saveForLaterAction : LASFLAction = .empty {
        didSet{
            configureView()
        }
    }

    @IBAction func toggleFavorites(_ sender: UIButton) {
        favoriteAction.toggle()
        configureView()
        delegate?.toFavorite(action: favoriteAction)
    }
    
    @IBAction func toggleSaveForLater(_ sender: UIButton) {
        saveForLaterAction.toggle()
        configureView()
        delegate?.toSaveForLater(action: saveForLaterAction)
    }
    
}


extension LikeAndSaveForLaterActionsCell{
    private func configureView(){
        let favoriteTitle = favoriteAction.getTitle(forAction: "Favorite")
        favoriteBTN.setTitle(favoriteTitle, for: .normal)
        let saveForLaterTitle = saveForLaterAction.getTitle(forAction: "Save for Later")
        saveForLaterBTN.setTitle(saveForLaterTitle, for: .normal)
    }
}
