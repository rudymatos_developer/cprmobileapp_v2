//
//  ReusableReviewPictureCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/7/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class ReviewPictureCell: UICollectionViewCell {
    
    @IBOutlet weak var reviewIV: UIImageView!
    
    override func awakeFromNib() {
        reviewIV.layer.cornerRadius = 15
    }
    
    func setImage(urlString: String){
        reviewIV.loadImage(withURLString: urlString)
    }
    
}
