//
//  ReusablePlaceNameCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/8/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

class PlaceNameCell: UITableViewCell {

    @IBOutlet weak var placeNameLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func set(placeName: String){
        placeNameLBL.text = placeName
    }
    
}
