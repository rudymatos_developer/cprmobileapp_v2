//
//  ReusablePlaceContactInfo.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol PlaceContactInfoDelegate: class{
    func contactPlace()
    func goToWebsite()
}

class PlaceContactInfo: UIView {
    
    @IBOutlet weak var goToWebsiteBTN: UIButton!
    @IBOutlet weak var contactPlaceBTN: UIButton!
    @IBOutlet var contentView: UIView!
    weak var delegate: PlaceContactInfoDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    @IBAction func contactPlace(_ sender: UIButton) {
        delegate?.contactPlace()
    }
    
    @IBAction func goToWebsite(_ sender: UIButton) {
        delegate?.goToWebsite()
    }
}
