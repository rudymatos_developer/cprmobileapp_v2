//
//  ReusablePlaceLocation.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit
import MapKit

class PlaceLocation: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var mapSnapshotIV: UIImageView!
    @IBOutlet weak var distanceLBL: UILabel!

    var placeLocationObject : (place: CPRPlace, userCurrentLocation: CLLocation?)!{
        didSet{
            loadData()
        }
    }

    private var placeLocation: CLLocation?
    private var coordinates: CLLocationCoordinate2D!
    private var mkSnapshot: MKMapSnapshotter? {
        didSet{
            loadMapImage()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    private func loadData(){
        getCurrentLocation()
        addAnnotation()
    }
    
    private func setDistance(text: String){
        DispatchQueue.main.async {
            self.distanceLBL.text = text
        }
    }
    
    @IBAction func openOnMaps(_ sender: UIButton) {
        openDestinationOnAppleMaps()
    }
    
    @IBAction func openOnGooglePlaces(_ sender: UIButton) {
        openDestinationOnGooglePlaces()
    }
    
}

extension PlaceLocation{
    
    private func openDestinationOnGooglePlaces(){
    }
    
    private func openDestinationOnAppleMaps(){
        guard let placeLocation = placeLocation else{
            return
        }
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: placeLocation.coordinate.latitude, longitude: placeLocation.coordinate.longitude)))
        destination.name = placeLocationObject.place.address ?? "Destination"
        destination.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
}

extension PlaceLocation{
    
    private func getCurrentLocation(){
        guard let userCurrentLocation = placeLocationObject.userCurrentLocation else {
            setDistance(text: "Distance to this location is currently unavailable")
            return
        }
        let placeLatitude = placeLocationObject.place.coordinates.latitude
        let placeLongitude = placeLocationObject.place.coordinates.longitude
        
        placeLocation = CLLocation(latitude: placeLatitude, longitude: placeLongitude)
        guard let placeLocation = placeLocation else {
            setDistance(text: "Distance to this location is currently unavailable")
            return
        }
        let miles = userCurrentLocation.distance(from: placeLocation) * 0.00062137
        setDistance(text: "You are currently \(Int(miles)) miles away from this location")
    }
    
    private func addAnnotation(){
        let mapSnapshotOptions = MKMapSnapshotter.Options()
        let location = CLLocationCoordinate2D(latitude: placeLocationObject.place.coordinates.latitude, longitude: placeLocationObject.place.coordinates.longitude)
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 500, longitudinalMeters: 500)
        mapSnapshotOptions.region = region
        mapSnapshotOptions.scale = UIScreen.main.scale
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
        DispatchQueue.main.async {
            mapSnapshotOptions.size = self.mapSnapshotIV.frame.size
            self.coordinates = location
            self.mkSnapshot = MKMapSnapshotter(options: mapSnapshotOptions)
        }
    }
    
    private func loadMapImage(){
        let bgQueue = DispatchQueue.global(qos: .background)
        mkSnapshot?.start(with: bgQueue, completionHandler: { [weak self] (snapshot, error) in
            guard let self = self, error == nil else {return}
            if let snapshopImage = snapshot?.image,
                let coordinatePoint = snapshot?.point(for: self.coordinates),
                let pinImage = UIImage(named: "map_pin"){
                UIGraphicsBeginImageContextWithOptions(snapshopImage.size, true, snapshopImage.scale)
                snapshopImage.draw(at: CGPoint.zero)
                let fixedPinPoint = CGPoint(x: coordinatePoint.x - pinImage.size.width / 2, y: coordinatePoint.y - pinImage.size.height)
                pinImage.draw(at: fixedPinPoint)
                let mapImage = UIGraphicsGetImageFromCurrentImageContext()
                DispatchQueue.main.async {
                    self.mapSnapshotIV.image = mapImage
                }
                UIGraphicsEndImageContext()
            }
        })
    }
    
}
