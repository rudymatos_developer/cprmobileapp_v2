//
//  SocialInfoView.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/31/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol SocialInfoCellDelegate:class{
    func loginTo(socialProfile: SocialProfile)
}

class SocialInfoCell: UITableViewCell {
    
    @IBOutlet weak var validSocialStack: UIStackView!
    @IBOutlet weak var invalidSocialStack: UIStackView!
    @IBOutlet weak var usernameLBL: UILabel!
    @IBOutlet weak var bioLBL: UILabel!
    @IBOutlet weak var profilePictureIV: UIImageView!
    @IBOutlet weak var socialNetworkIconIV: UIImageView!
    @IBOutlet weak var socialNetworkIconInvalidIV: UIImageView!
    @IBOutlet weak var shareToLBL: UILabel!
    @IBOutlet weak var isSharing: UISwitch!
    @IBOutlet weak var currentlyUnavailableLBL: UILabel!
    
    static let identifier = "SocialInfoCell"
    weak var delegate: SocialInfoCellDelegate?
    
    var socialProfile: SocialProfile!{
        didSet{
            configureView()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    private func configureView(){
        setDefaultValues()
        validSocialStack.isHidden = !socialProfile.isSocialAvailable()
        invalidSocialStack.isHidden = socialProfile.isSocialAvailable()
        currentlyUnavailableLBL.isHidden = socialProfile.isImplementationAvailable()
        if socialProfile.isSocialAvailable(){
            bioLBL.text = socialProfile.bio
            profilePictureIV.loadImage(withURLString: socialProfile.profilePictureURL?.absoluteString)
            profilePictureIV.layer.cornerRadius = profilePictureIV.frame.height / 2
            usernameLBL.text = socialProfile.getUsernameOrName()
        }
    }
    
    private func setDefaultValues(){
        socialNetworkIconIV.image = UIImage(named: socialProfile.socialNetwork.rawValue)
        socialNetworkIconInvalidIV.image = UIImage(named: socialProfile.socialNetwork.rawValue)
        shareToLBL.text = socialProfile.socialNetwork.getShareToText()
        profilePictureIV.image = UIImage(named: CPRConstants.logoName)
    }
 
    func shouldShare() -> Bool{
        return isSharing.isOn
    }
    
    @IBAction func share(_ sender: UISwitch) {
        socialProfile.shouldShare = sender.isOn
        if sender.isOn && !socialProfile.isSocialAvailable() && socialProfile.isImplementationAvailable(){
            delegate?.loginTo(socialProfile: socialProfile)
        }
    }
    
}
