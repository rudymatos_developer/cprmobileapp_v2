//
//  Rate.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/26/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit

class Rate: UIView {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var smilingFaceIV: UIImageView!
    @IBOutlet weak var starsIV: UIImageView!
    
    private var ratingStarsImgs : [String:UIImage?]?
    private var ratingImgs : [String:UIImage?]?
    
    
    @IBInspectable
    var cornerRadius : CGFloat = 0{
        didSet{
            self.cardView.layer.cornerRadius = cornerRadius
        }
    }
    
    var completion: ((Int) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("Rate", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        ratingImgs = ["review_1_stars" : UIImage(named:"review_1_stars"),"review_2_stars" : UIImage(named:"review_2_stars"),"review_3_stars" : UIImage(named:"review_3_stars"),"review_4_stars" : UIImage(named:"review_4_stars"),"review_5_stars" : UIImage(named:"review_5_stars")]
        ratingStarsImgs = ["1stars" : UIImage(named:"1stars"),"2stars" : UIImage(named:"2stars"),"3stars" : UIImage(named:"3stars"),"4stars" : UIImage(named:"4stars"),"5stars" : UIImage(named:"5stars")]
        
    }
    
    func changeRating(to: Int){
        smilingFaceIV.image = nil
        starsIV.image = nil
        DispatchQueue.main.async {
            self.smilingFaceIV.image = self.ratingImgs?["review_\(to)_stars"] ?? nil
            self.starsIV.image = self.ratingStarsImgs?["\(to)stars"] ?? nil
        }
        completion?(to)
    }
    
    @IBAction func changeRating(_ sender: UISlider) {
        let newValue = Int(sender.value)
        changeRating(to: newValue)
        
    }
    
}
