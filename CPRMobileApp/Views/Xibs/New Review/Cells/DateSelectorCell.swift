//
//  DateSelectorCell.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit
import FSCalendar


protocol DateSelectorCellDelegate:class{
    func selectDate(_ date: Date)
}

class DateSelectorCell: UITableViewCell {

    @IBOutlet weak var roundedView: UIView!
    @IBOutlet weak var fsCalendar: FSCalendar!
    weak var delegate: DateSelectorCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }

    private func configureView(){
        roundedView.layer.cornerRadius = 15
        fsCalendar.delegate = self
        fsCalendar.select(Date())
    }
    
}

extension DateSelectorCell: FSCalendarDelegate{
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        delegate?.selectDate(date)
    }
}
