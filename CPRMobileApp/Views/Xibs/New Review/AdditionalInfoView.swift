//
//  AdditionalInfoView.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

struct AdditionalInfoObject{
    var server: String
    var service: String
    var reasonOfVisit: String
    var companion: String
    var comments: String
}

class AdditionalInfoView: UIView {
    
    @IBOutlet weak var server: CPRTextView!
    @IBOutlet weak var service: CPRTextView!
    @IBOutlet weak var reasonOfVisit: CPRTextView!
    @IBOutlet weak var companion: CPRTextView!
    @IBOutlet weak var comments: CPRTextView!
    
    @IBOutlet var contentView: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func getAdditionalInfoObject() -> AdditionalInfoObject?{
        guard let server = server.getText(),
            let service = service.getText(),
            let reasonOfVisit = reasonOfVisit.getText(),
            let companion = companion.getText(),
            let comments = comments.getText() else {return nil}
        let object = AdditionalInfoObject(server: server, service: service, reasonOfVisit: reasonOfVisit, companion: companion, comments: comments)
        return object
    }
    
}
