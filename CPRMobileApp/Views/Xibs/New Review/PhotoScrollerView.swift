//
//  PhotoScrollerView.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol PhotoScrollerViewDelegate: class{
    func select(photo: UIImage)
}

class PhotoScrollerView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var photosCV: UICollectionView!
    weak var delegate: PhotoScrollerViewDelegate?
    
    var images : [UIImage?]?{
        didSet{
            DispatchQueue.main.async {
                self.photosCV.reloadData()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        photosCV.delegate = self
        photosCV.dataSource = self
        photosCV.register(UINib(nibName: "ReviewPictureCell", bundle: Bundle.main), forCellWithReuseIdentifier: "ReviewPictureCell")
    }
}

extension PhotoScrollerView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let image = images?[indexPath.row] else {return}
        delegate?.select(photo: image)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReviewPictureCell", for: indexPath) as? ReviewPictureCell,let image = images?[indexPath.row] else {return UICollectionViewCell()}
        cell.reviewIV.image = image
        return cell
    }
    
}
