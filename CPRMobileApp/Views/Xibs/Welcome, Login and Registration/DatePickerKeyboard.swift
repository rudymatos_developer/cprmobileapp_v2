//
//  DatePickerKeyboard.swift
//  cpr
//
//  Created by Rudy E Matos on 11/23/17.
//  Copyright © 2017 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol DatePickerKeyboardDelegate: class {
    func useDate()
    func cancel()
}

@IBDesignable
class DatePickerKeyboard: UIView {
    
    weak var delegate: DatePickerKeyboardDelegate?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var mainView: UIView?
    
    @IBInspectable
    var titleBGColor: UIColor = .cprPinkColor {
        didSet {
            self.mainView?.backgroundColor = self.titleBGColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    func configureView() {
        Bundle.main.loadNibNamed("\(type(of: self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        delegate?.cancel()
    }
    
    @IBAction func userDate(_ sender: UIButton) {
        delegate?.useDate()
    }
    
}
