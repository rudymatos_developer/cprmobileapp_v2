//
//  CountryCodePicker.swift
//  cpr
//
//  Created by Rudy E Matos on 11/27/17.
//  Copyright © 2017 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol OptionPickerViewDelegate: class {
    func assign(value: String,forType: OptionPickerConfigurator.OptionPickerViewType)
    func canceSelection(type: OptionPickerConfigurator.OptionPickerViewType)
    func update(withValue: String?,forType: OptionPickerConfigurator.OptionPickerViewType)
}

class OptionPickerConfigurator{
    
    enum OptionPickerViewType{
        case countryCode
        case maritalStatus
        case gender
    }
    
    init(type: OptionPickerViewType){
        self.type = type
        loadValues()
    }
    
    var type: OptionPickerViewType
    var values: [Any] = []
    var currentSelectedValue: String = ""
    
    func getTitle(forRow row: Int) -> String{
        switch type {
        case .countryCode:
            guard let countryCode = values[row] as? CPRCountryCode else {return ""}
            return "\(countryCode.isoCountryCode) - \(countryCode.countryName)"
        default:
            return values[row] as? String ?? ""
        }
    }
    
    func getBTNTitle() -> String{
        switch type{
        case .countryCode:
            return "Assign Country Code"
        case .maritalStatus:
            return "Assign Marital Status"
        case .gender:
            return "Assign Gender"
        }
    }
    
    func getUpdateCode(forRow row: Int) -> String?{
        switch type {
        case .countryCode:
            guard let countryCode = values[row] as? CPRCountryCode else {return nil}
            return countryCode.phonePrefix
        default:
            return nil
        }
    }
    
    func getTitleBackGroundColor() -> UIColor{
        switch type {
        case .countryCode:
            return .cprPinkColor
        case .gender:
            return .cprBlueColor
        case .maritalStatus:
            return .cprGreenColor
        }
    }
    
    func assignCurrentSelectedValue(forRow row: Int){
        switch type {
        case .countryCode:
            guard let countryCode = values[row] as? CPRCountryCode else {return}
            currentSelectedValue = "\(countryCode.isoCountryCode) - \(countryCode.countryName)"
        default:
            currentSelectedValue = values[row] as? String ?? ""
        }
    }
    
    
    private func loadValues(){
        switch type {
        case .countryCode:
            for countryISOCode in NSLocale.isoCountryCodes {
                if let country = (NSLocale.current as NSLocale).displayName(forKey: .countryCode, value: countryISOCode) {
                    values.append(CPRCountryCode(countryName: country, isoCountryCode: countryISOCode, phonePrefix: "+\(CPRCountryCode.getPhonePrefix(byISOCountryCode: countryISOCode))"))
                }
            }
        case .gender:
            values.append("Male")
            values.append("Female")
            values.append("Other")
            values.append("Unspecified")
        case .maritalStatus:
            values.append("Married")
            values.append("Widowed")
            values.append("Separated")
            values.append("Divorced")
            values.append("Single")
            values.append("Other")
            values.append("Unspecified")
        }
    }
    
}

class OptionPickerView: UIView {
    
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var assignBTN: UIButton!
    weak var delegate: OptionPickerViewDelegate?
    var configurator: OptionPickerConfigurator!{
        didSet{
            configureView()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureView() {
        Bundle.main.loadNibNamed("\(type(of: self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleWidth]
        titleView.backgroundColor = configurator.getTitleBackGroundColor()
        assignBTN.setTitle(configurator.getBTNTitle(), for: .normal)
        configurator.assignCurrentSelectedValue(forRow: 0)
    }
    
    @IBAction func assign(_ sender: UIButton) {
        delegate?.assign(value: configurator.currentSelectedValue, forType: configurator.type)
    }
    
    @IBAction func cancelCountryCodeSelection(_ sender: UIButton) {
        delegate?.canceSelection(type: configurator.type)
    }
    
}
extension OptionPickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        configurator.assignCurrentSelectedValue(forRow: row)
        delegate?.update(withValue: configurator.getUpdateCode(forRow: row), forType: configurator.type)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return configurator.values.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return configurator.getTitle(forRow: row)
    }
    
}
