//
//  GenericHelper.swift
//  cpr
//
//  Created by Rudy E Matos on 12/14/17.
//  Copyright © 2017 Bearded Gentleman. All rights reserved.
//

import UIKit

protocol HasUserDefaultService{
    var userDefaultService: UserDefaultsService {get set}
}

class UserDefaultsService {
    enum UDKeys {
        static let currentUserToRegister = "currentUserToRegister"
        static let currentUsername = "currentUsername"
        static let currentUserIsBusiness = "currentUserIsBusiness"
        static let currentDeviceFCM = "currentDeviceFCM"
        static let socialProfiles = "socialProfiles"
        static let currentFirebaseIdFromNotification = "currentFirebaseIdFromNotification"
        static let userReviews = "userReviews"
    }
}

extension UserDefaultsService{
    func createUserInfoInApp(user: CPRUser) {
        set(username: user.email)
        set(isBusiness: user.isBusiness)
    }
    func logout(){
        removeUsername()
        removeIsUserBusiness()
    }
}

extension UserDefaultsService {
    func getFirebaseIDFromNotification() -> String? {
        return UserDefaults.standard.object(forKey: UDKeys.currentFirebaseIdFromNotification) as? String
    }
    func set(firebaseIdFromNotification: String) {
        UserDefaults.standard.set(firebaseIdFromNotification, forKey: UDKeys.currentFirebaseIdFromNotification)
    }
    func removeFirebaseIdFromNotification() {
        UserDefaults.standard.removeObject(forKey: UDKeys.currentFirebaseIdFromNotification)
    }
}


extension UserDefaultsService {
    func getUsername() -> String? {
        return UserDefaults.standard.object(forKey: UDKeys.currentUsername) as? String
    }
    
    func set(username: String) {
        UserDefaults.standard.set(username, forKey: UDKeys.currentUsername)
    }
    
    func removeUsername() {
        UserDefaults.standard.removeObject(forKey: UDKeys.currentUsername)
    }
    
}

extension UserDefaultsService {
    
    func getDeviceFMCToken() -> String? {
        return UserDefaults.standard.object(forKey: UDKeys.currentDeviceFCM) as? String
    }
    
    func set(deviceFMCToken: String) {
        UserDefaults.standard.set(deviceFMCToken, forKey: UDKeys.currentDeviceFCM)
    }
    func removeDeviceFMCToken() {
        UserDefaults.standard.removeObject(forKey: UDKeys.currentDeviceFCM)
    }
}

extension UserDefaultsService {
    
    func set(isBusiness: Bool) {
        UserDefaults.standard.setValue(isBusiness, forKey: UDKeys.currentUserIsBusiness)
    }
    
    func removeIsUserBusiness() {
        UserDefaults.standard.removeObject(forKey: UDKeys.currentUserIsBusiness)
    }
    
    func isUserBusiness() -> Bool {
        return UserDefaults.standard.bool(forKey: UDKeys.currentUserIsBusiness)
    }
}


extension UserDefaultsService {
    
    func set(socialProfiles: [SocialProfile]) {
        let profilesData = socialProfiles.compactMap { profile -> Data? in
            try? JSONEncoder().encode(profile)
        }
        UserDefaults.standard.setValue(profilesData, forKey: UDKeys.socialProfiles)
    }
    
    func removeAllSocialProfiles() {
        UserDefaults.standard.removeObject(forKey: UDKeys.socialProfiles)
        UserDefaults.standard.synchronize()
    }
    
    func getSocialProfiles() -> [SocialProfile] {
        guard  let profilesData = UserDefaults.standard.object(forKey: UDKeys.socialProfiles) as? [Data] else {
            let socialProfiles = [SocialProfile(with: .twitter), SocialProfile(with: .pinterest), SocialProfile(with: .facebook)]
            return socialProfiles
        }
        
        let socialProfiles = profilesData.compactMap { data -> SocialProfile? in
            try? JSONDecoder().decode(SocialProfile.self, from: data)
        }
        return socialProfiles
    }
}
