//
//  LoadUserInfoService.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import MapKit

protocol HasLoadUserInfoService{
    var loadUserInfoService : LoadUserInfoService? {get set}
}

class LoadUserInfoService : HasDependencies{
    
    typealias Dependencies = HasUser & HasAuthorizableService & HasDataService & HasUserDefaultService & HasGooglePlaceService
    var dependencies: Dependencies?
    
    private var loginError: CPRError?
    private var usersCurrentLocation: CLLocation?{
        return dependencies?.currentUser?.currentLocation
    }
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
    }
    
    func loadUserReviews(user: CPRUser, completion: @escaping (Result<[CPRReview]>) -> Void){
        dependencies?.dataService.getAllReviews(byUser: user){ result in
            completion(result)
        }
    }
    
    func loadUserPlaces(user: CPRUser, fromCollection collectionName: CPRConstants.UserFields.UserPlaces, completion: @escaping (Result<[CPRPlace]>) -> Void){
        dependencies?.dataService.getPlaces(fromCollection: collectionName.rawValue, fromUser: user, limit: 10) { result in
            completion(result)
        }
    }
    
    func loadUserInfo(completion: @escaping (Result<CPRUser>)->Void){
        guard let dependencies = dependencies, let username = dependencies.userDefaultService.getUsername() , dependencies.authorizableService.isUserLoggedIn() else {
            let error = CPRError(message: "You must login first", errorType: .invalidUserData)
            completion(.error(error))
            return
        }

        let queue = DispatchQueue(label: "loadUserInfoQueue", qos: .userInitiated, attributes: .concurrent)
        let dispatchGroup = DispatchGroup()
        
        queue.async {
            dispatchGroup.enter()
            dependencies.dataService.getUserData(byUserKey: username) { [weak self] user, error in
                guard let user = user, error == nil else{
                    self?.loginError = CPRError(message: "You must login first", errorType: .invalidUserData)
                    dispatchGroup.leave()
                    return
                }
                self?.dependencies?.currentUser = user
                dispatchGroup.leave()
            }
            dispatchGroup.wait()
            if let user = dependencies.currentUser {
                dispatchGroup.enter()
                self.loadUserReviews(user: user) { [weak self] result in
                    switch result{
                    case .success(let reviews):
                        self?.dependencies?.currentUser?.reviews = reviews
                    case .error(let error):
                        print(error)
                    }
                    dispatchGroup.leave()
                }
                
                dispatchGroup.enter()
                self.loadUserPlaces(user: user, fromCollection: .favoritePlaces) { [weak self] result in
                    self?.savePlacesCompletion(collectionName: .favoritePlaces, result: result)
                    dispatchGroup.leave()
                }
                
                dispatchGroup.enter()
                self.loadUserPlaces(user: user, fromCollection: .saveForLaterPlaces) { [weak self] result in
                    self?.savePlacesCompletion(collectionName: .saveForLaterPlaces, result: result)
                    dispatchGroup.leave()
                }
            }
            
            dispatchGroup.notify(queue: .main, execute: {
                guard let user = self.dependencies?.currentUser, self.loginError == nil else{
                    completion(.error(self.loginError!))
                    return
                }
                completion(.success(user))
            })
        }
    }
    
    private func savePlacesCompletion(collectionName: CPRConstants.UserFields.UserPlaces, result: Result<[CPRPlace]>){
        switch result{
        case .success(let places):
            switch collectionName{
            case .favoritePlaces:
                dependencies?.currentUser?.favoritePlaces = places
            case .saveForLaterPlaces:
                dependencies?.currentUser?.saveForLaterPlaces = places
            }
        case .error(let error):
            print(error)
        }
    }
    
}

extension LoadUserInfoService{
    
    func loadNearByPlacesBy(category: MainCategory) -> (()->Void){
        func loadNearByPlaces(){
            guard let myCurrentLocation = usersCurrentLocation else{
                NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: nil)
                return
            }
            var userInfo : [String:Result<SortConfiguration>] = [:]
            dependencies?.googlePlacesService?.getNearByPlaces(byCurrentLocation: (latitude: myCurrentLocation.coordinate.latitude, longitude: myCurrentLocation.coordinate.longitude), andCategory: category.getGooglePlacesName(), completion: { result in
                switch result{
                case .success(let googlePlaceResponse):
                    guard googlePlaceResponse.results.count > 0 else {
                        NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: nil)
                        return
                    }
                    let sortConfiguration = SortConfiguration(objects: googlePlaceResponse.results.compactMap({CPRPlace.init(googlePlaceResult: $0, category: category)}))
                    sortConfiguration.sorters = sortConfiguration.getSorterForPlaces()
                    userInfo["result"] =  .success(sortConfiguration)
                    NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: userInfo)
                case .error(let error):
                    userInfo["result"] =  .error(error)
                    NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: userInfo)
                }
            })
        }
        return loadNearByPlaces
    }
    
    func loadFavoritePlaces(){
        loadPlacesBy(collectionName: .favoritePlaces)
    }
    
    func loadSaveForLaterPlaces(){
        loadPlacesBy(collectionName: .saveForLaterPlaces)
    }
    
    func getCategoryFunction(category: MainCategory) -> (()-> Void){
        func loadPlacesByCategories(){
            dependencies?.dataService.getAllPlaces(byCategory: category.getGooglePlacesName(), limit: 100, completion: { [weak self] result in
                self?.processPlacesResults(result)
            })
        }
        return loadPlacesByCategories
    }
    
    func loadPlacesBy(collectionName: CPRConstants.UserFields.UserPlaces){
        guard let user = dependencies?.currentUser else {return}
        dependencies?.dataService.getPlaces(fromCollection: collectionName.rawValue, fromUser: user, limit: 10, completion: { [weak self] result in
            self?.processPlacesResults(result)
        })
    }
    
    func processPlacesResults(_ result: (Result<[CPRPlace]>)) {
        var userInfo : [String:Result<SortConfiguration>] = [:]
        switch result{
        case .success(var places):
            if let myCurrentLocation = usersCurrentLocation {
                places.sort{ p1,p2 in
                    let p1Location = CLLocation(latitude: p1.coordinates.latitude, longitude: p1.coordinates.longitude)
                    let p2Location = CLLocation(latitude: p2.coordinates.latitude, longitude: p2.coordinates.longitude)
                    return myCurrentLocation.distance(from: p1Location) < myCurrentLocation.distance(from: p2Location)
                }
            }else{
                places.sort(by: {$0.name < $1.name})
            }
            let sortConfiguration = SortConfiguration(objects: places)
            sortConfiguration.sorters = sortConfiguration.getSorterForPlaces()
            userInfo["result"] =  .success(sortConfiguration)
            NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: userInfo)
        case .error(let error):
            userInfo["result"] =  .error(error)
            NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: userInfo)
        }
    }
    
    func loadAllUserReviews(){
        guard let user = dependencies?.currentUser else {return}
        dependencies?.dataService.getAllReviews(byUser: user, completion: { result in
            var userInfo : [String:Result<SortConfiguration>] = [:]
            switch result{
            case .success(let reviews):
                let sortConfiguration = SortConfiguration(objects: reviews)
                sortConfiguration.sorters = sortConfiguration.getSortersForReviews()
                userInfo["result"] =  .success(sortConfiguration)
                NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: userInfo)
            case .error(let error):
                userInfo["result"] =  .error(error)
                NotificationCenter.default.post(name: .optionSelectorDataLoaded, object: nil, userInfo: userInfo)
            }
        })
    }
    
    
}
