//
//  SocialMediaService.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/3/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import OAuthSwift

protocol HasSocialMediaService{
    var socialMediaService: SocialMediaService {get set}
}

class SocialMediaService{
    private let authorizationHeaderString = "Authorization"
    private var oauthSwift: OAuthSwift?
}


//MARK: - Login Extension
extension SocialMediaService{
    
    func login(to socialNetwork: SocialNetwork, completion: @escaping (Result<SocialProfile>) -> Void){
        oauthSwift = socialNetwork.getOAuthInstance()
        authorize(socialNetwork: socialNetwork, oauthSwift: oauthSwift, completion: { credential, response, parameters in
            switch socialNetwork{
            case .twitter:
                guard let userId = parameters[CPRURLs.Twitter.ReturnLoginParams.userIdParam] as? String, let screenName = parameters[CPRURLs.Twitter.ReturnLoginParams.screenName] as? String else {
                    let cprError = CPRError(message: "Error getting credentials. Invalid UserID or ScreenName", errorType: .invalidUserData)
                    completion(.error(cprError))
                    return
                }
                let socialProfile = SocialProfile(username: userId, name: screenName, oauthToken: credential.oauthToken, oauthTokenSecret: credential.oauthTokenSecret, socialNetwork: socialNetwork)
                completion(.success(socialProfile))
            case .pinterest:
                guard let accessToken = parameters[CPRURLs.Pinterest.ReturnLoginParams.accessToken] as? String else{
                    let cprError = CPRError(message: "Error getting credentials. Invalid Access Token", errorType: .invalidUserData)
                    completion(.error(cprError))
                    return
                }
                let socialProfile = SocialProfile(oauthToken: accessToken, socialNetwork: socialNetwork)
                completion(.success(socialProfile))
            case .facebook:
                let cprError = CPRError(message: "Error getting credentials. Invalid Social Network", errorType: .invalidUserData)
                completion(.error(cprError))
            }
        }) { error in
            let cprError = CPRError(message: "Error getting credentials. \(error.description)", errorType: .invalidUserData)
            completion(.error(cprError))
        }
    }
    
}

extension SocialMediaService{
    
    private func authorize(socialNetwork: SocialNetwork, oauthSwift: OAuthSwift?, completion: @escaping (OAuthSwiftCredential, OAuthSwiftResponse?, OAuthSwift.Parameters) -> Void, failureCompletion: @escaping (OAuthSwiftError)->Void){
        switch socialNetwork.getOAuthType(){
        case .oauth1:
            (oauthSwift as? OAuth1Swift)?.authorize(withCallbackURL: socialNetwork.getCallbackURL(), success: completion, failure: failureCompletion)
        case .oauth2:
            (oauthSwift as? OAuth2Swift)?.authorize(withCallbackURL: socialNetwork.getCallbackURL(), scope: socialNetwork.getScope(), state: socialNetwork.getState(), success: completion, failure: failureCompletion)
        }
    }
    
    private func getHeader(socialProfile: SocialProfile, urlString: String? = nil,  method: OAuthSwiftHTTPRequest.Method = .GET, params: [String:Any]? = nil) -> [String:String]?{
        switch socialProfile.socialNetwork{
        case .twitter:
            guard  let accessToken = socialProfile.oauthToken ,
                let accessTokenSecret = socialProfile.oauthTokenSecret ,
                let urlString = urlString,
                let params = params,
                let url = URL(string: urlString) else{
                    return nil
            }
            let credetial = OAuthSwiftCredential(consumerKey: CPRURLs.Twitter.consumerKey, consumerSecret: CPRURLs.Twitter.consumerSecret)
            credetial.signatureMethod = .HMAC_SHA1
            credetial.oauthToken = accessToken
            credetial.oauthTokenSecret = accessTokenSecret
            credetial.version = .oauth1
            return [authorizationHeaderString:credetial.authorizationHeader(method: method, url: url ,parameters: params)]
        case .pinterest, .facebook:
            guard  let accessToken = socialProfile.oauthToken else {return nil}
            return [authorizationHeaderString: "Bearer \(accessToken)"]
        }
    }
    
    
    
}


//MARK: - Profile Retriever Extension
extension SocialMediaService{
    
    private func getProfileRequest(socialProfile: SocialProfile) -> URLRequest?{
        var request : URLRequest?
        switch socialProfile.socialNetwork {
        case .twitter:
            guard let userId = socialProfile.username,
                let screenName = socialProfile.name,
                let url = URL(string: "\(CPRURLs.Twitter.AdditionalURLS.userShow)?user_id=\(userId)&screen_name=\(screenName)"),
                let headers = getHeader(socialProfile: socialProfile, urlString: CPRURLs.Twitter.AdditionalURLS.userShow, params:  [CPRURLs.Twitter.ReturnLoginParams.userIdParam:userId,CPRURLs.Twitter.ReturnLoginParams.screenName:screenName]) else {return nil}
            request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
            request?.allHTTPHeaderFields = headers
        case .pinterest:
            guard let url = URL(string:"https://api.pinterest.com/v1/me/?fields=first_name%2Cid%2Clast_name%2Curl%2Cimage%2Cbio%2Cusername"), let headers = getHeader(socialProfile: socialProfile) else {return nil}
            request =  URLRequest(url: url,cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
            request?.allHTTPHeaderFields = headers
        case .facebook:
            return nil
            
        }
        request?.httpMethod = "GET"
        return request
    }
    
    func retrieveProfile<T:Codable>(type: T.Type, socialProfile: SocialProfile, completion: @escaping (Result<T>) -> Void){
        guard let urlRequest = getProfileRequest(socialProfile: socialProfile) else{
            let cprError = CPRError(message: "Error getting profile URL.", errorType: .errorGettingData)
            completion(.error(cprError))
            return
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            do{
                guard let data = data , (response as? HTTPURLResponse)?.statusCode == 200 else
                {
                    let error = CPRError(message: "Error getting profile info with message : \(error.debugDescription)", errorType: .errorGettingData)
                    completion(.error(error))
                    return
                }
                let profileInfo = try JSONDecoder().decode(T.self, from: data)
                completion(.success(profileInfo))
            }catch{
                let error = CPRError(message: "Error getting profile info with message : \(error.localizedDescription)", errorType: .errorGettingData)
                completion(.error(error))
                return
            }
            }.resume()
    }
}

extension SocialMediaService{
    
    func getPostRequestHeader(socialProfile: SocialProfile, review: CPRReview) -> URLRequest?{
        var request : URLRequest?
        switch socialProfile.socialNetwork{
        case .twitter:
            guard let url = URL(string: CPRURLs.Twitter.AdditionalURLS.postTweetURL),
                var headers = getHeader(socialProfile: socialProfile,
                                        urlString: CPRURLs.Twitter.AdditionalURLS.postTweetURL,
                                        method: .POST,
                                        params: [CPRURLs.Twitter.status: review.getShareText(encode:false)]) else {
                return nil
            }
            request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
            headers["Content-Type"] = "application/x-www-form-urlencoded"
            request?.httpBody = "\(CPRURLs.Twitter.status)=\(review.getShareText(encode:true))".data(using: .utf8)
            request?.allHTTPHeaderFields = headers
            request?.httpMethod = "POST"
        default:
            print("do nothing")
        }
        return request
    }
    
    func post(socialProfile: SocialProfile, review: CPRReview, completion: @escaping (Result<Bool>) -> Void){
        switch socialProfile.socialNetwork{
        case .twitter:
            guard let urlRequest =  getPostRequestHeader(socialProfile: socialProfile, review: review) else {
                let cprError = CPRError(message: "Error sharing review. Invalid Header", errorType: .errorGettingData)
                completion(.error(cprError))
                return
            }
            URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                guard let _ = data , (response as? HTTPURLResponse)?.statusCode == 200 else
                {
                    let error = CPRError(message: "Error sharing tweet with message : \(error.debugDescription)", errorType: .errorGettingData)
                    completion(.error(error))
                    return
                }
                completion(.success(true))
                }.resume()
        case .facebook, .pinterest:
            print("do nothing")
            completion(.success(true))
        }
    }
}




