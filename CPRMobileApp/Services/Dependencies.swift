//
//  HasDependencies.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/4/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol HasDependencies{
    associatedtype Dependencies
    var dependencies: Dependencies? {get set}
}

class AppDependencies : HasUserDefaultService, HasAuthorizableService, HasDataService, HasUser, HasLoadUserInfoService, HasSocialMediaService, HasGooglePlaceService,HasBundleService{
    var userDefaultService: UserDefaultsService
    var dataService : FirebaseDataService
    var authorizableService : Authorizable
    var bundleService: BundleService
    var socialMediaService: SocialMediaService
    var googlePlacesService : GooglePlaceService?
    var loadUserInfoService: LoadUserInfoService?
    var currentUser: CPRUser?
    
    public init(userDefaultService: UserDefaultsService,
                dataService : FirebaseDataService,
                authorizableService : Authorizable, socialMediaService : SocialMediaService, bundleService :BundleService) {
        self.userDefaultService = userDefaultService
        self.dataService  = dataService
        self.bundleService = bundleService
        self.authorizableService  = authorizableService
        self.socialMediaService = socialMediaService
    }
}


