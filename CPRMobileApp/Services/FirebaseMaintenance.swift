//
//  FirebaseMaintenance.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/8/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import Firebase

class FirebaseMaintenance{
    
    private let db : Firestore = {
        let db = Firestore.firestore()
        let settings = db.settings
        db.settings = settings
        return db
    }()
    
    
    func fixFirstNameSurName(){
        print("ghere")
        let users = db.collection("users")
        users.getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else {return}
            for document in documents{
                guard let fullName = document.data()["fullName"] as? String else {return}
                let split = fullName.split(separator: " ").compactMap({String($0)})
                let firstName = split[0]
                let lastName = split[1]
                 self.db.collection("users").document(document.documentID).setData(["firstName":firstName, "surname":lastName], merge: true)
            }
        }
    }
    
    func fixReviews(){
        let reviewRef = db.collection("reviews")
        reviewRef.getDocuments { snapshot, error in
            guard let documents = snapshot?.documents.compactMap({CPRReview(data: $0.data())}) else {return}
            for document in documents{
                let userID = document.userID
                let userRef = self.db.collection("users").document(userID).collection("reviews").document(document.firebaseId)
                userRef.setData(document.parse(), merge: true)
                guard let placeID = document.placeID else {continue}
                let placeRef = self.db.collection("places").document(placeID).collection("reviews").document(document.firebaseId)
                placeRef.setData(document.parse(), merge: true)
            }
        }
    }
    
    func backupReviews(){
        let reviewRef = db.collection("reviews")
        reviewRef.getDocuments { snapshot, error in
            guard let documents = snapshot?.documents.compactMap({CPRReview(data: $0.data())}), documents.count > 0 else { return }
            for document in documents{
                let reviewBackup = self.db.collection("reviews_bk").document(document.firebaseId)
                reviewBackup.setData(document.parse(), merge: true)
            }
        }
    }
    
    func backupPlaces(){
        let reviewRef = db.collection("places")
        reviewRef.getDocuments { snapshot, error in
            guard let documents = snapshot?.documents.compactMap({CPRPlace(data: $0.data())}), documents.count > 0 else { return }
            for document in documents{
                let reviewBackup = self.db.collection("places_bk").document(document.googlePlaceID)
                reviewBackup.setData(document.parse(), merge: true)
            }
        }
    }
    
    func backupUsers(){
        let reviewRef = db.collection("users")
        reviewRef.getDocuments { snapshot, error in
            guard let documents = snapshot?.documents.compactMap({CPRUser(data: $0.data())}), documents.count > 0 else { return }
            for document in documents{
                let reviewBackup = self.db.collection("users_bk").document(document.email)
                reviewBackup.setData(document.parse(), merge: true)
            }
        }
    }
    
    func fixReviewPlaces(){
        let reviewRef = db.collection("reviews")
        reviewRef.getDocuments { snapshot, error in
            guard let documents = snapshot?.documents.compactMap({CPRReview(data: $0.data())}), documents.count > 0 else {
                print("no documents to work with")
                return
            }
            for review in documents{
                let reviewInUserRef = self.db.collection("users").document(review.userID).collection("reviews").document(review.firebaseId)
                let reviewRefInReviews = self.db.collection("reviews").document(review.firebaseId)
                if let placeID = review.placeID{
                    let reviewRefInPlace = self.db.collection("places").document(placeID).collection("reviews").document(review.firebaseId)
                    let placeIDRef = self.db.collection("places").document(placeID)
                    self.db.runTransaction({ (transation, error) -> Any? in
                        do{
                            let placeDocument = try transation.getDocument(placeIDRef)
                            if let place = CPRPlace(data: placeDocument.data()){
                                transation.setData(["place": place.parse()], forDocument: reviewRefInPlace, merge: true)
                                transation.setData(["place": place.parse()], forDocument: reviewRefInReviews, merge: true)
                                transation.setData(["place": place.parse()], forDocument: reviewInUserRef, merge: true)
                                transation.setData(placeDocument.data() ?? [:], forDocument: placeIDRef, merge: true)
                            }
                        }catch let error{
                            print(error)
                        }
                        return nil
                    }, completion: { _,error in
                        if let error = error{
                            print(error)
                        }
                    })
                }
            }
            return
        }
    }
}
