//
//  CoreDataService.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/16/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataService{
    
    init(){
        setupNotifications()
    }

    deinit{
        unregisterNotifications()
    }
    
    private func setupNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataService.saveContext), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CoreDataService.saveContext), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    private func unregisterNotifications(){
        NotificationCenter.default.removeObserver(self)
    }
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "CPRCoreData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    private var context : NSManagedObjectContext{
        return persistentContainer.viewContext
    }
    
    @objc func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            context.performAndWait {
                do {
                    try context.save()
                } catch {
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        }
    }
    
}

extension CoreDataService{
    
    
    func getPlaces(byPlaceId: [String]) -> [Place]?{
        return nil
    }
    
    func savePlace(placeId: String, coordinates: (lat: Double, lon:Double), name: String, address: String, globalCode: String, googleRating: Double, priceLevel: Int16, photos : [PlacePhotos]){
        let place = Place(context: context)
        place.placeId = placeId
        place.latitude = coordinates.lat
        place.longitude = coordinates.lon
        place.name = name
        place.address = address
        place.globalCode = globalCode
        place.googleRating = googleRating
        place.priceLevel = priceLevel
        photos.forEach({$0.place = place; place.addToPhotos($0)})
    }
    
    func createPlacePhoto(photoReference: String, width: Int16, height: Int16) -> PlacePhotos{
        let placePhoto = PlacePhotos(context: context)
        placePhoto.photoReference = photoReference
        placePhoto.width = width
        placePhoto.height = height
        return placePhoto
    }
    
}
