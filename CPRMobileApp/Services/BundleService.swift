//
//  BundleHelper.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol HasBundleService{
    var bundleService: BundleService {get set}
}

class BundleService {
    
    func getGoogleCategoriesMapper() -> PlacesCategories?{
        guard let categoriesJSONFile = Bundle.main.path(forResource: "google_categories_mapper", ofType: "json") else {return nil}
        let urlPath = URL(fileURLWithPath: categoriesJSONFile)
        guard let data = try? Data(contentsOf: urlPath) else {return nil}
        do{
            let placesCategories = try JSONDecoder().decode(PlacesCategories.self, from: data)
            return placesCategories
        }catch(let error){
                print(error)
            return nil
        }
    }
    
    func parseFileToDictionaryArray(filename: String) -> [Dictionary<String, Any>]? {
        var dictionaryArray: [Dictionary<String, Any>]?
        if let welcomeObjectPlistPath = Bundle.main.path(forResource: filename, ofType: "plist"), let objectArray = NSArray(contentsOfFile: welcomeObjectPlistPath) {
            dictionaryArray = [Dictionary<String, Any>]()
            for currentObject in objectArray {
                if let dictionary = currentObject as? Dictionary<String, Any> {
                    dictionaryArray?.append(dictionary)
                }
            }
        }
        return dictionaryArray
    }
}
