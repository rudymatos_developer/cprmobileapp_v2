//
//  FirebaseAuthHelper.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import Firebase

typealias LoginCompletion = (Result<CPRUser>) -> Void
typealias ResetPasswordCompletion = (CPRError?) -> Void
typealias CreateUserCompletion = (Result<Bool>) -> Void

protocol HasAuthorizableService{
    var authorizableService : Authorizable {get set}
}

protocol Authorizable {
    func login(byEmail: String, password: String, completion : @escaping LoginCompletion)
    func createUser(user: CPRUser, password: String, completion: @escaping CreateUserCompletion)
    func getEmail() -> String?
    func doesUserHaveBusinessProfile(email: String, completion: @escaping (Bool) -> Void )
    func isUserLoggedIn() -> Bool
    func resetPassword(byEmail: String, completion: @escaping ResetPasswordCompletion)
    func logout()
}

class FirebaseAuthService: HasDataService, Authorizable {
    
    var dataService : FirebaseDataService
    
    init(dataService: FirebaseDataService) {
        self.dataService = dataService
    }
    
    
    private func loginFromSocial(authCredentials: AuthCredential, loginType: LoginTypes, completion: @escaping LoginCompletion){
//        Auth.auth().signInAndRetrieveData(with: authCredentials) { [weak self] (authResult, error) in
//            if let error = error {
//                let cprError = CPRError(message: "Error trying to access to CPR with message : \(error.localizedDescription)", errorType: ErrorType.invalidAccessException)
//                completion(.error(cprError))
//                return
//            }
//
//            guard let fullName = authResult?.user.displayName, let photoURL = authResult?.user.photoURL else{
//                let cprError = CPRError(message: "Invalid user data from OAuth2", errorType: ErrorType.invalidAccessException)
//                completion(.error(cprError))
//                return
//            }
//
//            let userKey = authResult?.user.email ?? fullName.components(separatedBy: .whitespaces).joined().lowercased()
//            self?.dataService.getUserData(byUserKey: userKey, completion: { (user, error) in
//                if let error = error{
//                    if error.errorType == .invalidUserData {
//                        URLSession.shared.dataTask(with: photoURL, completionHandler: {(data, response, error) in
//                            var profileImage : UIImage? = nil
//                            if let data = data{
//                                profileImage = UIImage(data: data)
//                            }
//                            self?.dataService.assignNewProfilePictureFromSocial(image: profileImage, completion: { (profilePictureName, profilePictureURL) in
//                                let pictureName = profilePictureName ?? ""
//                                let pictureURL = profilePictureURL?.absoluteString ?? ""
//                                let cprUser = CPRUser(email: userKey, fullName: fullName, profilePicture: pictureName, profilePictureURL: pictureURL, loginUsing : loginType)
////                                self?.dataService.createNewUser(user: cprUser, completion: { (error) in
////                                    if let error = error{
////                                        completion(nil,error)
////                                        return
////                                    }
////                                    completion(cprUser, nil)
////                                    return
////                                })
//                            })
//                        }).resume()
//                    }else{
//                        let cprError = CPRError(message: "Error trying to access to CPR with message : \(error.message)", errorType: ErrorType.invalidAccessException)
//                        completion(.error(cprError))
//                        return
//                    }
//                }else{
//                   completion(.error(cprError))
//                }
//            })
//        }
    }
    
    func login(byEmail: String, password: String, completion : @escaping LoginCompletion) {
        Auth.auth().signIn(withEmail: byEmail, password: password) { [weak self] (user, error) in
            if let error = error {
                let cprError = CPRError(message: "Error trying to access to CPR with message : \(error.localizedDescription)", errorType: ErrorType.invalidAccessException)
                completion(.error(cprError))
            } else {
                self?.dataService.getUserData(byUserKey: byEmail, completion: { (user, error) in
                    guard let user = user , error == nil else{
                        completion(.error(error!))
                        return
                    }
                    completion(.success(user))
                })
            }
        }
    }
    
    func resetPassword(byEmail: String, completion: @escaping ResetPasswordCompletion) {
        Auth.auth().sendPasswordReset(withEmail: byEmail) { (error) in
            var cprError: CPRError? = nil
            if let error = error {
                cprError = CPRError(message: error.localizedDescription, errorType: ErrorType.invalidUserData)
            }
            completion(cprError)
        }
    }
    
    func getEmail() -> String? {
        return Auth.auth().currentUser?.email
    }
    
    func doesUserHaveBusinessProfile(email: String, completion: @escaping (Bool) -> Void ) {
        dataService.isUserBusiness(email: email, completion: completion)
    }
    
    func isUserLoggedIn() -> Bool {
        return Auth.auth().currentUser != nil
    }
    
    func logout() {
        do {
            try Auth.auth().signOut()
        } catch let error as NSError {
            print("error signing out the user with message : \(error.localizedDescription)")
        }
    }
    
    func createUser(user: CPRUser, password: String, completion: @escaping CreateUserCompletion) {
        Auth.auth().createUser(withEmail: user.email, password: password) { [weak self] (_, error) in
            if let error = error {
                let cprError = CPRError(message: "Invalid Login. \(error.localizedDescription)", errorType: .invalidUserData)
                completion(.error(cprError))
                return
            }
            self?.dataService.createNewUser(user: user, completion: completion)
        }
    }
}
