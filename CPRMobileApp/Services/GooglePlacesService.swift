//
//  GooglePlacesService.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/16/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import CoreLocation

protocol HasGooglePlaceService{
    var googlePlacesService : GooglePlaceService? {get set}
}

class GooglePlaceService: NSObject{
    
    var getCurrentLocationCompletion : ((Result<(latitude:CLLocationDegrees, longitude:CLLocationDegrees)>) -> Void)?
    lazy var locationManager: CLLocationManager? = {
        return CLLocationManager()
    }()
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
}


extension GooglePlaceService{
    
    private func generateSearchNearByPlacesURL(byCurrentLocation location:(latitude: CLLocationDegrees, longitude: CLLocationDegrees), andCategory category: String, nextPageToken : String? = nil) -> URLRequest?{
        let basedURLString = CPRURLs.GooglePlaces.AdditionalURLS.nearBySearchURL
        guard var components = URLComponents(string: basedURLString) else {return nil}
        let locationQueryItem = URLQueryItem(name: "location", value: "\(location.latitude),\(location.longitude)")
        let radiousQueryItem = URLQueryItem(name: "radius", value: "\(CPRURLs.GooglePlaces.defaultDistanceInMeters)")
        let keywordQueryItem = URLQueryItem(name: "keyword", value: category)
        let keyQueryItem = URLQueryItem(name: "key", value: CPRURLs.GooglePlaces.key)
        components.queryItems = [locationQueryItem,radiousQueryItem,keywordQueryItem,keyQueryItem]
        if let nextPageToken = nextPageToken{
            let nextPageToken = URLQueryItem(name: "pageToken", value: nextPageToken)
            components.queryItems?.append(nextPageToken)
        }
        guard let url = components.url else {return nil}
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.httpMethod = "GET"
        return urlRequest
    }
    
    private func generateGetPlaceInfo(byPlaceId: String) -> URLRequest?{
        let basedURLString = CPRURLs.GooglePlaces.AdditionalURLS.getPlaceDetails
        guard var components = URLComponents(string: basedURLString) else {return nil}
        let fieldsQueryItem = URLQueryItem(name: "fields", value: "\(CPRURLs.GooglePlaces.placesDetailsFields)")
        let placeIdQueryItem = URLQueryItem(name: "placeid", value: "\(byPlaceId)")
        let keyQueryItem = URLQueryItem(name: "key", value: CPRURLs.GooglePlaces.key)
        components.queryItems = [fieldsQueryItem,placeIdQueryItem,keyQueryItem]
        guard let url = components.url else {return nil}
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.httpMethod = "GET"
        return urlRequest
    }
    
    private func generateGetPhotoURL(byPhotoReference: String) -> URLRequest?{
        let basedURLString = CPRURLs.GooglePlaces.AdditionalURLS.getPhotoByReferenceURL
        guard var components = URLComponents(string: basedURLString) else {return nil}
        let photoRerefenceQI = URLQueryItem(name: "photoreference", value: byPhotoReference)
        let maxWidthQI = URLQueryItem(name: "maxwidth", value: "400")
        let keyQI = URLQueryItem(name: "key", value: CPRURLs.GooglePlaces.key)
        components.queryItems = [maxWidthQI,photoRerefenceQI,keyQI]
        guard let url = components.url else {return nil}
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.httpMethod = "GET"
        return urlRequest
    }
    
    func getPhoto(byReference reference: String, completion: @escaping((Result<Data>) -> Void)){
        guard let urlRequest = generateGetPhotoURL(byPhotoReference: reference) else {
            let urlRequestError = CPRError(message: "Error getting URLRequest", errorType: .errorGettingData)
            completion(.error(urlRequestError))
            return
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == 200, error == nil else{
                let errorGettingData = CPRError(message: "Error getting Data from Google Service with Message: \(error.debugDescription)", errorType: .errorGettingData)
                completion(.error(errorGettingData))
                return
            }
            completion(.success(data))
            }.resume()
    }
    
    func getNearByPlaces(byCurrentLocation location:(latitude: CLLocationDegrees, longitude: CLLocationDegrees), andCategory category: String, completion: @escaping ((Result<GooglePlaceResponse>) -> Void)){
        guard let urlRequest = generateSearchNearByPlacesURL(byCurrentLocation: location, andCategory: category) else {
            let urlRequestError = CPRError(message: "Error getting URLRequest", errorType: .errorGettingData)
            completion(.error(urlRequestError))
            return
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == 200, error == nil else{
                let errorGettingData = CPRError(message: "Error getting Data from Google Service with Message: \(error.debugDescription)", errorType: .errorGettingData)
                completion(.error(errorGettingData))
                return
            }
            do{
                let results = try JSONDecoder().decode(GooglePlaceResponse.self, from: data)
                completion(.success(results))
            }catch(let error){
                let errorGettingData = CPRError(message: "Error parsing data from Google Service with Message: \(error)", errorType: .errorGettingData)
                completion(.error(errorGettingData))
            }
        }.resume()
    }
    
    func getPlaceDetails(byPlaceId: String, completion: @escaping ((Result<GooglePlaceInfoResponse>) -> Void)){
        guard let urlRequest = generateGetPlaceInfo(byPlaceId: byPlaceId) else {
            let urlRequestError = CPRError(message: "Error getting URLRequest", errorType: .errorGettingData)
            completion(.error(urlRequestError))
            return
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, let httpResponse = (response as? HTTPURLResponse), httpResponse.statusCode == 200, error == nil else{
                let errorGettingData = CPRError(message: "Error getting Data from Google Service with Message: \(error.debugDescription)", errorType: .errorGettingData)
                completion(.error(errorGettingData))
                return
            }
            do{
                let results = try JSONDecoder().decode(GooglePlaceInfoResponse.self, from: data)
                completion(.success(results))
            }catch(let error){
                let errorGettingData = CPRError(message: "Error parsing data from Google Service with Message: \(error)", errorType: .errorGettingData)
                completion(.error(errorGettingData))
            }
            }.resume()
    }
    
}

extension GooglePlaceService{
    
    func getCurrentLocation(){
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined{
                locationManager?.requestWhenInUseAuthorization()
            }
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.delegate = self
            locationManager?.startUpdatingLocation()
        }else{
            let error = CPRError(message: "Error getting Location Services. Make sure to turn your GPS On.", errorType: .errorGettingData)
            getCurrentLocationCompletion?(.error(error))
        }
        
    }
}


extension GooglePlaceService: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("location error: \(error)")
        let cprError = CPRError(message: "Error getting Location Services. \(error.localizedDescription)).", errorType: .errorGettingData)
        getCurrentLocationCompletion?(.error(cprError))

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("in here")
        self.locationManager?.stopUpdatingLocation()
        self.locationManager?.delegate = nil
        guard let coordinates = locations.first?.coordinate else {
            let error = CPRError(message: "Error getting Location Services. Invalid Coordinates.", errorType: .errorGettingData)
            getCurrentLocationCompletion?(.error(error))
            return
        }
        getCurrentLocationCompletion?(.success((latitude:coordinates.latitude, longitude: coordinates.longitude)))
    }
    
}
