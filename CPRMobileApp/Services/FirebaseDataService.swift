//
//  FirebaseHelper.swift
//  cpr
//
//  Created by Rudy E Matos on 11/19/17.
//  Copyright © 2017 Bearded Gentleman. All rights reserved.
//

import Foundation
import Firebase

protocol HasDataService{
    var dataService: FirebaseDataService {get set}
}

class FirebaseDataService: CPRDataFormatter {
    
    private let db : Firestore = {
        let db = Firestore.firestore()
        let settings = db.settings
        db.settings = settings
        return db
    }()
    
    private let storage = Storage.storage().reference()
    private let cache = NSCache<NSString, UIImage>()
    
    func downloadPicture(firebaseId: String, withName: String, completion : @escaping (UIImage?) -> Void) {
        if let currentImage = cache.object(forKey: withName as NSString) {
            completion(currentImage)
        } else {
            let downloadRef = storage.child("\(CPRConstants.EndPoints.imagesRoot)/\(firebaseId)").child(withName)
            downloadRef.getData(maxSize: 5*1024*1024, completion: { (data, error) in
                if let error = error {
                    print(error)
                } else {
                    guard let currentImage = UIImage(data: data!) else {
                        completion(nil)
                        return 
                    }
                    self.cache.setObject(currentImage, forKey: withName as NSString)
                    completion(currentImage)
                }
            })
        }
    }
    
    func downloadPictures(fromReview review: CPRReview, completion : @escaping ([UIImage]?) -> Void) {
        if let imagesNamesFromReview = review.imagesNames {
            var currentDownloadCounter = 0
            if imagesNamesFromReview.count > 0 {
                var images = [UIImage]()
                for currentImageName  in imagesNamesFromReview {
                    if let currentImage = cache.object(forKey: currentImageName as NSString) {
                        images.append(currentImage)
                        currentDownloadCounter += 1
                        if(imagesNamesFromReview.count == currentDownloadCounter) {
                            completion(images)
                        }
                    } else {
                        let downloadRef = storage.child("\(CPRConstants.EndPoints.imagesRoot)/\(review.firebaseId)").child(currentImageName)
                        downloadRef.getData(maxSize: 5*1024*1024, completion: { (data, error) in
                            if let error = error {
                                print(error)
                                currentDownloadCounter += 1
                            } else {
                                if let currentImage = UIImage(data: data!) {
                                    images.append(currentImage)
                                    self.cache.setObject(currentImage, forKey: currentImageName as NSString)
                                    currentDownloadCounter += 1
                                    
                                }
                            }
                            if(imagesNamesFromReview.count == currentDownloadCounter) {
                                completion(images)
                            }
                        })
                    }
                }
            } else {
                completion(nil)
            }
        } else {
            completion(nil)
        }
    }
    
    func getAllPlaces(byBusinessOwner: String, completion :@escaping ([CPRPlace]) -> Void) {
        var places = [CPRPlace]()
        let query = db.collection(CPRConstants.EndPoints.placesRoot).whereField(CPRConstants.PlaceFields.businessOwner, isEqualTo: byBusinessOwner)
        query.getDocuments { (placesDocuments, _) in
            if let placesDocuments = placesDocuments, placesDocuments.documents.count > 0 {
                places = placesDocuments.documents.compactMap({CPRPlace(data: $0.data())})
            }
            completion(places)
        }
    }
    
    func getAllReviews(completion: @escaping (Result<[CPRReview]>) -> Void){
        let reviewsRef = db.collection(CPRConstants.EndPoints.reviewRoot)
        reviewsRef.getDocuments { snapshot, error in
            guard let reviews =  snapshot?.documents.compactMap({CPRReview.init(data: $0.data())}), error == nil else{
                let cprError = CPRError(message: "Error getting reviews with messages: \(error!.localizedDescription)", errorType: ErrorType.errorGettingData)
                completion(.error(cprError))
                return
            }
            completion(.success(reviews))
        }
    }
    
    func getAllReview(byPlace place: CPRPlace, limit: Int, completion : @escaping (Result<[CPRReview]>) -> Void){
        let placeReviewsRef = db.collection(CPRConstants.EndPoints.placesRoot).document(place.googlePlaceID).collection(CPRConstants.PlaceFields.reviews).limit(to: limit)
        placeReviewsRef.getDocuments { snapshot, error in
            guard let reviews =  snapshot?.documents.compactMap({CPRReview.init(data: $0.data())}), error == nil else{
                let cprError = CPRError(message: "Error getting reviews with messages: \(error!.localizedDescription)", errorType: ErrorType.errorGettingData)
                completion(.error(cprError))
                return
            }
            completion(.success(reviews))
        }
    }
    
    func getAllReviews(byUser user: CPRUser, completion : @escaping (Result<[CPRReview]>) -> Void) {
        let reviewsRef = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email).collection(user.isBusiness ? CPRConstants.UserFields.businessReviews : CPRConstants.UserFields.reviews)
        reviewsRef.getDocuments { (snapshot, error) in
            if let error = error {
                let cprError = CPRError(message: "Error getting reviews with messages: \(error.localizedDescription)", errorType: ErrorType.errorGettingData)
                completion(.error(cprError))
            } else {
                let reviews = snapshot?.documents.compactMap({ (document) -> CPRReview? in
                    return CPRReview(data: document.data())
                })
                completion(.success(reviews ?? []))
            }
        }
    }
    
    func save(review: CPRReview, images: [UIImage?]?,  completion : @escaping () -> Void) {
        let review = review
        let documentRef = db.collection(CPRConstants.EndPoints.reviewRoot).document()
        review.firebaseId = documentRef.documentID
        if let imagesNamesDictionary = review.imagesNames, let images = images, imagesNamesDictionary.count > 0 {
            var currentUploadCounter = 0
            var downloadURLs = [String]()
            for (index, imageName) in imagesNamesDictionary.enumerated() {
                if let currentImage = images[index], let uploadImage = currentImage.jpegData(compressionQuality: 0.25) {
                    let storageRef = storage.child("\(CPRConstants.EndPoints.imagesRoot)/\(review.firebaseId)").child(imageName)
                    let metadata = StorageMetadata()
                    metadata.contentType = "image/jpg"
                    storageRef.putData(uploadImage, metadata: metadata) { (metadata, error) in
                        if let error = error {
                            print(error)
                            return
                        }
                        storageRef.downloadURL(completion: { (url, error) in
                            currentUploadCounter += 1
                            if let url = url{
                                downloadURLs.append(url.absoluteString)
                            }
                            if currentUploadCounter == images.count {
                                currentUploadCounter = 0
                                review.imagesDownloadURLs = downloadURLs
                                self.persistReview(review: review, completion: completion)
                            }
                        })
                    }
                }
            }
        } else {
            persistReview(review: review, completion: completion)
        }
    }
}

// MARK: - User Management
extension FirebaseDataService {
    
    func assignNewProfilePictureFromSocial(image: UIImage?, completion : @escaping (String?,URL?) -> Void){
        guard let image = image else{
            completion(nil,nil)
            return
        }
        let metadata = StorageMetadata()
        let imageName = "\(generateUUID()).jpg"
        metadata.contentType = "image/jpg"
        if let imageToUpload = image.jpegData(compressionQuality: 0.25){
            let storageRef = storage.child(CPRConstants.EndPoints.profileImages).child(imageName)
            storageRef.putData(imageToUpload, metadata: metadata, completion: { (metadata, error) in
                if error != nil {
                    completion(nil,nil)
                }
                storageRef.downloadURL{ url, error in
                    guard let url = url, error == nil else {
                        completion(nil, nil)
                        return
                    }
                    completion(imageName, url)
                }
            })
        }
    }
    
    func assignNewProfileImage(email: String, image: UIImage, completion : @escaping (CPRError?) -> Void) {
        let currentUserDocument = db.collection(CPRConstants.EndPoints.usersRoot).document(email)
        let metadata = StorageMetadata()
        let imageName = "\(generateUUID()).jpg"
        metadata.contentType = "image/jpg"
        if let imageToUpload = image.jpegData(compressionQuality: 0.25) {
            let storageRef = storage.child(CPRConstants.EndPoints.profileImages).child(imageName)
            storageRef.putData(imageToUpload, metadata: metadata, completion: { (metadata, error) in
                if let error = error {
                    let cprError = CPRError(message: "Error uploading new profile picture with message: \(error.localizedDescription)", errorType: ErrorType.errorSavingUserData)
                    completion(cprError)
                }
                storageRef.downloadURL{ url, error in
                    guard let url = url, error == nil else {
                        let cprError = CPRError(message: "Error updating userInfo picture with message: \(error!.localizedDescription)", errorType: ErrorType.errorSavingUserData)
                        completion(cprError)
                        return
                    }
                    currentUserDocument.updateData([CPRConstants.UserFields.profilePicture: imageName, CPRConstants.UserFields.profilePictureURL: url.absoluteString], completion: { _ in
                        completion(nil)
                    })
                }
            })
        }
    }
    
    func updateUser(user: CPRUser){
        let documentRef = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email)
        documentRef.setData(user.parse(), merge: true)
    }
    
    func createNewUser(user: CPRUser, completion :  @escaping (Result<Bool>) -> Void) {
        let documentRef = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email)
        documentRef.setData(user.parse()) { (error) in
            if let error = error {
                let cprError = CPRError(message: "Error saving user data with message: \(error.localizedDescription)", errorType: .invalidUserData)
                completion(.error(cprError))
            } else {
                completion(.success(true))
            }
        }
    }
    
    func downloadProfilePicture(byImageName: String, completion : @escaping (UIImage?, CPRError?) -> Void) {
        if let imageFromCache = cache.object(forKey: byImageName as NSString) {
            completion(imageFromCache, nil)
        } else {
            let storageRef = storage.child(CPRConstants.EndPoints.profileImages).child(byImageName)
            storageRef.getData(maxSize: 5*1024*1024, completion: { (data, error) in
                guard let data = data, let image = UIImage(data: data) else {
                    var cprError = CPRError(message: "Error getting user's profile picture", errorType: ErrorType.invalidUserData)
                    if let error = error {
                        cprError = CPRError(message: "Error getting user's profile picture with message: \(error.localizedDescription)", errorType: ErrorType.invalidUserData)
                    }
                    completion(nil, cprError)
                    return
                }
                self.cache.setObject(image, forKey: byImageName as NSString)
                completion(image, nil)
            })
        }
    }
    
    func isUserBusiness(email: String, completion: @escaping (Bool) -> Void) {
        let userDocumentRef = db.collection(CPRConstants.EndPoints.usersRoot).document(email)
        userDocumentRef.getDocument { (document, error) in
            if error != nil {
                completion(false)
            } else {
                if let isBusiness = document?.data()?[CPRConstants.UserFields.isBusiness] as? Bool, isBusiness {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        }
    }
    
    func getUserData(byUserKey userKey: String, completion : @escaping (CPRUser?, CPRError?) -> Void) {
        let userDocumentRef = db.collection(CPRConstants.EndPoints.usersRoot).document(userKey)
        userDocumentRef.getDocument { (document, error) in
            if let error = error {
                let cprError = CPRError(message: "Error getting userData with message: \(error.localizedDescription)", errorType: .invalidUserData)
                completion(nil, cprError)
            } else {
                if let userData =   CPRUser(data: document?.data()) {
                    completion(userData, nil)
                } else {
                    let cprError = CPRError(message: "Invalid User Data", errorType: ErrorType.invalidUserData)
                    completion(nil, cprError)
                }
            }
        }
    }
}

// MARK: - Places Management
extension FirebaseDataService {
    
    func removePlace(fromUser: CPRUser, placeToRemove place: CPRPlace, completion : @escaping () -> Void) {
        db.collection(CPRConstants.EndPoints.placesRoot).whereField(CPRConstants.PlaceFields.googlePlaceID, isEqualTo: place.googlePlaceID).getDocuments { (placeDocuments, error) in
            self.db.runTransaction({ (transaction, _) -> Any? in
                do {
                    let userRef = self.db.collection(CPRConstants.EndPoints.usersRoot).document(fromUser.email)
                    let userDocument = try transaction.getDocument(userRef)
                    var tempReviews = [[String: Any]]()
                    
                    if let businessReviews = userDocument.data()?[CPRConstants.UserFields.businessReviews] as? [[String: Any]] {
                        for currentBusinessReview in businessReviews {
                            if currentBusinessReview[CPRConstants.ReviewFields.placeID.rawValue] as? String != place.googlePlaceID {
                                tempReviews.append(currentBusinessReview)
                            }
                        }
                        transaction.updateData([CPRConstants.UserFields.businessReviews: tempReviews], forDocument: userRef)
                    }
                } catch {
                }
                if let placeDocuments = placeDocuments, placeDocuments.documents.count > 0, let currentPlaceDocument = placeDocuments.documents.first {
                    currentPlaceDocument.reference.delete()
                }
                return nil
            }, completion: { (_, _) in
                completion()
            })
        }
    }
    
    func addPlaceIfNeeded(place: CPRPlace, withReview review : CPRReview, completion: @escaping(Result<Bool>) -> Void){
        db.collection(CPRConstants.EndPoints.placesRoot).whereField(CPRConstants.PlaceFields.googlePlaceID, isEqualTo: place.googlePlaceID).getDocuments(completion: { (placesDocuments, error) in
            self.db.runTransaction({ transaction, errorPointer -> Any? in
                let placeRef: DocumentReference!
                let placeReviewRef: DocumentReference!
                if let documentSnapshot = placesDocuments?.documents.first, let _ = CPRPlace(data: documentSnapshot.data()){
                    placeRef = self.db.collection(CPRConstants.EndPoints.placesRoot).document(documentSnapshot.documentID)
                    placeReviewRef = self.db.collection(CPRConstants.EndPoints.placesRoot).document(documentSnapshot.documentID).collection(CPRConstants.PlaceFields.reviews).document(review.firebaseId)
                }else{
                    placeRef = self.db.collection(CPRConstants.EndPoints.placesRoot).document(place.googlePlaceID)
                    placeReviewRef = self.db.collection(CPRConstants.EndPoints.placesRoot).document(place.googlePlaceID).collection(CPRConstants.PlaceFields.reviews).document(review.firebaseId)
                }
                transaction.setData(place.parse(), forDocument: placeRef, merge: true)
                transaction.setData([CPRConstants.PlaceFields.lastReviewDate: Date()], forDocument: placeRef, merge: true)
                transaction.setData(review.parse(), forDocument: placeReviewRef, merge: true)
                return nil
            }, completion: { _,error in
                guard error == nil else{
                    let cprError = CPRError(message: error!.localizedDescription, errorType: ErrorType.errorSavingReview)
                    completion(.error(cprError))
                    return
                }
                completion(.success(true))
            })
        })
    }
    
    func getAllPlaces(byCategory: String, limit: Int, completion : @escaping (Result<[CPRPlace]>) -> Void){
        db.collection(CPRConstants.EndPoints.placesRoot).whereField("categories", arrayContains: byCategory).order(by: "lastReviewDate", descending: true).limit(to: limit).getDocuments { snapshot, error in
            guard error == nil else{
                let cprError = CPRError(message: "No places to display with message: \(error!.localizedDescription)", errorType: .errorGettingData)
                completion(.error(cprError))
                return
            }
            guard let places = snapshot?.documents.compactMap({CPRPlace.init(data: $0.data())}),places.count > 0 else{
                let cprError = CPRError(message: "No places to display", errorType: .errorGettingData)
                completion(.error(cprError))
                return
            }
            completion(.success(places))
        }
    }
    
    func addPlace(toUser user: CPRUser, placeToAdd place: CPRPlace, completion: @escaping (CPRError? ) -> Void) {
        db.collection(CPRConstants.EndPoints.placesRoot).whereField(CPRConstants.PlaceFields.googlePlaceID, isEqualTo: place.googlePlaceID).getDocuments(completion: { (placesDocuments, error) in
            if let placesDocuments = placesDocuments, let businessOwner = placesDocuments.documents.first?.data()[CPRConstants.PlaceFields.businessOwner] as? String {
                if businessOwner == user.email {
                    completion(CPRError(message: "Place is already registered under YOUR user", errorType: ErrorType.placeAlreadyRegistered))
                } else {
                    completion(CPRError(message: "Place is already registered under ANOTHER user", errorType: ErrorType.placeAlreadyRegistered))
                }
            } else {
                self.db.collection(CPRConstants.EndPoints.reviewRoot).whereField(CPRConstants.ReviewFields.placeID.rawValue, isEqualTo: place.googlePlaceID).getDocuments(completion: { (reviewsByPlaceIDDocuments, error) in
                    self.db.runTransaction({ (transaction, _) -> Any? in
                        
                        //                        let userRef = self.db.collection(CPRConstants.EndPoints.usersRoot).document(user.email)
                        let placeRef = self.db.collection(CPRConstants.EndPoints.placesRoot).document()
                        
                        place.businessOwner = user.email
                        transaction.setData(place.parse(), forDocument: placeRef)
                        
                        if let reviewsByPlaceIDDocuments = reviewsByPlaceIDDocuments, reviewsByPlaceIDDocuments.count > 0 {
                            
                            var reviewsByPlacesID = reviewsByPlaceIDDocuments.documents.map({$0.data()})
                            reviewsByPlacesID = reviewsByPlacesID.compactMap({ (value) -> [String: Any] in
                                var newValue = value
                                newValue["businessDisplayName"] = place.displayName
                                return newValue
                            })
                            
                            //                            var reviewsToMigrate = user.businessReviews?.compactMap({$0.parse()}) ?? [[String: Any]]()
                            //                            reviewsToMigrate.append(contentsOf: reviewsByPlacesID)
                            //
                            //                            transaction.updateData([CPRConstants.PlaceFields.reviews: reviewsByPlacesID], forDocument: placeRef)
                            //                            transaction.updateData([CPRConstants.UserFields.businessReviews: reviewsToMigrate], forDocument: userRef)
                        }
                        return nil
                    }, completion: { (_, _) in
                        completion(nil)
                    })
                })
            }
        })
        
    }
}

extension FirebaseDataService{
    
    func getPlaces(fromCollection collectionName: String, fromUser user: CPRUser, limit: Int, completion: @escaping (Result<[CPRPlace]>) -> Void){
        db.collection(CPRConstants.EndPoints.usersRoot).document(user.email).collection(collectionName).order(by: "addedOn", descending: true).limit(to: limit).getDocuments { (snapshot, error) in
            guard error == nil else{
                let cprError = CPRError(message: "No places to display with message: \(error!.localizedDescription)", errorType: .errorGettingData)
                completion(.error(cprError))
                return
            }
            guard let places = snapshot?.documents.compactMap({CPRPlace.init(data: $0.data()["place"] as? [String:Any])}),places.count > 0 else{
                let cprError = CPRError(message: "No places to display", errorType: .errorGettingData)
                completion(.error(cprError))
                return
            }
            completion(.success(places))
        }
    }
    
    func removePlace(fromCollection collectionName: CPRConstants.UserFields.UserPlaces, user: CPRUser, place: CPRPlace){
        let document = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email).collection(collectionName.rawValue).document(place.googlePlaceID)
        document.delete()
    }
    
    func addPlace(toCollection collectionName: CPRConstants.UserFields.UserPlaces, user: CPRUser, place: CPRPlace){
        let document = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email).collection(collectionName.rawValue).document(place.googlePlaceID)
        document.setData(["addedOn" : Date(), "place" : place.parse()], merge: true)
    }
}

// MARK: - Review Management
extension FirebaseDataService {
    
    func archiveReview(review: CPRReview) {
        let userObjectRef = self.db.collection(CPRConstants.EndPoints.usersRoot).document(review.userID).collection(CPRConstants.UserFields.reviews).document(review.firebaseId)
        userObjectRef.updateData([CPRConstants.ReviewFields.archived.rawValue: true])
    }
    
    private func persistReview(review: CPRReview, completion: @escaping () -> Void){
        let placeID = review.placeID ?? "INVALID_PLACE_ID"
        let reviewRef = db.collection(CPRConstants.EndPoints.reviewRoot).document(review.firebaseId)
        let userRef = db.collection(CPRConstants.EndPoints.usersRoot).document(review.userID)
        let placesQueryRef = db.collection(CPRConstants.EndPoints.placesRoot).whereField(CPRConstants.PlaceFields.googlePlaceID, isEqualTo: placeID)
        let personalReviewRef = userRef.collection(CPRConstants.UserFields.reviews).document(review.firebaseId)
        placesQueryRef.getDocuments { (placesDocuments, error) in
            self.db.runTransaction({ (transaction, errorPointer) -> Any? in
                transaction.setData(review.parse(), forDocument: reviewRef)
                transaction.setData(review.parse(), forDocument: personalReviewRef)
                return nil
            }) { (_, error) in
                if let error = error {
                    print("errorMessage: \(error)")
                    return
                } else {
                    print("Transaction succesfully commited")
                    completion()
                }
            }
        }
    }
}

// MARK: - Notification Management
extension FirebaseDataService {
    
    func addNotificationToken(toUser user: CPRUser, fcmToken: String?, completion: @escaping (CPRError?) -> Void) {
        guard let fcmToken = fcmToken else {
            let error = CPRError(message: "Invalid Token to Register", errorType: .errorCreatingMapping)
            completion(error)
            return
        }
        let userObjectRef = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email)
        self.db.runTransaction({ (transaction, errorPointer) -> Any? in
            do {
                let userObject = try transaction.getDocument(userObjectRef)
                if var tokens = userObject.data()?[CPRConstants.UserFields.tokens] as? [String] {
                    tokens.append(fcmToken)
                    transaction.updateData([CPRConstants.UserFields.tokens: tokens], forDocument: userObjectRef)
                } else {
                    var tokens = [String]()
                    tokens.append(fcmToken)
                    transaction.setData([CPRConstants.UserFields.tokens: tokens], forDocument: userObjectRef, merge: true)
                }
            } catch let error as NSError {
                errorPointer?.pointee = error
                return nil
            }
            return nil
        }) { (_, error) in
            var cprError: CPRError? = nil
            if let error = error {
                cprError = CPRError(message: error.localizedDescription, errorType: ErrorType.errorCreatingMapping)
            }
            completion(cprError)
        }
    }
    
    func removeNotificationToken(from user: CPRUser, fcmToken: String?, completion: @escaping (CPRError?) -> Void) {
        guard let fcmToken = fcmToken else {
            let error = CPRError(message: "Invalid Token to Unregister", errorType: .errorCreatingMapping)
            completion(error)
            return
        }
        let userObjectRef = db.collection(CPRConstants.EndPoints.usersRoot).document(user.email)
        self.db.runTransaction({ (transaction, errorPointer) -> Any? in
            do {
                let userObject = try transaction.getDocument(userObjectRef)
                if var tokens = userObject.data()?[CPRConstants.UserFields.tokens] as? [String] {
                    if let index = tokens.firstIndex(of: fcmToken) {
                        tokens.remove(at: index)
                        transaction.updateData([CPRConstants.UserFields.tokens: tokens], forDocument: userObjectRef)
                    }
                }
            } catch let error as NSError {
                errorPointer?.pointee = error
                return nil
            }
            return nil
        }) { (_, error) in
            var cprError: CPRError? = nil
            if let error = error {
                cprError = CPRError(message: error.localizedDescription, errorType: ErrorType.errorCreatingMapping)
            }
            completion(cprError)
        }
    }
    
}
