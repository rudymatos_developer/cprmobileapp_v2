//
//  URL+Extension.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/22/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

extension URL{
    var typeIdentifier: String?{
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String?{
        return (absoluteString as NSString).lastPathComponent
    }
    
}
