//
//  UIColor+Extension.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/30/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit
extension UIColor {
    static let cprPinkColor = UIColor(named: "CPRPink")!
    static let cprGreenColor = UIColor(named: "CPRGreen")!
    static let cprBlueColor = UIColor(named: "CPRBlue")!
    static let disabledColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
    static let cprColors = [UIColor.cprGreenColor,UIColor.cprPinkColor,UIColor.cprBlueColor]
    
}
