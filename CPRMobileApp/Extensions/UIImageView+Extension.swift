//
//  UIImageView+Extension.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/30/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView{
    func loadImage(withURLString: String?, orLoadDefaultImageName: String? = nil, completion: (()->Void)? = nil){
        image = nil
        guard let urlString = withURLString , let url = URL(string: urlString) else{
            if let defaultImage = orLoadDefaultImageName{
                DispatchQueue.main.async {
                    self.image = UIImage(named: defaultImage)
                    completion?()
                }
            }
            return
        }
        if let image = imageCache.object(forKey: NSString(string: urlString)){
            DispatchQueue.main.async {
                self.image = image
                completion?()
            }
        }else{
            let dispatchQueue = DispatchQueue(label: "imageData", qos: .userInitiated)
            dispatchQueue.async { [weak self] in
                guard let imageData = try? Data(contentsOf: url) else {return}
                guard let image = UIImage(data: imageData) else {return}
                DispatchQueue.main.async {
                    self?.image = image
                }
                imageCache.setObject(image, forKey: NSString(string: urlString))
                completion?()
            }
        }
    }
    
    func loadGooglePlacesImage(reference: String?, orLoadDefaultImageName: String? = nil, completion: (()->Void)? = nil){
        guard let reference = reference , let urlRequest = generateGetPhotoURL(byPhotoReference: reference) else{
            if let defaultImage = orLoadDefaultImageName{
                DispatchQueue.main.async {
                    self.image = UIImage(named: defaultImage)
                    completion?()
                }
            }
            return
        }
        loadImage(withURLString: urlRequest.url?.absoluteString, orLoadDefaultImageName: orLoadDefaultImageName, completion: completion)
    }
    
    private func generateGetPhotoURL(byPhotoReference: String) -> URLRequest?{
        let basedURLString = CPRURLs.GooglePlaces.AdditionalURLS.getPhotoByReferenceURL
        guard var components = URLComponents(string: basedURLString) else {return nil}
        let photoRerefenceQI = URLQueryItem(name: "photoreference", value: byPhotoReference)
        let maxWidthQI = URLQueryItem(name: "maxwidth", value: "400")
        let keyQI = URLQueryItem(name: "key", value: CPRURLs.GooglePlaces.key)
        components.queryItems = [maxWidthQI,photoRerefenceQI,keyQI]
        guard let url = components.url else {return nil}
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.httpMethod = "GET"
        return urlRequest
    }
    
}
