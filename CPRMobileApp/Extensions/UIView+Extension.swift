//
//  UIView+Extension.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/21/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit


protocol DocumentIterable{
    func saveTempFileAndGetURL(image : UIImage) -> URL?
}

extension DocumentIterable{
    func saveTempFileAndGetURL(image : UIImage) -> URL?{
        let tempImage = "tempImage.png"
        var pathURL = FileManager.default.temporaryDirectory
        guard let imageData = image.jpegData(compressionQuality: 1) else {
            return nil
        }
        pathURL = pathURL.appendingPathComponent(tempImage)
        do{
            try imageData.write(to: pathURL)
        }catch (let error){
            print(error)
            return nil
        }
        return pathURL
    }
}

protocol CardCreator: class {
    func createCard(shadowView: UIView, mainView: UIView, cornerRadius: CGFloat, offSet: CGSize)
}

extension CardCreator {
    func createCard(shadowView: UIView, mainView: UIView, cornerRadius: CGFloat = 10,offSet: CGSize = CGSize(width: 3, height: 3)) {
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = offSet
        shadowView.layer.shadowRadius = 4
        shadowView.layer.shadowOpacity = 0.7
        shadowView.layer.masksToBounds = false
        mainView.layer.cornerRadius = cornerRadius
        mainView.layer.masksToBounds = true
    }
}
