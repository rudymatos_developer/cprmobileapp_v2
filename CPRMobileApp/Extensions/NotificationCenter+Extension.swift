//
//  NotificationCenter+Extension.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/17/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let addPlace = Notification.Name("addPlace")
    static let removePlace = Notification.Name("removePlace")
    static let refreshUserReviews = Notification.Name("refreshUserReviews")
    static let refreshUserPlaces = Notification.Name("refreshUserPlaces")
    static let optionSelectorDataLoaded = Notification.Name("optionSelectorDataLoaded")
}
