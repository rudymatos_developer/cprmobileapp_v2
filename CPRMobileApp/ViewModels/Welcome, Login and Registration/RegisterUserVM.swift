//
//  CreateUserViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/30/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol Registrator {
    
    var firstName: String? {get}
    var surname: String? {get}
    var email: String? {get}
    var password: String? {get}
    var confirmPassword: String? {get}
    var agreeWithTerms: DynamicType<Bool> {get}
    
    mutating func toggleAgreeWithTerms()
    func register(completion: @escaping  (Result<Bool>) -> Void)
   
}

class RegisterUserVM: NSObject, Registrator, HasDependencies, CPRDataValidator {
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    typealias Dependencies = HasDataService & HasAuthorizableService & HasUserDefaultService & HasLoadUserInfoService & HasUser
    var dependencies: Dependencies?
    private(set) weak var loginCoordinator: LoginCoordinator?
    
    var firstName: String?
    var surname: String?
    var email: String?
    var password: String?
    var confirmPassword: String?
    var agreeWithTerms: DynamicType<Bool> = DynamicType<Bool>(false)
    
    init(dependencies: Dependencies, loginCoordinator: LoginCoordinator) {
        self.loginCoordinator = loginCoordinator
        self.dependencies = dependencies
    }
    
    func toggleAgreeWithTerms() {
        agreeWithTerms.value = !agreeWithTerms.value
    }
    
    private lazy var user: CPRUser = CPRUser(userCreator: self)
    
    func register(completion: @escaping  (Result<Bool>) -> Void) {
        
        guard let email = email, isValidEmail(email: email), !isEmpty(firstName), !isEmpty(surname) else {
            completion(.error(CPRError(message: "Required Data Missing. Please fill all the fields.", errorType: .invalidUserData)))
            return
        }
        
        guard isValidPassword(password) && isValidPassword(confirmPassword) else {
            completion(.error(CPRError(message: "Invalid Password. Please make sure your password is at least 8 characters long", errorType: .invalidUserData)))
            return
        }
        
        guard let password = password, password == confirmPassword else{
            completion(.error(CPRError(message: "Invalid Passwords. Passwords don't match", errorType: .invalidUserData)))
            return
        }
        
        if !agreeWithTerms.value {
            completion(.error(CPRError(message: "You are not agree with terms.", errorType: .invalidUserData)))
            return
        }
        
        let user = CPRUser(userCreator: self)
        dependencies?.userDefaultService.createUserInfoInApp(user: user)
        dependencies?.authorizableService.createUser(user: user, password: password){ [weak self] result in
            switch result{
            case .success:
                let dispatchGroup = DispatchGroup()
                dispatchGroup.enter()
                self?.addNotificationToken { (error) in
                    if let error = error {
                        print(error.message)
                    }
                    dispatchGroup.leave()
                }
                dispatchGroup.enter()
                self?.dependencies?.authorizableService.login(byEmail: email, password: password) { _ in
                    dispatchGroup.leave()
                }
                dispatchGroup.notify(queue: .main){
                    self?.dependencies?.loadUserInfoService?.loadUserInfo{ result in
                        switch result{
                        case .success(let user):
                            self?.dependencies?.currentUser = user
                            completion(.success(true))
                        case .error(let error):
                            completion(.error(error))
                        }
                    }
                }
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
    func callRegistrationCompletion(){
        loginCoordinator?.loginCoordinatorSuccessfullyCompletion?(user.isBusiness ? .business : .normal)
        loginCoordinator?.parent?.removeChild(coordinator: &loginCoordinator)
    }
    
    func addNotificationToken(completion: @escaping ((CPRError?)->Void)){
        let deviceToken = dependencies?.userDefaultService.getDeviceFMCToken()
        dependencies?.dataService.addNotificationToken(toUser: user, fcmToken: deviceToken, completion: completion)
    }
    
}
