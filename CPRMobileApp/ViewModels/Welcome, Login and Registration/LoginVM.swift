//
//  LoginViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol Loginable {
    var username: String? { get }
    var password: String? { get }
    var emailToReset: String? {get}
    func login(completion: @escaping ((Result<CPRUser>) -> Void))
    func resetPassword(completion: @escaping ResetPasswordCompletion)
}

class LoginVM: NSObject, HasDependencies, Loginable, CPRDataValidator, CPRDataFormatter {
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    typealias Dependencies = HasAuthorizableService & HasUserDefaultService &  HasDataService & HasUser & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    var username: String?
    var password: String?
    var emailToReset: String?
    
    private(set) weak var loginCoordinator: LoginCoordinator?
    
    init(dependencies: Dependencies, loginCoordinator: LoginCoordinator) {
        self.dependencies = dependencies
        self.loginCoordinator = loginCoordinator
    }
    
    func resetPassword(completion: @escaping ResetPasswordCompletion) {
        guard let emailToReset = emailToReset, isValidPassword(emailToReset) else {
            completion(CPRError(message: NSLocalizedString("Invalid Email", comment: "Either email value is empty or email formmat is invalid"), errorType: .invalidAccessException))
            return
        }
        dependencies?.authorizableService.resetPassword(byEmail: emailToReset) { (error) in
            completion(error)
        }
    }
    
    func addNotificationToken(toUser user: CPRUser, completion: @escaping ((CPRError?)->Void)){
        let deviceToken = dependencies?.userDefaultService.getDeviceFMCToken()
        dependencies?.dataService.addNotificationToken(toUser: user, fcmToken: deviceToken, completion: completion)
    }
    
    func createUserInfoInApp(user: CPRUser) {
        dependencies?.userDefaultService.createUserInfoInApp(user: user)
    }
    
    func logout(){
        dependencies?.authorizableService.logout()
        dependencies?.userDefaultService.logout()
    }
    
    func showRegistration(){
        loginCoordinator?.navigate(to: .registration)
    }
    
    func goToForgotPassword(){
        loginCoordinator?.navigate(to: .forgotPassword)
    }
    
    func completeLogin(destination: AppCoordinator.Destination){
        loginCoordinator?.loginCoordinatorSuccessfullyCompletion?(destination)
        loginCoordinator?.parent?.removeChild(coordinator: &loginCoordinator)
    }
    
    func login(completion: @escaping ((Result<CPRUser>) -> Void)) {
        
        guard let username = username?.trimmingCharacters(in: .whitespaces), isValidEmail(email: username)  else {
            let error = CPRError(message: NSLocalizedString("Username is invalid", comment: "Email field must not be empty or should be valid"), errorType: .invalidAccessException)
            completion(.error(error))
            return
        }
        
        guard let password = password, isValidPassword(password) else {
            let error = CPRError(message: NSLocalizedString("Password is invalid", comment: "Password field must not be empty or should be valid"), errorType: .invalidAccessException)
            completion(.error(error))
            return
        }
        
        dependencies?.authorizableService.login(byEmail: username, password: password, completion: { [weak self] result in
            switch result{
            case .success(let user):
                self?.createUserInfoInApp(user: user)
                self?.addNotificationToken(toUser: user, completion: { error in
                    if let error = error {
                        completion(.error(error))
                        return
                    }
                    self?.dependencies?.loadUserInfoService?.loadUserInfo{ result in
                        switch result{
                        case .success(let user):
                            self?.dependencies?.currentUser = user
                            completion(.success(user))
                        case .error(let error):
                            completion(.error(error))
                        }
                    }
                })
            case .error(let error):
                 completion(.error(error))
            }
        })
    }
}


