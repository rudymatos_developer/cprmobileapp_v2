//
//  WelcomeScreenViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/20/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol WelcomeScreenPresentation {
    var title: String { get }
    var description: String { get }
    var imageName: String { get }
    var shouldHideStartNowButton: Bool { get }
    var wasObjectAlreadyShown: Bool { get }
}

protocol WelcomeScreenUIPresentation {
    var objects: [WelcomeScreenObject]? { get }
    func loadWelcomeObjects() -> [WelcomeScreenObject]?
}

protocol WelcomeScreenUICellPresentation {
    func configureCell(viewModel: WelcomeScreenPresentation, startNowCompletion: (() -> Void)?)
    func animateScreen(viewModel: WelcomeScreenPresentation)
}

struct WelcomeScreenObject: WelcomeScreenPresentation {
    var title: String
    var description: String
    var imageName: String
    var shouldHideStartNowButton: Bool
    var wasObjectAlreadyShown: Bool
}

class WelcomeScreenVM: NSObject, HasDependencies,  WelcomeScreenUIPresentation {
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    typealias Dependencies = HasBundleService
    var dependencies: Dependencies?
    
    var objects: [WelcomeScreenObject]?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
        self.objects = loadWelcomeObjects()
    }

    func loadWelcomeObjects() -> [WelcomeScreenObject]? {
        var welcomeScreensObjects: [WelcomeScreenObject]? = nil
        //PENDING: change this implementation to recognize the env automatically
        if let objectArray = dependencies?.bundleService.parseFileToDictionaryArray(filename: "welcome_strings") {
            welcomeScreensObjects = [WelcomeScreenObject]()
            for currentObject in objectArray {
                if let imageName = currentObject["img"] as? String, let title = currentObject["title"] as? String, let description = currentObject["description"] as? String, let shouldHideStartNowButton = currentObject["should_hide_start_button"] as? Bool {
                    let welcomeObject = WelcomeScreenObject(title: title, description: description, imageName: imageName, shouldHideStartNowButton: shouldHideStartNowButton, wasObjectAlreadyShown: false)
                    welcomeScreensObjects?.append(welcomeObject)
                }
            }
        }
        return welcomeScreensObjects
    }
    
    func objectWasAlreadyShow(atIndex: Int) {
        objects?[atIndex].wasObjectAlreadyShown = true
    }
    
    func getObjectCount() -> Int {
        return objects?.count ?? 0
    }
    
    func getObject(forIndex: Int) -> WelcomeScreenObject? {
        return objects?[forIndex]
    }
    
}
