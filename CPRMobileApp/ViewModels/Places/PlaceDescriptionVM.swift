//
//  PlaceDescriptionVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import MapKit

protocol PlaceDescriptionVMDelegate: class{
    func displayReviewDetails(review: CPRReview)
    func createNewReview(withPlace: CPRPlace)
}

class PlaceDescriptionVM: NSObject, HasDependencies{
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    typealias Dependencies = HasDataService & HasGooglePlaceService & HasUser
    var dependencies: Dependencies?
    weak var delegate: PlaceDescriptionVMDelegate?
    
    private var place : CPRPlace
    private var reviews: [CPRReview]?
    
    private var doesPlaceHaveReviews: Bool {
        return reviews?.count ?? 0 > 0
    }
    
    private var placeLocationView: PlaceLocation = {
        let view = PlaceLocation(frame: CGRect.zero)
        return view
    }()
    
    private var placeReviewsView: ReviewsViewer = {
        let view = ReviewsViewer(frame: CGRect.zero)
        return view
    }()
    
    init(dependencies: Dependencies, place: CPRPlace){
        self.dependencies = dependencies
        self.place = place
        super.init()
    }
}

extension PlaceDescriptionVM{
    
    func createNewReview(){
        delegate?.createNewReview(withPlace: place)
    }
    
    func displayReviewDetails(review: CPRReview){
        delegate?.displayReviewDetails(review: review)
    }
    
}

extension PlaceDescriptionVM{
    
    func to(collection: CPRConstants.UserFields.UserPlaces , withAction action: LASFLAction){
        guard let user = self.dependencies?.currentUser else {return}
        var userCollection = collection == .favoritePlaces ? user.favoritePlaces : user.saveForLaterPlaces
        switch action{
        case .added:
            dependencies?.dataService.addPlace(toCollection: collection, user: user, place: place)
            if userCollection == nil {
                userCollection = []
            }
            userCollection?.append(place)
        case .empty:
            dependencies?.dataService.removePlace(fromCollection: collection, user: user, place: place)
            if let index = userCollection?.firstIndex(of: place){
                userCollection?.remove(at: index)
            }
        }
        switch collection {
        case .favoritePlaces:
            self.dependencies?.currentUser?.favoritePlaces = userCollection
        case .saveForLaterPlaces:
            self.dependencies?.currentUser?.saveForLaterPlaces = userCollection
        }
        
        if let userCollection = userCollection{
            let userInfo : [String: Any] = ["collection": collection, "places" : userCollection]
            NotificationCenter.default.post(name: .refreshUserPlaces, object: nil, userInfo: userInfo)
        }
    }
    
    func getSaveForLaterAction() -> LASFLAction{
        guard let user = dependencies?.currentUser , let saveForLaterPlaces = user.saveForLaterPlaces, saveForLaterPlaces.count > 0, let _ = saveForLaterPlaces.first(where: {$0 == place}) else {return .empty}
        return .added
    }
    
    func getFavoriteAction() -> LASFLAction{
        guard let user = dependencies?.currentUser , let favoritePlaces = user.favoritePlaces, favoritePlaces.count > 0, let _ = favoritePlaces.first(where: {$0 == place}) else {return .empty}
        return .added
    }
    
}

extension PlaceDescriptionVM{
    
    func getPlaceName() -> String{
        return place.name
    }
    
    func getPlaceWebsite() -> URL?{
        guard let website = place.website else {return nil}
        return URL(string: website)
    }
    
    func getPlacePhoneNumber() -> URL?{
        guard var phone = place.phone else {return nil}
        let invalidCharacters = ["-"," ","(",")","+"]
        invalidCharacters.forEach({phone = phone.replacingOccurrences(of: $0, with: "")})
        return URL(string: "tel://\(phone)")
    }
}

extension PlaceDescriptionVM{
    
    private func configurePlaceLocation(){
        let userCurrentLocation = dependencies?.currentUser?.currentLocation
        placeLocationView.placeLocationObject = (place: place, userCurrentLocation: userCurrentLocation)
    }
    
    func getPlaceAdditionalInformation(completion: @escaping (Result<Data?>)->Void){
        
        let dispatchGroup = DispatchGroup()
        var cprError : CPRError?
        var imageData: Data?
        
        func getPlaceReviews(){
            dispatchGroup.enter()
            self.dependencies?.dataService.getAllReview(byPlace: place, limit: 20) {[weak self] result in
                switch result{
                case .success(let reviews):
                    self?.reviews = reviews
                    self?.placeReviewsView.viewModel = ReviewViewerVM(reviews: reviews)
                case .error(let error):
                    print(error)
                }
                dispatchGroup.leave()
            }
        }
        
        func getPlaceImage(){
            guard let photoReference = place.googlePhotoReferences?.randomElement() else {
                return
            }
            let queue = DispatchQueue(label: "placePhoto", qos: .userInitiated)
            dispatchGroup.enter()
            queue.async {
                self.dependencies?.googlePlacesService?.getPhoto(byReference: photoReference, completion: { result in
                    switch result{
                    case .success(let data):
                        imageData = data
                    case .error (let error):
                        print("image not found for place \(error)")
                    }
                    dispatchGroup.leave()
                })
            }
        }
        
        func getPlaceDetails(){
            dispatchGroup.enter()
            self.dependencies?.googlePlacesService?.getPlaceDetails(byPlaceId: place.googlePlaceID, completion: { [weak self] result in
                switch result{
                case .success(let googlePlaceInfoResponse):
                    self?.place.updateInfo(googleObjectResult: googlePlaceInfoResponse.result)
                case .error(let error):
                    cprError = error
                }
                dispatchGroup.leave()
            })
        }
        
        if place.shouldGetAdditionalInformation(){
            getPlaceDetails()
        }
        getPlaceReviews()
        getPlaceImage()
        dispatchGroup.notify(queue: .main){
            self.configurePlaceLocation()
            completion(.success(imageData))
        }
    }
}

extension PlaceDescriptionVM{
    
    func getNumberOrRows() -> Int{
        return doesPlaceHaveReviews ? 6 : 5
    }
    
    func getHeight(forRow row: Int) -> CGFloat{
        switch row {
        case 0:
            return 50
        case 1:
            return 50
        case 2:
            return 200
        case 3:
            return 340
        case 4:
            return doesPlaceHaveReviews ? 430 : 200
        case 5:
            return 200
        default:
            return 0
        }
    }
    
    func setDelegate(view: UIView, vc: UIViewController){
        if let view = (view as? SingleActionView), let delegate = vc as? SingleActionDelegate{
            view.delegate = delegate
        }else if let view = (view as? PlaceContactInfo), let delegate = vc as? PlaceContactInfoDelegate{
            view.delegate = delegate
        }else if let view = (view as? ReviewsViewer), let delegate = vc as? ReviewsViewerDelegate{
            view.delegate = delegate
        }
    }
    
    func getImage(forRow row: Int) -> String?{
        switch row{
        case 2:
            return "place_description_sticker"
        case 3:
            return "place_description_compass"
        case 4:
            return doesPlaceHaveReviews ? "place_description_testimonial" : "place_description_phone_book"
        case 5:
            return "place_description_phone_book"
        default:
            return nil
        }
    }
    
    func getTitleAndDescription(forRow row: Int) -> (title:String, description: String){
        switch row{
        case 2:
            return ("Were you here recently?", "Share your experience. Review this place")
        case 3:
            return ("Directions", place.address ?? "")
        case 4:
            return doesPlaceHaveReviews ? ("Reviews", "Look what people think about this place") : ("Place's Info", "Place Contact Information")
        case 5:
            return ("Place's Info", "Place Contact Information")
        default:
            return ("","")
        }
    }
    
    func getView(forRow row: Int) -> UIView{
        switch row{
        case 2:
            return getSingleActionView()
        case 3:
            return placeLocationView
        case 4:
            return doesPlaceHaveReviews ? placeReviewsView : PlaceContactInfo()
        case 5:
          return PlaceContactInfo()
        default:
            return UIView()
        }
    }
}

extension PlaceDescriptionVM{

    private func getSingleActionView() -> SingleActionView{
        let singleActionView = SingleActionView()
        let singleActionObject = SingleActionObject(title: "Create Review", color: UIColor.cprPinkColor)
        singleActionView.object = singleActionObject
        return singleActionView
    }
    
}
