//
//  ReusableReviewViewerVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/7/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

class ReviewViewerVM{
    
    private var reviews: [CPRReview]
    private var selectedIndex = 0
    
    
    init(reviews: [CPRReview]){
        self.reviews = reviews
    }
    
    func getNumberOfItems() -> Int{
        return reviews.count
    }
    
    func set(selectedIndex: Int){
        self.selectedIndex = selectedIndex
    }
    
    func getSelectedReview() -> CPRReview{
        return reviews[selectedIndex]
    }
    
    func getReview(byIndex: Int) -> CPRReview{
        return reviews[byIndex]
    }
    
    func getReviewNumber() -> String{
        return "\(selectedIndex+1) out of \(getNumberOfItems()) reviews"
    }
}
