//
//  AdditionalUserInformationVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

class AdditionalUserInformationVM: NSObject, CPRDataFormatter, HasDependencies {
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    var displayName: DynamicType<String?>!
    var birthday: DynamicType<String?>!
    var countryCode: DynamicType<String?>!
    var mobileNumber: DynamicType<String?>!
    var gender: DynamicType<String?>!
    var maritalStatus: DynamicType<String?>!
    
    typealias Dependencies = HasDataService & HasUser & HasUserDefaultService & HasAuthorizableService & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    private weak var userCoordinator: UserCoordinator?
    weak var delegate: ProfileVMDelegate?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
        super.init()
        initUI()
    }
    
    private func initUI(){
        guard let user = dependencies?.currentUser else {fatalError("Invalid user or dependency")}
        displayName = DynamicType(user.displayName)
        birthday = DynamicType(convert(date: user.dateOfBirth, usingPattern: .short))
        countryCode = DynamicType(user.countryCode)
        mobileNumber = DynamicType(user.mobile)
        gender = DynamicType(user.gender)
        maritalStatus = DynamicType(user.maritalStatus)
    }
    
    func getProfilePictureURL() -> String?{
        return dependencies?.currentUser?.profilePictureURL
    }
    
    func updateProfile(){
        guard let user = dependencies?.currentUser else {fatalError("Invalid user or dependency")}
        user.displayName = displayName.value
        user.dateOfBirth = convert(string: birthday.value, usingPattern: .short)
        user.countryCode = countryCode.value
        user.mobile = mobileNumber.value
        user.gender = gender.value
        user.maritalStatus = maritalStatus.value
        dependencies?.dataService.updateUser(user: user)
    }
    
}
