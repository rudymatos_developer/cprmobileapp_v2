//
//  ProfileViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit


enum Badge: String{
    
    case bronze = "bronze"
    case silver = "silver"
    case gold = "gold"
    case planitum = "platinum"
    
    init(reviewsCount: Int){
        switch reviewsCount {
        case 6..<11:
            self = .silver
        case 11..<20:
            self =  .gold
        case 20...:
            self = .planitum
        default:
            self = .bronze
        }
    }

    func getTitle() -> String{
        return self.rawValue.capitalized
    }
    
    func getImage() -> UIImage{
        return UIImage(named: "badge_\(self.rawValue)")!
    }
    
    func getFontColor() -> UIColor{
        switch self {
        case .bronze:
            return UIColor(red: 242/255, green: 168/255, blue: 48/255, alpha: 1)
        case .silver:
            return UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1)
        case .gold:
             return UIColor(red: 253/255, green: 208/255, blue: 109/255, alpha: 1)
        case .planitum:
             return UIColor(red: 123/255, green: 190/255, blue: 235/255, alpha: 1)
        }
    }
}


protocol ProfileVMDelegate:class{
    func showAll(configuration: OptionSelectorConfiguration)
    func goToAddAdditionalInformation()
}

class ProfileVM: NSObject, CPRDataFormatter, HasDependencies {
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    var badge: DynamicType<Badge>!
    var displayName: DynamicType<String>!
    var email: DynamicType<String>!
    var favoritePlacesCount: DynamicType<Int>!
    var sflPlaceCount: DynamicType<Int>!
    var since: DynamicType<String?>!
    var profilePicture: DynamicType<String?>!
    var numberOfReviews: DynamicType<Int>!
    
    typealias Dependencies = HasDataService & HasUser & HasUserDefaultService & HasAuthorizableService & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    private weak var userCoordinator: UserCoordinator?
    weak var delegate: ProfileVMDelegate?
    
    init(dependencies: Dependencies, userCoordinator: UserCoordinator) {
        self.dependencies = dependencies
        self.userCoordinator = userCoordinator
        super.init()
        initUI()
        registerNotifications()
    }
    
    private func initUI(){
        guard let user = dependencies?.currentUser else {fatalError("Invalid user or dependency")}
        displayName = DynamicType(user.fullName)
        email = DynamicType(user.email)
        profilePicture = DynamicType(user.profilePicture)
        numberOfReviews = DynamicType(user.reviews?.count ?? 0)
        favoritePlacesCount = DynamicType(user.favoritePlaces?.count ?? 0)
        sflPlaceCount = DynamicType(user.saveForLaterPlaces?.count ?? 0)
        badge = DynamicType(Badge(reviewsCount: user.reviews?.count ?? 0))
        since = DynamicType(convert(date: user.dateOfBirth, usingPattern: .full))
    }
    
    func getProfilePictureURL() -> String?{
        return dependencies?.currentUser?.profilePictureURL
    }
    
    func updateUserInfo(user: CPRUser){
        dependencies?.currentUser = user
    }
    
    func assignNewProfilePicture(withImage: UIImage, completion: @escaping ((CPRUser?, CPRError?)->Void)){
        guard let currentUser = dependencies?.currentUser, let dataService = dependencies?.dataService else {return}
        dataService.assignNewProfileImage(email: currentUser.email, image: withImage) { (error) in
            guard error == nil else{
                completion(nil,error)
                return
            }
            dataService.getUserData(byUserKey: currentUser.email) { updatedUser, error in
                guard error == nil else{
                    completion(nil,error)
                    return
                }
                completion(updatedUser,nil)
            }
        }
    }
}

extension ProfileVM{
    
    func logout(){
        guard let user = dependencies?.currentUser else {return}
        let fcmToken = dependencies?.userDefaultService.getDeviceFMCToken()
        dependencies?.authorizableService.logout()
        dependencies?.currentUser = nil
        dependencies?.userDefaultService.logout()
        removeAllSocialProfiles()
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        dependencies?.dataService.removeNotificationToken(from: user, fcmToken: fcmToken) { error in
            if let error = error{
                print(error)
            }
            dispatchGroup.leave()
        }
        dispatchGroup.notify(queue: .main){
            self.userCoordinator?.logoutCompletion?()
            self.userCoordinator?.parent?.removeChild(coordinator: &self.userCoordinator)
        }
    }
    
    private func removeAllSocialProfiles(){
        dependencies?.userDefaultService.removeAllSocialProfiles()
    }
    
    func showAddAdditionalInformationVC(){
        delegate?.goToAddAdditionalInformation()
    }
    
}


extension ProfileVM{
    
    func showItemsBy(category: MainCategory){
        guard category == .myFavoritePlaces || category == .saveForLaterPlaces || category == .myReviews else {return}
        var configurator = OptionSelectorConfiguration(category: category)
        switch category{
        case .myFavoritePlaces:
            configurator.loadOptionRequest = dependencies?.loadUserInfoService?.loadFavoritePlaces
        case .saveForLaterPlaces:
            configurator.loadOptionRequest = dependencies?.loadUserInfoService?.loadSaveForLaterPlaces
        case .myReviews:
            configurator.loadOptionRequest = dependencies?.loadUserInfoService?.loadAllUserReviews
        default:
            print("do nothing")
        }
        delegate?.showAll(configuration: configurator)
    }
    
}

extension ProfileVM{
    
    private func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileVM.refreshUserReviews(_:)), name: .refreshUserReviews, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileVM.reloadUserPlaces(_:)), name: .refreshUserPlaces, object: nil)
    }
    
    private func refreshUserReviews(reviews: [CPRReview]){
        numberOfReviews.value = reviews.count
        badge.value = Badge(reviewsCount: reviews.count)
    }
    
    @objc private func refreshUserReviews(_ notification: Notification){
        guard let userInfo = notification.userInfo, let reviews = userInfo[CPRConstants.UserFields.reviews] as? [CPRReview] else {return}
        refreshUserReviews(reviews: reviews)
    }
    
    @objc private func reloadUserPlaces(_ notification : Notification){
        guard let collection = notification.userInfo?["collection"] as? CPRConstants.UserFields.UserPlaces, let places = notification.userInfo?["places"] as? [CPRPlace] else {return}
        switch collection {
        case .favoritePlaces:
            favoritePlacesCount.value = places.count
        case .saveForLaterPlaces:
            sflPlaceCount.value = places.count
        }
    }
}
