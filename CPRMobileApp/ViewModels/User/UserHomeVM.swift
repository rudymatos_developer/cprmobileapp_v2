//
//  PurchaseCategoriesVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/3/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MapKit

protocol UserHomeVMDelegate:class{
    func goToSelectedItem(option: Section.Option)
    func goToPlaceDescription(place: CPRPlace)
    func goToSeeAll(selectionType: OptionSelectorConfiguration)
    func goToMainOptionBasedOnCategory(selectionType: OptionSelectorConfiguration)
}

class UserHomeVM: NSObject,HasDependencies{
    
    let TITLE_INDEX = 0
    let CONTENT_INDEX = 1
    
    typealias Dependencies = HasUser & HasDataService & HasBundleService & HasGooglePlaceService & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    weak var delegate : UserHomeVMDelegate!
    var loadItemCompletion: ((Result<Bool>) -> Void)?
    
    private var myCurrentLocation : CLLocation?
    private var sections = Dictionary<String,Section>()
    private var availableSections : [(key: String, value: Section)] {
        return sections.filter({$0.value.status == .valid}).sorted(by: {$0.value.priority < $1.value.priority})
    }
    
    private var availableCategories = MainCategory.allCases
    private var place: CPRPlace?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
        super.init()
        registerNotifications()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("💀 im out from \(self.classForCoder.description())")
    }
}

extension UserHomeVM{
    func goToSelectedItem(option: Section.Option){
        delegate.goToSelectedItem(option: option)
    }
    func goToSeeAll(selectionType: OptionSelectorConfiguration){
        var selectionType = selectionType
        guard let category = selectionType.category as? MainCategory else {return}
        switch category{
        case .myReviews:
            selectionType.loadOptionRequest = dependencies?.loadUserInfoService?.loadAllUserReviews
        case .bars, .cafes, .restaurants, .salons, .hotels:
            selectionType.loadOptionRequest = dependencies?.loadUserInfoService?.getCategoryFunction(category: category)
        case .myFavoritePlaces:
            selectionType.loadOptionRequest = dependencies?.loadUserInfoService?.loadFavoritePlaces
        case .saveForLaterPlaces:
            selectionType.loadOptionRequest = dependencies?.loadUserInfoService?.loadSaveForLaterPlaces
        }
        delegate.goToSeeAll(selectionType: selectionType)
    }
    
    func goToMainOption(selectionType: OptionSelectorConfiguration){
        var selectionType = selectionType
        guard let category = selectionType.category as? MainCategory, (category == .bars || category == .cafes ||
            category == .restaurants ||
            category == .salons ||
            category == .hotels ||
            category == .bars) else {return}
            selectionType.loadOptionRequest = dependencies?.loadUserInfoService?.loadNearByPlacesBy(category: category)
        delegate.goToMainOptionBasedOnCategory(selectionType: selectionType)
    }
}

extension UserHomeVM{
    
    func set(place: CPRPlace){
        self.place = place
        delegate.goToPlaceDescription(place: place)
    }
    
    func getCurrentUser() -> CPRUser{
        guard let currentUser = dependencies?.currentUser else {
            fatalError("MainScreen should not be visible is user doesn't exit")
        }
        return currentUser
    }
    
    func getUserCurrentLocation(){
        self.dependencies?.googlePlacesService?.getCurrentLocationCompletion = { [weak self] result in
            switch result {
            case .success(let coordinates):
                print(coordinates)
                let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
                self?.dependencies?.currentUser?.currentLocation = location
                self?.myCurrentLocation = location
                self?.loadData(sortByLocation: true)
            case .error(let error):
                print(error)
                self?.loadData()
            }
        }
        self.dependencies?.googlePlacesService?.getCurrentLocation()
    }
    
    func loadData(sortByLocation: Bool = false){
        resetSections()
        let dispatchQueue = DispatchQueue(label: "loadDataQueue", qos: .userInitiated, attributes: .concurrent)
        dispatchQueue.async {
            let dispatchGroup = DispatchGroup()
            self.loadPlacesSectionsByCategories(dispatchGroup, sortByLocation)
            dispatchGroup.notify(queue: .main){
                self.loadMyReviews()
                self.loadMyPlaces()
                self.loadItemCompletion?(.success(true))
            }
        }
    }
    
    private func loadPlacesSectionsByCategories(_ dispatchGroup: DispatchGroup, _ sortByLocation: Bool){
        for category in self.availableCategories.filter({$0.isCategorized()}){
            dispatchGroup.enter()
            self.dependencies?.dataService.getAllPlaces(byCategory: category.getGooglePlacesName(), limit: 10, completion: { [weak self] result in
                switch result{
                case .success(var places):
                    if sortByLocation{
                        guard let myCurrentLocation = self?.myCurrentLocation else {
                            self?.addToSection(category: category, objects: places)
                            dispatchGroup.leave()
                            return
                        }
                        places.sort{ p1,p2 in
                            let p1Location = CLLocation(latitude: p1.coordinates.latitude, longitude: p1.coordinates.longitude)
                            let p2Location = CLLocation(latitude: p2.coordinates.latitude, longitude: p2.coordinates.longitude)
                            return myCurrentLocation.distance(from: p1Location) < myCurrentLocation.distance(from: p2Location)
                        }
                    }
                    self?.addToSection(category: category, objects: places)
                case .error(let error):
                    print("ERROR HERE : \(error)")
                }
                dispatchGroup.leave()
            })
        }
    }
    
    private func loadMyPlaces(){
        guard let user = dependencies?.currentUser else {return}
        if let favoritePlaces = user.favoritePlaces, favoritePlaces.count > 0{
            addToSection(category: MainCategory.myFavoritePlaces, objects: favoritePlaces)
        }
        if let saveForLaterPlaces = user.saveForLaterPlaces, saveForLaterPlaces.count > 0{
            addToSection(category: MainCategory.saveForLaterPlaces, objects: saveForLaterPlaces)
        }
    }
    
    private func loadMyReviews() {
        if var reviews = self.dependencies?.currentUser?.reviews, reviews.count > 0{
            reviews.sort(by: {$0.when > $1.when})
            self.addToSection(category: MainCategory.myReviews, objects:reviews)
        }
    }
}

extension UserHomeVM{
    
    private func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(UserHomeVM.reloadUserPlaces(_:)), name: .refreshUserPlaces, object: nil)
    }
    
    @objc private func reloadUserPlaces(_ notification : Notification){
        let queue = DispatchQueue(label: "refreshUserData", qos: .userInitiated, attributes: .concurrent)
        queue.async {
            guard let user = self.dependencies?.currentUser, let collection = notification.userInfo?["collection"] as? CPRConstants.UserFields.UserPlaces, let places = notification.userInfo?["places"] as? [CPRPlace] else {return}
            var userCollection = collection == .favoritePlaces ? user.favoritePlaces : user.saveForLaterPlaces
            switch collection {
            case .favoritePlaces:
                cleanSectionWhenNoReviews(category: .myFavoritePlaces, collection: userCollection)
            case .saveForLaterPlaces:
                cleanSectionWhenNoReviews(category: .saveForLaterPlaces, collection: userCollection)
            }
            self.loadMyPlaces()
            self.loadItemCompletion?(.success(true))
        }
        func cleanSectionWhenNoReviews(category: MainCategory, collection: [CPRPlace]?){
            if collection == nil || collection?.count == 0 {
                sections[category.getCategoryName()] = nil
            }
        }
    }
}

extension UserHomeVM{
    
    private func resetSections(){
        initAvailableCategories()
    }
    
    private func initAvailableCategories(){
        availableCategories.forEach({sections[$0.getCategoryName()] = Section()})
    }
    
    private func addToSection(category: MainCategory, objects: [Any]){
        guard objects.count > 0 else {return}
        let optionTitle = Section.OptionTitle(title: category.getDisplayName(), object: category, optionType: .title)
        let options = objects.compactMap { object -> Section.Option? in
            guard let mainScreenFormatter = object as? MainScreenFormatter else {return nil}
            return Section.Option(title: mainScreenFormatter.getName(), description: mainScreenFormatter.getDescription(), imagePath: mainScreenFormatter.getRandomImage(), bgColor: nil, optionType: .withImageTitleAndDescription, object: object)
        }
        let optionsSection = Section(sectionTitle: optionTitle, options:options, category: category, priority: category.getPriority())
        sections[category.getCategoryName()] = optionsSection
    }
    
}

extension UserHomeVM{
    
    func getSection(byIndex index: Int) -> Section?{
        guard index <= availableSections.count - 1 else {return nil}
        return availableSections[index].value
    }
    
    func getHeight(forRow rowIndex: Int) -> Int{
        if rowIndex == TITLE_INDEX{
            return 30
        }else{
            return 180
        }
    }
    
    func getNumberOfSections() -> Int{
        return availableSections.count
    }
    
}
