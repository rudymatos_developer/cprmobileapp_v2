//
//  NewReviewVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

enum NewReviewAction: String{
    
    case addPlace = "Add Place"
    case replacePlace = "Replace Place"
    case addPhoto = "Add Photo"
    case saveReview = "Save Review"
    case rate = "Rate"
    case waiting = "Wait"
    
    func getTitle() -> String{
        switch self{
        case .addPlace:
            return "Add Place"
        case .replacePlace:
            return "Replace Place"
        case .addPhoto:
            return "Add Photo"
        case .saveReview:
            return "Save Review"
        default:
            return ""
        }
    }
    
    func getColor() -> UIColor{
        switch self{
        case .addPlace, .replacePlace:
            return UIColor.cprPinkColor
        case .addPhoto, .saveReview:
            return UIColor.cprGreenColor
        default:
            return .black
        }
    }
    
}

enum NewReviewSection: Int{
    case place = 0
    case photos = 1
    case additionalInfo = 2
}


class NewReviewVM: NSObject, HasDependencies{
    
    private var review = CPRReview()
    typealias Dependencies = HasUser & HasSocialMediaService & HasUserDefaultService & HasDataService & HasBundleService & HasLoadUserInfoService & HasGooglePlaceService
    var dependencies: Dependencies?
    
    var saveReviewCompletion : ((CPRUser, CPRReview?)->Void)?
    
    private var placeLocationView: PlaceLocation = {
        let view = PlaceLocation(frame: CGRect.zero)
        return view
    }()

    private var additionalInfoView: AdditionalInfoView = {
        let view = AdditionalInfoView(frame: CGRect.zero)
        return view
    }()

    
    init(dependencies: Dependencies, place: CPRPlace){
        self.dependencies = dependencies
        review.place = place
        super.init()
        configurePlaceLocation()
    }
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
    }
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
}


extension NewReviewVM{
    
    private func configurePlaceLocation(){
        guard let userCurrentLocation = dependencies?.currentUser?.currentLocation, let place = review.place else {return}
        placeLocationView.placeLocationObject = (place: place, userCurrentLocation: userCurrentLocation)
    }
    
    func set(place: CPRPlace){
        review.place = place
        review.location = place.coordinates
        review.locationName = place.name
        review.address = place.address ?? ""
        review.placeID = place.googlePlaceID
        configurePlaceLocation()
    }
    
    func setWhen(_ date: Date){
        review.when = date
    }
    
    func setRating(_ value: Int){
        review.rating = value
    }
    
    func setWaiting(_ value: Int){
        review.waiting = value
    }
    
    func getPlaceName() -> String{
        return review.place?.name ?? ""
    }
    
    func getImageAt(index: Int) -> UIImage?{
        return review.images?[index]
    }
    
    func add(image: UIImage){
        if review.images == nil{
            review.images = []
        }
        review.images?.append(image)
    }
    
    func doesReviewHasAPlaceAssigned() -> Bool{
        return review.place != nil
    }
    
    func doesReviewHasPhotos() -> Bool{
        guard let images = review.images, images.count > 0 else {return false}
        return true
    }
    
}
extension NewReviewVM{
    
    func isGoodToSave() -> Bool{
        guard let additionalInfoViewObject = additionalInfoView.getAdditionalInfoObject(), review.place != nil else { return false }
        review.server = additionalInfoViewObject.server
        review.service = additionalInfoViewObject.service
        review.reasonOfVisit = additionalInfoViewObject.reasonOfVisit
        review.companion = additionalInfoViewObject.companion
        review.comments = additionalInfoViewObject.comments
        return true
    }
    
    func save(completion : @escaping (Result<Bool>) -> Void){
        
        guard let currentUser = dependencies?.currentUser,let place = review.place else {
            let error = CPRError(message: "Invalid User",errorType:.errorSavingReview)
            completion(.error(error))
            return
        }

        review.location = place.coordinates
        review.locationName = place.name
        review.address = place.address ?? ""
        review.placeID = place.googlePlaceID
        review.archived = false
        review.rating = review.rating + 1
        review.waiting = review.waiting + 1
        review.userDisplayName = currentUser.fullName
        review.userID = currentUser.email
        review.userProfilePictureURL = currentUser.profilePictureURL
        review.generateImagesNames()

        dependencies?.dataService.save(review: review, images: review.images, completion: { [unowned self] in
            let dispatchGroup = DispatchGroup()
            var cprError : CPRError?
            var latestReviews: [CPRReview]?
            if let place = self.review.place{
                if place.shouldGetAdditionalInformation(){
                    dispatchGroup.enter()
                    self.dependencies?.googlePlacesService?.getPlaceDetails(byPlaceId: place.googlePlaceID) { result in
                        switch result{
                        case .success(let googlePlaceInfoResponse):
                            place.updateInfo(googleObjectResult: googlePlaceInfoResponse.result)
                        case .error(let error):
                            print(error)
                            cprError = error
                        }
                        dispatchGroup.leave()
                    }
                    dispatchGroup.wait()
                }
                
                dispatchGroup.enter()
                self.dependencies?.dataService.addPlaceIfNeeded(place: place, withReview: self.review){ result  in
                    switch result{
                    case .error(let error):
                        print(error)
                        cprError = error
                    default:
                        print("")
                    }
                    dispatchGroup.leave()
                }
            }
            dispatchGroup.enter()
            self.dependencies?.loadUserInfoService?.loadUserReviews(user: currentUser, completion: { result in
                switch result{
                case .success(let reviews):
                    latestReviews = reviews
                case .error(let error):
                    print(error)
                    cprError = error
                }
                dispatchGroup.leave()
            })
            
            dispatchGroup.notify(queue: .main){
                guard let reviews = latestReviews, cprError == nil else{
                    completion(.error(cprError!))
                    return
                }
                self.dependencies?.currentUser?.reviews = reviews
                let userInfo = [CPRConstants.UserFields.reviews : reviews]
                NotificationCenter.default.post(name: .refreshUserReviews, object: nil, userInfo: userInfo)
                completion(.success(true))
                self.saveReviewCompletion?(currentUser, self.review)
            }
        })
    }
    
    func getSections() -> Int{
        return 3
    }
    
    func getNumberOfRows(forSection section: Int) -> Int{
        guard let newReviewSection = NewReviewSection(rawValue: section) else {return 0}
        switch newReviewSection{
        case .place:
            if doesReviewHasAPlaceAssigned(){
                return 3
            }
            return 1
        case .photos:
            if doesReviewHasPhotos(){
                return 2
            }
            return 1
        case .additionalInfo:
            return 5
        }
    }
    
    func getHeight(forIndexPath indexPath: IndexPath) -> CGFloat{
        guard let newReviewSection = NewReviewSection(rawValue: indexPath.section) else {return 0}
        switch newReviewSection{
        case .place:
            if doesReviewHasAPlaceAssigned(){
                switch indexPath.row {
                case 0:
                    return 50
                case 1:
                    return 200
                case 2:
                    return 340
                default:
                    return 0
                }
            }
            return 200
        case .photos:
            if doesReviewHasPhotos(){
                switch indexPath.row {
                case 0:
                    return 200
                case 1:
                    return 220
                default:
                    return 0
                }
            }
            return 200
        case .additionalInfo:
            switch indexPath.row{
            case 0:
                return 340
            case 1:
                return 625
            case 2:
                return 200
            case 3:
                return 200
            case 4:
                return 70
            default:
                return 0
            }
        }
    }
    
    func setDelegate(view: UIView, vc: UIViewController){
        if let view = (view as? SingleActionView), let delegate = vc as? SingleActionDelegate{
            view.delegate = delegate
        } else if let view = (view as? GenericRatingComponent), let delegate = vc as? GenericRatingComponentDelegate{
            view.delegate = delegate
        } else if let view = (view as? PhotoScrollerView), let delegate = vc as? PhotoScrollerViewDelegate{
            view.delegate = delegate
        }
    }
    
    func getImage(forIndexPath indexPath: IndexPath) -> String?{
        guard let newReviewSection = NewReviewSection(rawValue: indexPath.section) else {return nil}
        switch newReviewSection{
        case .place:
            if doesReviewHasAPlaceAssigned(){
                switch indexPath.row {
                case 1:
                    return "newreview_add_place"
                case 2:
                    return "place_description_compass"
                default:
                    return nil
                }
            }
            return "newreview_add_place"
        case .photos:
            if doesReviewHasPhotos(){
                switch indexPath.row {
                case 0:
                    return "newreview_photo"
                case 1:
                    return "newreview_pictures"
                default:
                    return nil
                }
            }
            return "newreview_photo"
        case .additionalInfo:
            switch indexPath.row{
            case 1:
                return "newreview_additional_info"
            case 2:
                return "newreview_waiting"
                
            case 3:
                return "newreview_review"
            default:
                return nil
            }
        }
    }
    
    func getTitleAndDescription(forIndexPath indexPath: IndexPath) -> (title:String, description: String){
        guard let newReviewSection = NewReviewSection(rawValue: indexPath.section) else {return ("","")}
        switch newReviewSection{
        case .place:
            if doesReviewHasAPlaceAssigned(){
                switch indexPath.row {
                case 1:
                    return ("Step 1 - Where did you go?", "Select the place that you are/were for this review")
                case 2:
                    return ("Directions", review.place?.address ?? "")
                default:
                    return ("","")
                }
            }
            return ("Step 1 - Where did you go?", "Select the place that you are/were for this review")
        case .photos:
            if doesReviewHasPhotos(){
                switch indexPath.row {
                case 0:
                    return ("Step 2 - Add Photos", "Select photos from your library or use your camera")
                case 1:
                    return ("Photos", "You currenlty have \(review.images?.count ?? 0) pictures for this review")
                default:
                    return ("","")
                }
            }
            return ("Step 2 - Add Photos", "Select photos from your library or use your camera")
        case .additionalInfo:
            switch indexPath.row{
            case 1:
                return ("Step 4 - Additional Info", "Tell us more about your experience")
            case 2:
                return ("Step 5 - Rate your waiting time", "How long did you wait to get your service?")
            case 3:
                return ("Step 6 - Rate your entire experience", "How was your overall experience?")
            default:
                return ("","")
            }
        }
    }
    
    func getView(forIndexPath indexPath: IndexPath) -> UIView{
        guard let newReviewSection = NewReviewSection(rawValue: indexPath.section) else {return UIView()}
        switch newReviewSection{
        case .place:
            if doesReviewHasAPlaceAssigned(){
                switch indexPath.row {
                case 1:
                    return getSingleActionView(newReviewAction: .replacePlace)
                case 2:
                    return placeLocationView
                default:
                    return UIView()
                }
            }
            return getSingleActionView(newReviewAction: .addPlace)
        case .photos:
            if doesReviewHasPhotos(){
                switch indexPath.row {
                case 0:
                    return getSingleActionView(newReviewAction: .addPhoto)
                case 1:
                    return getPhotoScrollerView()
                default:
                    return UIView()
                }
            }
            return getSingleActionView(newReviewAction: .addPhoto)
        case .additionalInfo:
            switch indexPath.row{
            case 1:
                return additionalInfoView
            case 2:
                return getRatingView(animationType: .star, forType: .waiting)
            case 3:
                return getRatingView(animationType: .heart, forType: .rate)
            default:
                return UIView()
            }
        }
    }
}

extension NewReviewVM{
    
    private func getPhotoScrollerView() -> PhotoScrollerView{
        let photoScroller = PhotoScrollerView()
        photoScroller.images = review.images
        return photoScroller
    }
    
    private func getRatingView(animationType: AnimationType, forType: NewReviewAction) -> GenericRatingComponent{
        let ratingView = GenericRatingComponent()
        ratingView.fileName = animationType
        ratingView.forType = forType.rawValue
        ratingView.setRatingManually(value: forType == .rate ? review.rating : review.waiting)
        return ratingView
    }
    
    private func getSingleActionView(newReviewAction: NewReviewAction) -> SingleActionView{
        let singleActionView = SingleActionView()
        let singleActionObject = SingleActionObject(title: newReviewAction.getTitle(), color: newReviewAction.getColor())
        singleActionView.object = singleActionObject
        return singleActionView
    }
}
