//
//  MyReviewsViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol ReviewListType {
    var selectedReview: CPRReview? {get set}
    var reviewListVM: [String: [CPRReview]] {get}
    var sectionTitles: [String] {get}
    func getReview(byIndex: Int, andSection: Int) -> CPRReview?
    func shouldShowNoReviewsMessage() -> Bool
    func getAllSections() -> [String]
    func getSectionTitle(byIndex: Int) -> (String, String)
    func totalOfSections() -> Int
    func totalOfReviews(bySection: Int) -> Int
    func getReview(withFirebaseId: String) -> CPRReview?
}

protocol SorterViewModelType {
    var sortBy: DynamicType<Sort> {get}
}

class ReviewListVM: NSObject, ReviewListType, SorterViewModelType, CPRDataFormatter {

    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    var selectedReview: CPRReview?
    private(set) var sectionTitles = [String]()
    private(set) var sortBy: DynamicType<Sort> = DynamicType<Sort>(Sort(field: .date, order: .ascending))
    private var CPRReviewList = [CPRReview]()
    private(set) var reviewListVM = [String: [CPRReview]]()
    
    init(reviews: [CPRReview]?) {
        super.init()
        if let reviews = reviews, reviews.count > 0 {
            CPRReviewList = sort(reviews)
            populateReviewListWithSectionsObject(reviews: CPRReviewList)
        }
    }
    
    private func sort(_ reviews: [CPRReview]) -> [CPRReview] {
        var sortedReviews: [CPRReview]
        switch sortBy.value.field {
        case .date:
            sortedReviews = reviews.sorted(by: { (r1, r2) -> Bool in
                return sortBy.value.order == .ascending ? r1.when < r2.when : r1.when > r2.when
            })
        case .rating:
            sortedReviews = reviews.sorted(by: { (r1, r2) -> Bool in
                return sortBy.value.order == .ascending ? r1.rating < r2.rating : r1.rating > r2.rating
            })
        case .businessDisplayName:
            sortedReviews = reviews.sorted(by: { (r1, r2) -> Bool in
                return sortBy.value.order == .ascending ? r1.businessDisplayName < r2.businessDisplayName : r1.businessDisplayName > r2.businessDisplayName
            })
        }
        return sortedReviews
    }
    
    func sortReviews(by: Sort) {
        print("setting new value to: \(by.field)-\(by.order)")
        CPRReviewList = sort(CPRReviewList)
        populateReviewListWithSectionsObject(reviews: CPRReviewList)
        sortBy.value = by
    }
    
    private func populateReviewListWithSectionsObject(reviews: [CPRReview]) {
        calculateSections(reviews: CPRReviewList)
        for title in sectionTitles {
            switch sortBy.value.field {
            case .date:
                reviewListVM[title] = reviews.filter({convert(date: $0.when, usingPattern: .section) == title})
            case .rating:
                reviewListVM[title] = reviews.filter({"\($0.rating) stars" == title})
            case .businessDisplayName:
                reviewListVM[title] = reviews.filter({$0.businessDisplayName == title})
            }
        }
    }
    
    private  func calculateSections(reviews: [CPRReview]) {
        switch sortBy.value.field {
        case .date:
            sectionTitles = reviews.reduce(into: []) { (titles, reviews) in
                let title = convert(date: reviews.when, usingPattern: .section)
                if !titles.contains(title) {
                        titles.append(title)
                }
            }
        case .rating:
            sectionTitles = reviews.reduce(into: []) { (titles, reviews) in
                if !titles.contains("\(reviews.rating) stars") {
                    titles.append("\(reviews.rating) stars")
                }
            }
        case .businessDisplayName:
            sectionTitles = reviews.reduce(into: []) { (titles, reviews) in
                let businessName = reviews.businessDisplayName
                if !titles.contains(businessName) {
                    titles.append(businessName)
                }
            }
        }
    }
    
    func getReview(withFirebaseId: String) -> CPRReview? {
        var reviewsWithFirebaseId: CPRReview? = nil
        sectionTitles.forEach { (section) in
            if let valueInSection = reviewListVM[section]?.filter({$0.firebaseId == withFirebaseId}).first {
                reviewsWithFirebaseId = valueInSection
            }
        }
        return reviewsWithFirebaseId
    }
    
    func getReview(byIndex: Int, andSection: Int) -> CPRReview? {
        let sectionTitle = sectionTitles[andSection]
        return reviewListVM[sectionTitle]?[byIndex]
    }
    
    func shouldShowFilterButton() -> Bool {
        return reviewListVM.count > 0
    }
    
    func shouldShowReviewTable() -> Bool {
        return reviewListVM.count > 0
    }
    
    func shouldShowNoReviewsMessage() -> Bool {
        return reviewListVM.count == 0
    }
    
    func getAllSections() -> [String] {
        return sectionTitles
    }
    
    func getSectionTitle(byIndex: Int) -> (String, String) {
        let title = sectionTitles[byIndex]
        switch sortBy.value.field {
        case .date:
            let month = title.split(separator: " ")[0]
            let year = title.split(separator: " ")[1]
            return (base: String(month), detail: String(year))
        default:
            return (base:title, detail:"")
        }
    }
    
    func totalOfSections() -> Int {
        return sectionTitles.count
    }
    
    func totalOfReviews(bySection: Int) -> Int {
        let sectionTitle = sectionTitles[bySection]
        return reviewListVM[sectionTitle]?.count ?? 0
    }
}
