//
//  ShareReviewVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/1/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

class ShareReviewVM: NSObject, HasDependencies{

    var socialProfiles: [SocialProfile]!
    var review : CPRReview
    var shareCompletion : (()->Void)?
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    typealias Dependencies = HasUser & HasSocialMediaService & HasUserDefaultService
    var dependencies: Dependencies?
    
    init(dependencies: Dependencies, socialProfiles: [SocialProfile]?, review: CPRReview){
        self.dependencies = dependencies
        self.socialProfiles = socialProfiles
        self.review = review
    }
    
    func getNumOfRows() -> Int{
        return socialProfiles.count
    }
    
    func getSocialProfile(byIndex: Int) -> SocialProfile?{
        guard byIndex <= socialProfiles.count - 1 else {return nil}
        return socialProfiles[byIndex]
    }
    
    private func getSocialProfile(bySocialNetwork socialNetwork: SocialNetwork) -> SocialProfile?{
        return socialProfiles.filter({$0.socialNetwork == socialNetwork}).first
    }
    
    func getReviewPlace() -> String{
        return review.locationName
    }
    
    func getReviewAddress() -> String{
        return review.address
    }
    
    func getReviewRating() -> String{
        return "\(review.rating) out of 5 stars"
    }
    
    func login(socialNetwork: SocialNetwork, completion: @escaping ((Result<SocialProfile>)->Void)){
        dependencies?.socialMediaService.login(to: socialNetwork) { [unowned self] result in
            switch result{
            case .success(let profile):
                self.dependencies?.socialMediaService.retrieveProfile(type: TwitterProfile.self, socialProfile: profile, completion: { profileResult in
                    switch profileResult{
                    case .success(let twitterProfile):
                        profile.shouldShare = true
                        profile.bio = twitterProfile.description
                        profile.profilePictureURL = URL(string:twitterProfile.profileImage)
                        if let index = self.socialProfiles.firstIndex(where: {$0.socialNetwork == socialNetwork}){
                            self.socialProfiles[index] = profile
                            self.dependencies?.userDefaultService.set(socialProfiles: self.socialProfiles)
                        }
                        completion(.success(profile))
                    case .error(let errorRetrivingProfile):
                        completion(.error(errorRetrivingProfile))
                    }
                })
            case .error:
                completion(result)
            }
        }
    }
    
    private func resetSharingPreferences(){
        socialProfiles.forEach({$0.shouldShare = false})
        dependencies?.userDefaultService.set(socialProfiles: self.socialProfiles)
    }
    
    func shareReview(completion: @escaping ()->()){
        let sharingToProfiles = socialProfiles.filter({$0.shouldShare})
        var counter = 0
        if sharingToProfiles.count > 0{
            sharingToProfiles.forEach { [weak self] profile in
                dependencies?.socialMediaService.post(socialProfile: profile, review: review, completion: { result in
                    counter += 1
                    if counter == sharingToProfiles.count{
                        self?.resetSharingPreferences()
                        self?.shareCompletion?()
                        completion()
                    }
                })
            }
        }else{
            shareCompletion?()
            completion()
        }
    }
    
    func cancelSharing(completion: @escaping ()->()){
        resetSharingPreferences()
        shareCompletion?()
        completion()
    }
}
