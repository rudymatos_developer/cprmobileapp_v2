//
//  CalendarVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/19/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol CalendarVMDelegate: class{
    func createNewReview()
    func goToSelected(review: CPRReview)
}

class CalendarVM: NSObject, HasDependencies, CPRDataFormatter{
    
    typealias Dependencies = HasUser & HasDataService
    var dependencies: Dependencies?
    
    private var myReviews = [CPRReview]()
    private var currentReview: CPRReview?
    private var dateWithReviews = [String]()
    private var filteredReviews = [CPRReview]()
    private var selectedDate = Date()
    
    weak var delegate: CalendarVMDelegate!
    
    init(depedencies: Dependencies){
        self.dependencies = depedencies
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
        loadData()
    }
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
     func loadData(){
        guard let user = dependencies?.currentUser, let reviews = user.reviews else {return}
        myReviews = reviews.filter({!$0.archived})
        dateWithReviews = myReviews.compactMap({self.convert(date: $0.when)})
    }
    
    func getSelectedDate() -> Date {
        return selectedDate
    }
    
    func validateReviews(onDate: Date = Date(), completion:@escaping (Bool)->Void) {
        let dateString = convert(date: onDate)
        let hasReviews = dateWithReviews.contains(dateString)
        if hasReviews{
            filteredReviews = myReviews.filter({convert(date: $0.when) == dateString})
        }
        completion(dateWithReviews.contains(dateString))
    }
    
    func getReview(byRow: Int) -> CPRReview{
        return filteredReviews[byRow]
    }
    
    func getReviewCount() -> Int{
        return filteredReviews.count
    }
    
    func filterReviews(byDate date: Date, completion: @escaping (Bool)-> Void){
        let currentDate = convert(date: date)
        filteredReviews = myReviews.filter({convert(date: $0.when) == currentDate})
        selectedDate = date
        completion(getReviewCount()>0)
        
    }
    
    func getImage(forDate date: Date) -> UIImage?{
        let dateString = convert(date: date)
        if dateWithReviews.contains(dateString) {
            return  UIImage(named: "calendar_selected")
        }
        return nil
    }
}


extension CalendarVM{
    
    func createNewReview(){
        delegate.createNewReview()
    }
    
    func goToSelected(review: CPRReview){
        delegate.goToSelected(review: review)
    }
}
