//
//  ReviewDetailVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/20/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class ReviewDetailVM: NSObject, HasDependencies, CPRDataFormatter{
    
    typealias Dependencies = HasUser & HasDataService & HasGooglePlaceService & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    private var review: CPRReview
    
    var doneCompletion : (()->Void)?
    
    private var placeLocationView: PlaceLocation = {
        let view = PlaceLocation(frame: CGRect.zero)
        return view
    }()
    
    private var reviewDetailView: ReviewDetailView = {
        let view = ReviewDetailView(frame: CGRect.zero)
        return view
    }()

    private var place : CPRPlace?{
        return review.place
    }
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    init(dependencies: Dependencies, review: CPRReview){
        self.dependencies = dependencies
        self.review = review
        super.init()
        configurePlaceLocation()
        reviewDetailView.review = review
    }
    
    private func configurePlaceLocation(){
        guard let place = place else {return}
        let userCurrentLocation = dependencies?.currentUser?.currentLocation
        placeLocationView.placeLocationObject = (place: place, userCurrentLocation: userCurrentLocation)
    }
    
    
    func downloadImages(completion: @escaping (Int)->Void){
        dependencies?.dataService.downloadPictures(fromReview: review) { [weak self] images in
            guard let images = images, images.count > 0 else {completion(0);return}
            self?.review.images = images
            completion(images.count)
        }
    }
    
    func archiveReview(completion:@escaping (Result<Bool>) -> Void){
        var latestReviews: [CPRReview]?
        var cprError : CPRError?
        guard let currentUser = dependencies?.currentUser else {
            let error = CPRError(message: "Invalid User",errorType:.errorSavingReview)
            completion(.error(error))
            return
        }
        let dispatchGroup = DispatchGroup()
        dependencies?.dataService.archiveReview(review: review)
        dispatchGroup.enter()
        self.dependencies?.loadUserInfoService?.loadUserReviews(user: currentUser, completion: { result in
            switch result{
            case .success(let reviews):
                latestReviews = reviews
            case .error(let error):
                print(error)
                cprError = error
            }
            dispatchGroup.leave()
        })
        dispatchGroup.notify(queue: .main){
            guard let reviews = latestReviews, cprError == nil else{
                completion(.error(cprError!))
                return
            }
            self.dependencies?.currentUser?.reviews = reviews
            let userInfo = [CPRConstants.UserFields.reviews : reviews]
            NotificationCenter.default.post(name: .refreshUserReviews, object: nil, userInfo: userInfo)
            self.doneCompletion?()
            completion(.success(true))
        }
    }
    
}

extension ReviewDetailVM{
    
    func getPlaceName() -> String{
        return review.locationName
    }
    
    func getPlaceImage(completion: @escaping ((Result<Data>)->Void)){
        guard let photoReference = review.place?.googlePhotoReferences?.randomElement() else {
            let error = CPRError(message: "Invalid Photo Reference", errorType: .errorGettingData)
            completion(.error(error))
            return
        }
        let queue = DispatchQueue(label: "placePhoto", qos: .userInitiated)
        queue.async {
            self.dependencies?.googlePlacesService?.getPhoto(byReference: photoReference, completion: { result in
                completion(result)
                return
            })
        }
    }
}


extension ReviewDetailVM{
    
    func setDelegate(view: UIView, vc: UIViewController){
        if let view = (view as? ReviewDetailView), let delegate = vc as? ReviewDetailViewDelegate{
            view.delegate = delegate
        }
    }
    
    func getNumberOrRows() -> Int{
        return 6
    }
    
    func getView(forRow row: Int) -> UIView{
        switch row{
        case 2:
            return placeLocationView
        case 3:
            return reviewDetailView
        default:
            return UIView()
        }
    }
    
    func getTitleAndDescription(forRow row: Int) -> (title:String, description: String){
        switch row{
        case 2:
            return ("Directions", place?.address ?? "")
        case 3:
            return ("Review", "Created on \(convert(date: review.when, usingPattern: .full))")
        default:
            return ("","")
        }
    }
    
    func getImage(forRow row: Int) -> String?{
        switch row{
        case 2:
            return "place_description_compass"
        case 3:
            return "place_description_testimonial"
        default:
            return nil
        }
    }
    
    func getHeight(forRow row: Int) -> CGFloat{
        switch row {
        case 0:
            return 50
        case 1:
            return 50
        case 2:
            return 340
        case 3:
            return 300
        case 4:
            return 150
        case 5:
            return 70
        default:
            return 0
        }
    }
    
}


extension ReviewDetailVM{
    
    func getRating() -> Int{
        return review.rating
    }

    func getImageAt(index: Int) -> UIImage?{
        return review.images?[index]
    }
    
    func getRandomReviewImage() -> UIImage?{
        return review.images?.randomElement() ?? nil
    }
    
    func getComments() -> String{
        return review.comments
    }
    
    func getWhen() -> String{
        return convert(date:  review.when, usingPattern: .full)
    }
    
    func getCompanion() -> String{
        return review.companion
    }
   
    func getLocationName() -> String{
        return review.address
    }
    func getServer() -> String{
        return review.server
    }
    func getService() -> String{
        return review.service
    }
    func getReasonOfVisit() -> String{
        return review.reasonOfVisit
    }
    
    func getReviewShareText() -> String{
        return review.getShareText()
    }
    
    func getWaiting() -> Int{
        return review.waiting
    }
}
