//
//  OptionSelectorVM.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/14/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

class OptionSelectorVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasUser
    var dependencies: Dependencies?
    private var filteredOptions : [OptionSelectorConfiguration.Option]! = []
    private var configuration : OptionSelectorConfiguration!
    
    weak var delegate: SelectionOptionDelegate?
    
    var dataLoadCompleted : (()-> Void)?
    var sortDataCompleted : (()-> Void)?
    
    deinit{
        NotificationCenter.default.removeObserver(self)
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    init(dependencies: Dependencies, configuration: OptionSelectorConfiguration){
        self.configuration = configuration
        self.dependencies = dependencies
        super.init()
        registerNotifications()
    }
    
    func executeRequest(){
        configuration.loadOptionRequest?()
    }
    
    func getTitle() -> String{
        return configuration.title
    }
    
    func getDescription() -> String{
        return configuration.description
    }
    
    func getResultsCounter() -> String{
        return "Showing \(filteredOptions.count) results"
    }
    
    func selectOptionOn(index: Int){
        let option = getOptionBy(index: index)
        delegate?.select(option: option)
    }
    
    func getIcon() -> String{
        return configuration.icon
    }
    
    func filterBy(text: String){
        if text.trimmingCharacters(in: .whitespaces) == ""{
            filteredOptions = configuration.options
        }else{
            filteredOptions = configuration.options.filter({$0.name.uppercased().contains(text.uppercased())})
        }
    }
    
    func sortData(callCompletion : Bool = true){
        guard let index = configuration.sortConfiguration.sorters.firstIndex(where: {$0.selected}) else {return}
        configuration.sortConfiguration.sorters[index].sort()
        let options = configuration.sortConfiguration.objects.compactMap({($0 as? OptionSelectorConverter)?.convertToOption()})
        configuration.options = options
        filteredOptions = options
        if callCompletion{
            sortDataCompleted?()
        }
    }
    
    func getCount() -> Int{
        return filteredOptions.count
    }
    
    func getOptionBy(index: Int) -> OptionSelectorConfiguration.Option{
        return filteredOptions[index]
    }
    
    
    func getOptionSelectorSorter() -> OptionSelectorSorterViewDD{
        return OptionSelectorSorterViewDD(sortConfiguration: configuration.sortConfiguration)
    }
    
}


extension OptionSelectorVM{
    
    func registerNotifications(){
        NotificationCenter.default.addObserver(self, selector: #selector(OptionSelectorVM.dataLoaded(_:)), name: .optionSelectorDataLoaded, object: nil)
    }
    
    @objc private func dataLoaded(_ notification : Notification){
        guard let userInfo = notification.userInfo, let result = userInfo["result"] as? Result<SortConfiguration> else{
            configuration.sortConfiguration = SortConfiguration(objects: [])
            dataLoadCompleted?()
            return
        }
        switch result {
        case .success(let results):
            configuration.sortConfiguration = results
            sortData(callCompletion: false)
        case .error(let error):
            print(error)
        }
        dataLoadCompleted?()
    }
}
