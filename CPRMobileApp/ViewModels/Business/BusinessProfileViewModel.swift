//
//  BusinessProfileViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/19/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol BusinessProfileType {
    
    var places: [CPRPlace]? {get set}
    func calculateCPRAndGoogleAVGRating() -> (cprAVG: Double, googleAVG: Double)?
    func getTextForPlacesInBusiness() -> String
    func placesCount() -> Int
}

class BusinessProfileViewModel: NSObject, BusinessProfileType {
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    var places: [CPRPlace]?
    
    override init() {
    }
    
    private func calculateAVG(ratings: [Double]?) -> Double {
        guard let ratings = ratings, ratings.count > 0 else {
            return 0
        }
        let value = ratings.reduce(0, +) / Double(ratings.count)
        return Double(round(1000*value)/1000)
    }
    
    func calculateCPRAndGoogleAVGRating() -> (cprAVG: Double, googleAVG: Double)? {
//        if let places = places {
////            let totalRating = places.compactMap({$0.reviews?.compactMap({$0.rating}).reduce(0, +)}).reduce(0, +)
////            let totalReviews = places.compactMap({$0.reviews?.count}).reduce(0, +)
//            let cprAVGRating = totalReviews > 0 ? Double(totalRating) / Double(totalReviews) : 0
//            let googleAVGRating = calculateAVG(ratings: places.compactMap({Double($0.googleRating)}))
//            let avgrs = (cprAVG: cprAVGRating, googleAVG:googleAVGRating)
//            return avgrs
//        }
        return nil
    }
    
    func placesCount() -> Int {
        return places?.count ?? 0
    }
    
    func getTextForPlacesInBusiness() -> String {
        if placesCount() > 0 {
            let count = placesCount()
            return NSLocalizedString("There \(count == 1 ? "is" :"are") \(count) \(count == 1 ? "place" :"places") registered under this profile",
                comment: "String for business when it has places attached to it")
        } else {
            return NSLocalizedString("There is no a single place registered under this profile", comment: "No places for business comment")
        }
    }
    
}
