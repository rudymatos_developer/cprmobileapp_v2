//
//  SortReviewViewModel.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/18/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol SortingDelegate: class {
    func sort(by: Sort)
}

struct Sort: Equatable {
    var field: SortField
    var order: SortField.SortOrdering
}

enum SortField: String {
    case date = "sortByDate"
    case rating = "sortByRating"
    case businessDisplayName = "sortByPlaceName"
    enum SortOrdering: String {
        case ascending = "ascending"
        case descending = "descending"
    }
}

protocol SortType {
    var sortBy: DynamicType<Sort> {get}
    mutating func setSortOrdering(to: SortField.SortOrdering)
    mutating func setSortField(to: SortField)
    func isSortOrderingEquals(to: SortField.SortOrdering) -> Bool
    func isSortFieldEquals(to: SortField) -> Bool
}

class SortReviewVM: NSObject, SortType {
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    var sortBy: DynamicType<Sort> = DynamicType<Sort>(Sort(field: .date, order: .ascending))
    
    func setSortOrdering(to: SortField.SortOrdering) {
        sortBy.value.order = to
    }
    
    func setSortField(to: SortField) {
        sortBy.value.field = to
    }
    
    func isSortOrderingEquals(to: SortField.SortOrdering) -> Bool {
        return sortBy.value.order == to
    }
    func isSortFieldEquals(to: SortField) -> Bool {
        return sortBy.value.field == to
    }
    
}
