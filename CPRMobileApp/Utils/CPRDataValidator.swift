//
//  CPRDataValidator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/31/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol CPRDataValidator {
    func isValidEmail(email: String?) -> Bool
    func isEmpty(_ value: String?) -> Bool
    func isValidPassword(_ value: String?) -> Bool
    func isValidWebSite(website: String?) -> Bool
}

extension CPRDataValidator {
    
    func isValidPassword(_ value: String?) -> Bool {
        guard let value = value, value.trimmingCharacters(in: .whitespaces) != "",  value.count >= 8 else {
            return false
        }
        return true
    }
    
    func isValidWebSite(website: String?) -> Bool {
        if isEmpty(website) {return false}
        let websiteRegex = "\\w+(\\.\\w{2,})+"
        let websiteTest = NSPredicate(format: "SELF MATCHES %@", websiteRegex)
        return websiteTest.evaluate(with: website)
    }
    
    func isEmpty(_ value: String?) -> Bool {
        return value == nil || value?.trimmingCharacters(in: CharacterSet.whitespaces) == ""
    }
    
    func isValidEmail(email: String?) -> Bool {
        if isEmpty(email) {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
}
