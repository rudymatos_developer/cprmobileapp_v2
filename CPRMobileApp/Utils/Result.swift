//
//  Result.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/2/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

enum Result<T>{
    case success(T)
    case error(CPRError)
}
