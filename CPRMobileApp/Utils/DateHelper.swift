//
//  DateHelper.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/8/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation


class DateHelper{
    func convertStringToDate(withFormat dateFormat: String = "yyyy-MM-dd'T'hh:mm:ssZ", dateString : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: dateString) ?? Date()
    }
}
