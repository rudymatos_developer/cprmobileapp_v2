//
//  DynamicType.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/25/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

class DynamicType<T> {
    
    typealias Listener = (T)->Void
    var listener: Listener?

    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }

    func bind(_ listener: Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(_ listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
    
}
