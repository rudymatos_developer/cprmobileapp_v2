//
//  ClearBitHelper.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/16/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class ClearBitHelper {
    func loadCompanyLogo(website: URL?, onImageView: UIImageView) {
        onImageView.image =  UIImage(named: "logov2")
        onImageView.contentMode = .scaleAspectFit
        onImageView.isHidden = false
        if let website = website, let url = URL(string: "https://logo.clearbit.com/\(website)") {
            onImageView.loadImage(withURLString: url.absoluteString)
            onImageView.contentMode = .scaleToFill
        }
    }
}
