//
//  CPRDataFormatter.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/31/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

enum DatePattern: String{
    case short = "yyyy-MM-dd"
    case normal = "yyyy-MM-dd HH:mm:ss"
    case long = "yyyy-MM-dd'T'hh:mm:ssZ"
    case time = "HH:mm"
    case full
    case amadeus = "yyyy-MM-dd'T'HH:mm"
    case section = "LLLL yyyy"
    case justYear = "yyyy"
    case monthAndDay = "MMMM d"
}

protocol CPRDataFormatter {
    func convert(date: Date?, usingPattern: DatePattern) -> String
    func generateUUID() -> String
    func convert(string: String?, usingPattern: DatePattern) -> Date?
    func addSpace(toString: String) -> String
}

extension CPRDataFormatter {
    
    func addSpace(toString: String) -> String{
        guard toString.trimmingCharacters(in: .whitespaces) != "" && toString.count > 1 else {return toString}
        return toString.compactMap({"\($0) "}).reduce(""){$0 + $1}
    }
    
    func convert(date: Date?, usingPattern: DatePattern = .short) -> String {
        if let date = date {
            if usingPattern == .full{
                let prefix = convert(date: date, usingPattern: .monthAndDay)
                let ordinarySuffix = daySuffix(from: date)
                let suffix = convert(date: date, usingPattern: .justYear)
                return "\(prefix)\(ordinarySuffix) \(suffix)"
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = usingPattern.rawValue
                return dateFormatter.string(from: date)
            }
        }
        return ""
    }
    
    func generateUUID() -> String {
        return "\(NSUUID().uuidString)"
    }
    
    func generateWhitePlaceholderTextAttributeString(text: String, color: UIColor = UIColor.white) -> NSAttributedString {
        let attribute = NSMutableAttributedString(string: text)
        attribute.addAttributes([NSAttributedString.Key.foregroundColor: color], range: NSRange(location: 0, length: text.count))
        return attribute
    }
    
    func generateBoldAttributeString(text: String, fontSize: CGFloat = 12, range: Int) -> NSAttributedString{
        let attribute = NSMutableAttributedString(string: text)
        attribute.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: fontSize), NSAttributedString.Key.strokeColor: UIColor.black, NSAttributedString.Key.strokeWidth: -2.0], range: NSRange(location: 0, length: range))
        return attribute
    }
    
    func convert(string: String?, usingPattern: DatePattern = .short) -> Date? {
        if let string = string {
            if usingPattern == .full{
                return nil
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = usingPattern.rawValue
                return dateFormatter.date(from: string)
            }
        }
        return nil
    }
    
    private func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
}
