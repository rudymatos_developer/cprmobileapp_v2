//
//  CacheStorage.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 8/31/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class CacheStorage{
    static let sharedInstance = CacheStorage()
    private var cache = NSCache<NSString, UIImage>()
    
    private init(){
    }
    
    func getCachedImage(imageURLString: String) -> UIImage?{
        return cache.object(forKey: NSString(string: imageURLString))
    }
    
    func saveImage(withName: String, image: UIImage){
        cache.setObject(image, forKey: NSString(string:withName))
    }
}
