//
//  StoryBoardsNames.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/11/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardInitializer: class{
    static func getInstance(storyBoardName: String) -> Self
}

extension StoryboardInitializer where Self : UIViewController{
    static var identifier : String{
        return String(describing: self)
    }

    static func getInstance(storyBoardName: String) -> Self{
        let storyBoard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        print("🏀 initializing \(Self.identifier)")
        return storyBoard.instantiateViewController(withIdentifier: Self.identifier) as! Self
    }
}


