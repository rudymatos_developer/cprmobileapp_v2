//
//  ReviewsCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/19/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class ReviewsCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{
    
    var rootViewController: UINavigationController
    var parent: Coordinator?
    var childCoordinators: [Coordinator]
    typealias Dependencies = HasDataService & HasUser & HasAuthorizableService & HasLoadUserInfoService & HasSocialMediaService & HasUserDefaultService & HasBundleService & HasGooglePlaceService
    var dependencies: Dependencies?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
        childCoordinators = [Coordinator]()
        rootViewController = calendarNavVC
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    private var calendarVC: CalendarVC = {
        let viewController =  CalendarVC.getInstance(storyBoardName: "Calendar")
        viewController.tabBarItem = UITabBarItem(title: "Reviews", image: UIImage(named:"reviews"), selectedImage: UIImage(named:"reviews"))
        return viewController
    }()
    
    private var calendarNavVC: UINavigationController! = {
        let navBarVC = UINavigationController()
        navBarVC.navigationBar.isTranslucent = true
        navBarVC.navigationBar.barStyle = .blackTranslucent
        return navBarVC
    }()
    
    internal var newReviewCoordinator: NewReviewCoordinator!
    private var calendarVM : CalendarVM!
    
    func start() {
        rootViewController.delegate = self
        calendarNavVC.navigationBar.barStyle = .blackTranslucent
        navigate(to: .main)
    }
    
    func stop() {
    }
    
    enum Destination{
        case main
        case createReview
        case reviewDetail(CPRReview)
    }
}

extension ReviewsCoordinator: CalendarVMDelegate{
    func goToSelected(review: CPRReview) {
        navigate(to: .reviewDetail(review))
    }
    func createNewReview() {
        navigate(to: .createReview)
    }
}

extension ReviewsCoordinator: Navigator{
    func navigate(to: ReviewsCoordinator.Destination) {
        switch to{
        case .main:
            presentCalendarVC()
        case .reviewDetail(let review):
            presentReviewDetailVC(review)
        case .createReview:
            initializeNewReviewCoordinator(withPlace: nil)
        }
    }
}

extension ReviewsCoordinator: NewReviewDisplayable, ReviewDescriptionDisplayable {}

extension ReviewsCoordinator{
    private func presentCalendarVC() {
        calendarVM = CalendarVM(depedencies: dependencies!)
        calendarVM.delegate = self
        calendarVC.viewModel = calendarVM
        present(viewController: calendarVC, presentationStyle: .push(false))
    }
}

extension ReviewsCoordinator: UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let viewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(viewController) else { return }
        if viewController is NewReviewVC{
            removeChild(coordinator: &newReviewCoordinator)
        }
    }
}
