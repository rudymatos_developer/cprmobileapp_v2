//
//  AppCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/3/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import GooglePlaces
import FirebaseMessaging
import UserNotifications
import IQKeyboardManagerSwift
import OAuthSwift

class AppCoordinator: NSObject, Coordinator, HasRootViewController{
    
    weak var parent: Coordinator?
    var rootViewController = UINavigationController()
    var childCoordinators = [Coordinator]()
    
    let window: UIWindow
    
    private var appDependencies : AppDependencies!
    
    init(window: UIWindow){
        self.window = window
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
        start()
    }
    
    enum Destination{
        case splash
        case welcome
        case login
        case normal
        case business
    }
    
    func start(){
        registerExternalAPIs()
        initServices()
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        navigate(to: .splash)
    }
    
    func stop(){
        print("this method won't do much")
    }
}

extension AppCoordinator: Navigator{
    func navigate(to: AppCoordinator.Destination) {
        switch to {
        case .splash:
            presentSplashScreen()
        case .welcome:
            presentWelcomeScreens()
        case .login:
            presentLoginCoordinator()
        case .normal:
            presentUserCoordinator()
        case .business:
            print("going to business")
        }
    }
}

extension AppCoordinator{
    
    private func presentSplashScreen() {
        let splashVC = SplashScreenVC.getInstance(storyBoardName: "SplashScreen")
        splashVC.splashScreenAnimationCompletion = { [weak self]  in
            self?.loadUserInfo()
        }
        rootViewController.navigationBar.isHidden = true
        present(viewController: splashVC, presentationStyle: .crossDisolved)
    }
    
    private func presentWelcomeScreens() {
        let welcomeVC = WelcomeScreenVC.getInstance(storyBoardName: "Welcome")
        welcomeVC.startNowCompletion = {[weak self] in
            self?.navigate(to: .login)
        }
        let viewModel = WelcomeScreenVM(dependencies: appDependencies)
        welcomeVC.viewModel = viewModel
        rootViewController.navigationBar.isHidden = true
        present(viewController: welcomeVC, presentationStyle: .crossDisolved)
    }
    
    private func presentLoginCoordinator() {
        let loginCoordinator = LoginCoordinator(dependencies: appDependencies, rootViewController: rootViewController, parent: self)
        loginCoordinator.loginCoordinatorSuccessfullyCompletion = { [weak self]  destination in
            self?.rootViewController.popToRootViewController(animated: false)
            self?.navigate(to: destination)
        }
        rootViewController.navigationBar.isHidden = true
        loginCoordinator.start()
        addChild(coordinator: loginCoordinator)
    }
    
    private func presentUserCoordinator() {
        let tabBarController = UITabBarController()
        tabBarController.tabBar.tintColor = #colorLiteral(red: 0.9108691216, green: 0.2001005411, blue: 0.5882041454, alpha: 1)
        tabBarController.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.4862297177, green: 0.4863031507, blue: 0.4862136841, alpha: 1)
        window.rootViewController?.dismiss(animated: false, completion: nil)
        window.rootViewController = tabBarController
        let googlePlacesService = GooglePlaceService()
        appDependencies.googlePlacesService = googlePlacesService
        let userCoordinator = UserCoordinator(rootViewController: tabBarController, dependencies: appDependencies, parent: self)
        userCoordinator.logoutCompletion = { [weak self] in
            (self?.window.rootViewController as? UITabBarController)?.setViewControllers([], animated: false)
            self?.window.rootViewController?.dismiss(animated: false, completion: nil)
            let navigationController = UINavigationController()
            self?.rootViewController = navigationController
            self?.window.rootViewController = navigationController
            self?.appDependencies.googlePlacesService = nil
            self?.navigate(to: .login)
        }
        userCoordinator.start()
        userCoordinator.parent = self
        addChild(coordinator: userCoordinator)
    }
    
}

extension AppCoordinator{
    
    func loadUserInfo(){
        appDependencies.loadUserInfoService?.loadUserInfo { [weak self] result in
            switch result{
            case .success(let user):
                self?.navigate(to: user.isBusiness ? .business : .normal)
            case .error:
                self?.navigate(to: .welcome)
            }
        }
    }
    
    func initServices(){
        let userDefaultService = UserDefaultsService()
        let dataService = FirebaseDataService()
        let authorizableService = FirebaseAuthService(dataService: dataService)
        let socialMediaService = SocialMediaService()
        let bundleService = BundleService()
        appDependencies = AppDependencies(userDefaultService: userDefaultService,
                                          dataService: dataService,
                                          authorizableService: authorizableService,
                                          socialMediaService: socialMediaService,
                                          bundleService: bundleService)
        let loadUserInfoService = LoadUserInfoService(dependencies: appDependencies)
        appDependencies.loadUserInfoService = loadUserInfoService
    }
}

extension AppCoordinator{
    
    func handleOAuthSwiftRegistration(app: UIApplication, url: URL, options: [UIApplication.OpenURLOptionsKey: Any]){
        if (url.host == "oauth-swift" && options[.sourceApplication] as? String == "com.apple.mobilesafari"){
            OAuthSwift.handle(url: url)
        }
    }
    
    private func registerExternalAPIs(){
        FirebaseApp.configure()
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        IQKeyboardManager.shared.enable = true
        GMSPlacesClient.provideAPIKey(CPRURLs.GooglePlaces.key)
        Messaging.messaging().delegate = self
    }
    
    private func registerForNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, _) in
            guard granted else {return}
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                guard settings.authorizationStatus == .authorized else {return}
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }
    }
    
}

extension AppCoordinator: MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        appDependencies.userDefaultService.set(deviceFMCToken: fcmToken)
    }
    
    func handleReceivedNotification(userInfo: [AnyHashable: Any]){
        if let firebaseId = userInfo["gcm.notification.firebaseId"] as? String {
            let userDefaultHelper = UserDefaultsService()
            userDefaultHelper.set(firebaseIdFromNotification: firebaseId)
            #warning("Should load business screen from here")
//            UIApplication.shared.keyWindow?.rootViewController = screenToShow
            (window.rootViewController as? UITabBarController)?.selectedIndex = 0
        }
    }
}

