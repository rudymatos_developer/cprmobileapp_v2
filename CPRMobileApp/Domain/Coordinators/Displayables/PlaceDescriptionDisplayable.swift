//
//  PlaceDescriptionDisplayable.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/21/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol PlaceDescriptionDisplayable: class{
    var placeDescriptionCoordinator: PlaceDescriptionCoordinator! {get set}
    func presentPlaceDescription(withOption option: Section.Option)
}

extension PlaceDescriptionDisplayable where Self:Coordinator, Self: HasDependencies, Self: HasRootViewController{
   
    private func initPlaceDescriptionCoordinator(withPlace place: CPRPlace){
        placeDescriptionCoordinator = PlaceDescriptionCoordinator(rootViewController: rootViewController as! UINavigationController, dependencies: dependencies as! PlaceDescriptionCoordinator.Dependencies, place: place)
        placeDescriptionCoordinator.start()
        addChild(coordinator: placeDescriptionCoordinator)
    }
    
    func presentPlaceDescription(place: CPRPlace){
        initPlaceDescriptionCoordinator(withPlace: place)
    }
    
    func presentPlaceDescription(withOption option: Section.Option){
        guard let place = option.object as? CPRPlace else {return}
        initPlaceDescriptionCoordinator(withPlace: place)
    }
}
