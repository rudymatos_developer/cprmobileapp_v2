//
//  OptionSelectorDisplayable.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/21/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol OptionSelectorDisplayable: class{
    var searchItemCoordinator: SearchItemCoordinator! {get set}
    func presentOptionSelectorVC(configuration: OptionSelectorConfiguration)
}

extension OptionSelectorDisplayable where Self:Coordinator, Self: HasDependencies, Self: HasRootViewController, Self.ViewController == UINavigationController{
    func presentOptionSelectorVC(configuration: OptionSelectorConfiguration) {
        searchItemCoordinator = SearchItemCoordinator(dependencies: dependencies! as! SearchItemCoordinator.Dependencies, configuration: configuration)
        searchItemCoordinator.dismissCompletion = { [weak self] in
            guard let self = self else {return}
            self.removeChild(coordinator: &self.searchItemCoordinator)
        }
        searchItemCoordinator.start()
        present(viewController: searchItemCoordinator.rootViewController, presentationStyle: .modally(true))
        addChild(coordinator: searchItemCoordinator)
    }

}
