//
//  NewReviewDisplayable.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/27/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol NewReviewDisplayable: class{
    var newReviewCoordinator: NewReviewCoordinator! {get set}
    func initializeNewReviewCoordinator(withPlace: CPRPlace?)
}

extension NewReviewDisplayable where Self:Coordinator, Self: HasDependencies, Self: HasRootViewController{
    func initializeNewReviewCoordinator(withPlace place: CPRPlace?){
        if place != nil{
            newReviewCoordinator = NewReviewCoordinator(rootViewController: rootViewController as! UINavigationController, dependencies: dependencies! as! NewReviewCoordinator.Dependencies, place: place!)
        }else{
            newReviewCoordinator = NewReviewCoordinator(rootViewController: rootViewController as! UINavigationController, dependencies: dependencies! as! NewReviewCoordinator.Dependencies)
        }
        newReviewCoordinator.start()
        addChild(coordinator: newReviewCoordinator)
    }
}
