//
//  LoginCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/5/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class LoginCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{

    weak var parent: Coordinator?
    var rootViewController: UINavigationController
    var childCoordinators: [Coordinator]
    
    typealias Dependencies = HasUserDefaultService & HasAuthorizableService & HasDataService & HasUser & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    var loginCoordinatorSuccessfullyCompletion: ((AppCoordinator.Destination) -> Void)?
    
    var loginVC : LoginVC = {
        let viewController = LoginVC.getInstance(storyBoardName: "Login")
        return viewController
    }()
    
    init(dependencies: Dependencies, rootViewController: UINavigationController, parent: Coordinator?){
        self.dependencies = dependencies
        self.parent = parent
        self.rootViewController = rootViewController
        childCoordinators = [Coordinator]()
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    func start() {
        navigate(to: .login)
    }
    
    func stop() {
    }
}


extension LoginCoordinator: Navigator{
    
    enum Destination{
        case login
        case forgotPassword
        case registration
    }
    
    func navigate(to: LoginCoordinator.Destination) {
        guard let dependencies = dependencies else{
            fatalError("Dependencies were not met to Initialize Login Coordinator")
        }
        switch to {
        case .login:
            loginVC.viewModel = LoginVM(dependencies: dependencies, loginCoordinator: self)
            present(viewController: loginVC, presentationStyle: .modallyWithPush(true))
        case .forgotPassword:
            let forgotPasswordVC = AddTextInfoVC.getInstance(storyBoardName: "Generic")
            let tsp = TextSetterUIProperties(title: "Reset Password",
                                             description: "Please provide your username",
                                             placeHolder: "Username/Email",
                                             validator: .email,
                                             shouldBeAbleToCancel: true)
            forgotPasswordVC.textSetterUIProperties = tsp
            forgotPasswordVC.delegate = loginVC
            forgotPasswordVC.modalPresentationStyle = .overCurrentContext
            loginVC.present(forgotPasswordVC, animated: true, completion: nil)
        case .registration:
            let registrationVC = RegistrationVC.getInstance(storyBoardName: "Login")
            registrationVC.viewModel = RegisterUserVM(dependencies: dependencies, loginCoordinator: self)
            registrationVC.modalPresentationStyle = .overCurrentContext
            loginVC.present(registrationVC, animated: true, completion: nil)

        }
    }
}
