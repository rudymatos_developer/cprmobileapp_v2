//
//  NewReviewCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/27/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class NewReviewCoordinator: NSObject, Coordinator, HasRootViewController, HasDependencies{
    
    typealias Dependencies = HasUser & HasBundleService & HasDataService & HasGooglePlaceService & HasLoadUserInfoService & HasSocialMediaService & HasUser & HasUserDefaultService
    var dependencies: Dependencies?
    
    var rootViewController: UINavigationController
    var parent: Coordinator?
    var childCoordinators: [Coordinator]
    
    private var place: CPRPlace?
    
    init(rootViewController: UINavigationController, dependencies: Dependencies){
        self.rootViewController = rootViewController
        self.dependencies = dependencies
        self.childCoordinators = []
        super.init()
        print("🏀 initializing \(String(describing: self))")
    }

    convenience init(rootViewController: UINavigationController, dependencies: Dependencies, place: CPRPlace){
        self.init(rootViewController: rootViewController, dependencies: dependencies)
        self.place = place
    }
    
    func start() {
        navigate(to: .main)
    }
    
    func stop() {
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    enum Destination{
        case main
        case shareReview(CPRReview)
    }
    
}

extension NewReviewCoordinator: Navigator{
    
    func navigate(to: NewReviewCoordinator.Destination) {
        switch to{
        case .main:
            presentCreateReviewVC()
        case .shareReview(let review):
            presentShareReviewScreen(review: review)
        }
    }
    
    private func presentShareReviewScreen(review: CPRReview){
        let shareReviewVC = ShareReviewVC.getInstance(storyBoardName: "Share")
        let socialProfiles = dependencies?.userDefaultService.getSocialProfiles()
        let viewModel = ShareReviewVM(dependencies: dependencies!, socialProfiles: socialProfiles, review: review)
        shareReviewVC.viewModel = viewModel
        viewModel.shareCompletion = {[weak self] in
            self?.rootViewController.popToRootViewController(animated: true)
        }
        present(viewController: shareReviewVC, presentationStyle: .modally(true))
    }
    
    private func presentCreateReviewVC() {
        let createNormalReviewVC = NewReviewVC.getInstance(storyBoardName: "NewReview")
        let viewModel : NewReviewVM
        if place != nil {
            viewModel = NewReviewVM(dependencies: dependencies!,place: place!)
        }else{
            viewModel = NewReviewVM(dependencies: dependencies!)
        }
        createNormalReviewVC.viewModel = viewModel
        viewModel.saveReviewCompletion = {[weak self] _, review in
            guard let self = self, let review = review else {return}
            self.navigate(to: .shareReview(review))
        }
        present(viewController: createNormalReviewVC, presentationStyle: .push(true))
    }
    
}


