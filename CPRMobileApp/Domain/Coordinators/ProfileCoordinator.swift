//
//  ProfileCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/21/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class ProfileCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{
    
    weak var parent: Coordinator?
    var rootViewController: UINavigationController
    var childCoordinators: [Coordinator]
    
    typealias Dependencies = HasDataService & HasUser & HasUserDefaultService & HasAuthorizableService & HasLoadUserInfoService
    var dependencies: Dependencies?
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    init(dependencies: Dependencies, parent: Coordinator){
        self.rootViewController = mainNavVC
        self.dependencies = dependencies
        self.parent = parent
        childCoordinators = [Coordinator]()
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    internal var searchItemCoordinator : SearchItemCoordinator!
    internal var placeDescriptionCoordinator : PlaceDescriptionCoordinator!
    private var profileViewModel : ProfileVM!

    var profileVC: ProfileVC = {
        let viewController = ProfileVC.getInstance(storyBoardName: "Profile")
        viewController.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "profile"), selectedImage: UIImage(named: "profile"))
        return viewController
    }()
    
    private var mainNavVC: UINavigationController! = {
        let navBarVC = UINavigationController()
        navBarVC.navigationBar.isHidden = true
        return navBarVC
    }()
    
    func start(){
        rootViewController.delegate = self
        navigate(to: .main)
    }
    
    func stop(){
    }
    
    enum Destination{
        case main
        case addAdditionalUserInformation
        case seeAll(OptionSelectorConfiguration)
    }
}

extension ProfileCoordinator: PlaceDescriptionDisplayable, ReviewDescriptionDisplayable, OptionSelectorDisplayable{}

extension ProfileCoordinator: Navigator{
    func navigate(to: ProfileCoordinator.Destination) {
        switch to {
        case .main:
            initializeProfileVC()
        case .seeAll(let configuration):
               presentOptionSelectorVC(configuration: configuration)
        case .addAdditionalUserInformation:
            showAddAdditionalUserInfoVC()
        }
    }
}

extension ProfileCoordinator{
    private func initializeProfileVC() {
        profileViewModel = ProfileVM(dependencies: dependencies!, userCoordinator: parent as! UserCoordinator)
        profileVC.viewModel = profileViewModel
        profileViewModel.delegate = self
        present(viewController: profileVC, presentationStyle: .push(true))
    }
    
    private func showAddAdditionalUserInfoVC(){
        let additionalUserInfo = AdditionalUserInformationVC.getInstance(storyBoardName: "Profile")
        additionalUserInfo.modalPresentationStyle = .overCurrentContext
        let viewModel = AdditionalUserInformationVM(dependencies: dependencies!)
        additionalUserInfo.viewModel = viewModel
        profileVC.present(additionalUserInfo, animated: true, completion: nil)
    }
    
}

extension ProfileCoordinator: ProfileVMDelegate{
    func showAll(configuration: OptionSelectorConfiguration){
        navigate(to: .seeAll(configuration))
    }
    
    func goToAddAdditionalInformation(){
        navigate(to: .addAdditionalUserInformation)
    }
    
}

extension ProfileCoordinator: UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let viewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(viewController) else { return }
        if viewController is OptionSelectorVC{
            removeChild(coordinator: &searchItemCoordinator)
        }else if viewController is PlaceDescriptionVC {
            removeChild(coordinator: &placeDescriptionCoordinator)
        }else if viewController is NewReviewVC{
            if navigationController.viewControllers.filter({$0 is PlaceDescriptionVC}).count > 0{
                placeDescriptionCoordinator.stopChild()
            }else{
                removeChild(coordinator: &placeDescriptionCoordinator)
            }
        }
    }
}
