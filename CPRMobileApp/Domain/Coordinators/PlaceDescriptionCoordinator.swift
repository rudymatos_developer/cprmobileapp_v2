//
//  PlaceDescriptionCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/15/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class PlaceDescriptionCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{
    
    var parent: Coordinator?
    var rootViewController: UINavigationController
    var childCoordinators: [Coordinator]
    
    typealias Dependencies = HasDataService & HasGooglePlaceService & HasUser & HasBundleService & HasLoadUserInfoService & HasSocialMediaService & HasUserDefaultService
    var dependencies: Dependencies?
    
    private var place: CPRPlace
    
    init(rootViewController: UINavigationController, dependencies: Dependencies,  place: CPRPlace){
        self.rootViewController = rootViewController
        self.dependencies = dependencies
        self.place = place
        childCoordinators = [Coordinator]()
        super.init()
        print("🏀 initializing \(String(describing: self))")
    }
    
    private var placeDescriptionVC : PlaceDescriptionVC = {
        let viewController = PlaceDescriptionVC.getInstance(storyBoardName: "PlaceDescription")
        return viewController
    }()
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    internal var newReviewCoordinator: NewReviewCoordinator!
    
    enum Destination{
        case main
        case createReview(CPRPlace)
        case reviewDetails(CPRReview)
    }
    
    func start() {
        navigate(to: .main)
    }
    
    func stopChild(){
        removeChild(coordinator: &newReviewCoordinator)
    }
    
    func stop() {
        removeChild(coordinator: &newReviewCoordinator)
    }
    
}
extension PlaceDescriptionCoordinator: Navigator{
    func navigate(to: PlaceDescriptionCoordinator.Destination) {
        switch to {
        case .main:
            presentPlaceDescriptionVC()
        case .reviewDetails(let review):
            presentReviewDetailVC(review)
        case .createReview(let place):
            initializeNewReviewCoordinator(withPlace: place)
        }
    }
}

extension PlaceDescriptionCoordinator: PlaceDescriptionVMDelegate{
    func displayReviewDetails(review: CPRReview) {
        navigate(to: .reviewDetails(review))
    }
    func createNewReview(withPlace place: CPRPlace){
        navigate(to: .createReview(place))
    }
}

extension PlaceDescriptionCoordinator: ReviewDescriptionDisplayable, NewReviewDisplayable{
    
    private func presentPlaceDescriptionVC(){
        let viewModel = PlaceDescriptionVM(dependencies: dependencies!, place: place)
        placeDescriptionVC.viewModel = viewModel
        viewModel.delegate = self
        present(viewController: placeDescriptionVC, presentationStyle: .push(true))
    }
    
}
