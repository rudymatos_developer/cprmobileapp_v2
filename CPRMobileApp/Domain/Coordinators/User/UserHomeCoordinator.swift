//
//  UserHomeCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/14/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class UserHomeCoordinator: NSObject, HasDependencies, Coordinator, HasRootViewController{
    
    weak var parent: Coordinator?
    var rootViewController: UINavigationController
    var childCoordinators: [Coordinator]
    
    typealias Dependencies = HasBundleService & HasDataService & HasGooglePlaceService & HasLoadUserInfoService & HasSocialMediaService & HasUser & HasUserDefaultService
    var dependencies: Dependencies?
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
        self.rootViewController = mainNavVC
        childCoordinators = [Coordinator]()
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
   private var mainVC : UserHomeVC = {
        let viewController = UserHomeVC.getInstance(storyBoardName: "UserHome")
        viewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "feed"), selectedImage: UIImage(named: "feed"))
        return viewController
    }()
    
    private var mainNavVC: UINavigationController! = {
        let navBarVC = UINavigationController()
        navBarVC.navigationBar.isHidden = true
        return navBarVC
    }()
    
    internal var searchItemCoordinator : SearchItemCoordinator!
    internal var placeDescriptionCoordinator : PlaceDescriptionCoordinator!
    private var mainViewModel : UserHomeVM!
    
    func start() {
        rootViewController.delegate = self
        initializeMainVC()
    }
    
    func stop() {
        parent = nil
    }
    
    enum Destination{
        case itemDescription(Section.Option)
        case placeDescription(CPRPlace)
        case seeAll(OptionSelectorConfiguration)
        case seeNearBy(OptionSelectorConfiguration)
        case reviewDetails(CPRReview)
    }
}

extension UserHomeCoordinator: Navigator{
    func navigate(to: UserHomeCoordinator.Destination) {
        switch to {
        case .itemDescription(let option):
            if option.object is CPRPlace{
                presentPlaceDescription(withOption: option)
            }else if let review = option.object as? CPRReview{
                presentReviewDetailVC(review)
            }
        case .placeDescription(let place):
            presentPlaceDescription(place: place)
        case .seeNearBy(let configuration), .seeAll(let configuration):
            presentOptionSelectorVC(configuration: configuration)
        case .reviewDetails(let review):
            presentReviewDetailVC(review)
        }
    }
}

extension UserHomeCoordinator: PlaceDescriptionDisplayable, ReviewDescriptionDisplayable, OptionSelectorDisplayable{
}

extension UserHomeCoordinator{

    private func initializeMainVC() {
        mainViewModel = UserHomeVM(dependencies: dependencies!)
        mainViewModel.delegate = self
        mainVC.viewModel = mainViewModel
        present(viewController: mainVC, presentationStyle: .push(true))
    }
}

extension UserHomeCoordinator: UserHomeVMDelegate{
    func goToMainOptionBasedOnCategory(selectionType: OptionSelectorConfiguration){
        navigate(to: .seeNearBy(selectionType))
    }
    
    func goToPlaceDescription(place: CPRPlace){
        navigate(to: .placeDescription(place))
    }
    
    func goToSelectedItem(option: Section.Option) {
        navigate(to: .itemDescription(option))
    }
    
    func goToSeeAll(selectionType: OptionSelectorConfiguration){
        navigate(to: .seeAll(selectionType))
    }
}

extension UserHomeCoordinator: UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let viewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(viewController) else { return }
        if viewController is OptionSelectorVC{
            removeChild(coordinator: &searchItemCoordinator)
        }else if viewController is PlaceDescriptionVC {
            removeChild(coordinator: &placeDescriptionCoordinator)
        }else if viewController is NewReviewVC{
            if navigationController.viewControllers.filter({$0 is PlaceDescriptionVC}).count > 0{
                placeDescriptionCoordinator.stopChild()
            }else{
                removeChild(coordinator: &placeDescriptionCoordinator)
            }
        }
    }
}


