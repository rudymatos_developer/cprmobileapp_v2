//
//  NormalUserCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/7/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class UserCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController,Logoutable{
    
    weak var parent: Coordinator?
    var rootViewController: UITabBarController
    var childCoordinators: [Coordinator]
    
    typealias Dependencies = HasDataService & HasUserDefaultService & HasAuthorizableService &  HasUser & HasLoadUserInfoService & HasSocialMediaService &  HasGooglePlaceService & HasBundleService
    var dependencies: Dependencies?
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    var logoutCompletion : (()->Void)?
    
    private var reviewCoordinator: ReviewsCoordinator!
    private var userHomeCoordinator: UserHomeCoordinator!
    private var profileCoordinator: ProfileCoordinator!
    
    init(rootViewController: UITabBarController, dependencies: Dependencies, parent: Coordinator){
        self.rootViewController = rootViewController
        self.dependencies = dependencies
        self.parent = parent
        childCoordinators = [Coordinator]()
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    func start() {
        rootViewController.tabBar.barStyle = .black
        initializeCoordinators()
        rootViewController.setViewControllers([userHomeCoordinator.rootViewController,
                                               reviewCoordinator.rootViewController,
                                               profileCoordinator.rootViewController], animated: false)
    }
    
    func stop() {
    }
    
}


extension UserCoordinator{
    
    private func initializeCoordinators(){
        initializeUserHomeCoordinator()
        initilizeReviewCoordinator()
        initializeProfileVC()
    }
    
    private func initializeProfileVC() {
        profileCoordinator = ProfileCoordinator(dependencies: dependencies!, parent: self)
        profileCoordinator.start()
        addChild(coordinator: profileCoordinator)
    }
    
    private func initializeUserHomeCoordinator(){
        userHomeCoordinator = UserHomeCoordinator(dependencies: dependencies!)
        userHomeCoordinator.start()
        addChild(coordinator: userHomeCoordinator)
    }
    
    private func initilizeReviewCoordinator(){
        reviewCoordinator = ReviewsCoordinator(dependencies: dependencies!)
        reviewCoordinator.start()
        addChild(coordinator: reviewCoordinator)
    }
    
}


