//
//  ProductListCoordinator.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/13/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

class SearchItemCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{
    
    typealias Dependencies = HasDataService & HasUser & HasGooglePlaceService & HasBundleService & HasLoadUserInfoService & HasSocialMediaService & HasUserDefaultService
    var dependencies: Dependencies?
    
    var rootViewController: UINavigationController
    weak var parent: Coordinator?
    var childCoordinators: [Coordinator]
    private let configuration: OptionSelectorConfiguration
    
    var dismissCompletion: (() -> Void)?
    
    init(dependencies: Dependencies,  configuration: OptionSelectorConfiguration){
        print("🏀 initializing SearchItemCoordinator")
        self.rootViewController = navVC
        self.dependencies = dependencies
        self.configuration = configuration
        childCoordinators = [Coordinator]()
    }
    
    private var navVC : UINavigationController = {
        let navigationVC = UINavigationController()
        navigationVC.navigationBar.isHidden = true
        return navigationVC
    }()
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }

    private var placeDescriptionCoordinator: PlaceDescriptionCoordinator!
    
    enum Destination{
        case main
        case itemDescription(Any?)
    }
    
    func start() {
        rootViewController.delegate = self
        initializeOptionSelectorVC()
    }
    
    func stop() {
    }
}

extension SearchItemCoordinator: Navigator{
    func navigate(to: SearchItemCoordinator.Destination) {
        switch to{
        case .main:
            initializeOptionSelectorVC()
        case .itemDescription(let item):
            guard let item = item, (item is CPRReview || item is CPRPlace) else {return}
            if let review  = (item as? CPRReview){
                presentReviewDetailVC(review)
            }else if let place  = (item as? CPRPlace){
                presentPlaceDescription(place: place)
            }
        }
    }
}

extension SearchItemCoordinator{
    
    private func initializeOptionSelectorVC(){
        let optionSelectorVC = OptionSelectorVC.getInstance(storyBoardName: "OptionSelector")
        let viewModel = OptionSelectorVM(dependencies: dependencies!, configuration: configuration)
        optionSelectorVC.viewModel = viewModel
        viewModel.delegate = self
        optionSelectorVC.dismissCompletion = dismissCompletion
        navVC.pushViewController(optionSelectorVC, animated: false)
    }

    private func presentPlaceDescription(place: CPRPlace){
        placeDescriptionCoordinator = PlaceDescriptionCoordinator(rootViewController: rootViewController, dependencies: dependencies!, place: place)
        placeDescriptionCoordinator.start()
        addChild(coordinator: placeDescriptionCoordinator)
    }
}

extension SearchItemCoordinator: ReviewDescriptionDisplayable{
    
}
extension SearchItemCoordinator: SelectionOptionDelegate{
    func select(option: OptionSelectorConfiguration.Option) {
        navigate(to: .itemDescription(option.object))
    }
}

extension SearchItemCoordinator: UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let viewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(viewController) else { return }
        if viewController is PlaceDescriptionVC {
            removeChild(coordinator: &placeDescriptionCoordinator)
        }else if viewController is NewReviewVC{
            if navigationController.viewControllers.filter({$0 is PlaceDescriptionVC}).count > 0{
                placeDescriptionCoordinator.stopChild()
            }else{
                removeChild(coordinator: &placeDescriptionCoordinator)
            }
        }
    }
}
