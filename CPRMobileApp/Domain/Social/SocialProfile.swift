//
//  SocialProfile.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/9/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

class SocialProfile: Codable{
    
    var username: String?
    var oauthToken : String?
    var oauthTokenSecret : String?
    var profilePictureURL : URL?
    var name : String?
    var bio: String?
    var socialNetwork : SocialNetwork
    var shouldShare: Bool = false
    
    init(with socialNetwork: SocialNetwork){
        self.socialNetwork = socialNetwork
        self.name = ""
    }
    
    init(username: String, name: String,  oauthToken: String, oauthTokenSecret: String, socialNetwork: SocialNetwork){
        self.username = username
        self.oauthToken = oauthToken
        self.oauthTokenSecret = oauthTokenSecret
        self.name = name
        self.socialNetwork = socialNetwork
    }
    
    init( oauthToken: String, socialNetwork: SocialNetwork){
        self.oauthToken = oauthToken
        self.socialNetwork = socialNetwork
    }
    
    func isSocialAvailable() -> Bool{
        if socialNetwork.getOAuthType() == .oauth1 && oauthToken != nil && oauthTokenSecret != nil{
            return true
        }
        if socialNetwork.getOAuthType() == .oauth2 && oauthToken != nil{
            return true
        }
        return false
    }
    
    func isImplementationAvailable() -> Bool{
        return socialNetwork == .twitter
    }

    func getUsernameOrName() -> String{
        if let name = name{
            return name
        }
        return username ?? "NAME NOT AVAILABLE"
    }
    
}
