//
//  SocialProfile.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/31/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import OAuthSwift
import KeychainAccess

enum SocialNetwork: String, Codable, CaseIterable{
    
    case twitter = "twitter"
    case pinterest = "pinterest"
    case facebook = "facebook"
    
    func getOAuthType() -> OAuthType{
        switch self{
        case .twitter:
            return .oauth1
        case .pinterest, .facebook:
            return .oauth2
        }
    }
    
    func getShareToText() -> String{
        switch self{
        case .twitter:
            return "Share to Twitter"
        case .pinterest:
            return "Share to Pinterest"
        case .facebook:
            return "Share to Facebook"
        }
    }
    
    func getOAuthInstance() -> OAuthSwift{
        var oauthSwift : OAuthSwift
        switch self {
        case .twitter:
            oauthSwift = OAuth1Swift(consumerKey: CPRURLs.Twitter.consumerKey,
                                     consumerSecret: CPRURLs.Twitter.consumerSecret,
                                     requestTokenUrl: CPRURLs.Twitter.requestTokenUrl,
                                     authorizeUrl: CPRURLs.Twitter.authorizeUrl,
                                     accessTokenUrl:  CPRURLs.Twitter.accessTokenUrl)
        case .pinterest:
            oauthSwift = OAuth2Swift(consumerKey: CPRURLs.Pinterest.consumerKey,
                                     consumerSecret: CPRURLs.Pinterest.consumerSecret,
                                     authorizeUrl: CPRURLs.Pinterest.authorizeUrl,
                                     accessTokenUrl: CPRURLs.Pinterest.accessTokenUrl,
                                     responseType: CPRURLs.Pinterest.responseType)
        
        case .facebook:
            oauthSwift = OAuth2Swift(consumerKey: CPRURLs.Pinterest.consumerKey,
                                     consumerSecret: CPRURLs.Pinterest.consumerSecret,
                                     authorizeUrl: CPRURLs.Pinterest.authorizeUrl,
                                     accessTokenUrl: CPRURLs.Pinterest.accessTokenUrl,
                                     responseType: CPRURLs.Pinterest.responseType)
            
        }
        return oauthSwift
    }
    
    func getScope() -> String{
        switch self {
        case .pinterest, .facebook:
            return CPRURLs.Pinterest.scope
        default:
           return  ""
        }
    }
    
    func getState() -> String{
        switch self {
        case .pinterest, .facebook:
            return CPRURLs.Pinterest.state
        default:
            return ""
        }
    }

    func getCallbackURL() -> String{
        switch self {
        case .twitter:
            return CPRURLs.Twitter.callbackURL
        case .pinterest:
            return CPRURLs.Pinterest.callback
        case .facebook:
            return CPRURLs.Pinterest.callback
        }
    }
    
    enum OAuthType{
        case oauth1
        case oauth2
    }
}

