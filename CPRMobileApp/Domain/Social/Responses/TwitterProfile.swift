//
//  TwitterProfile.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/12/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//
import Foundation

struct TwitterProfile: Codable{
    
    let id: Int
    let name: String
    let description : String
    let profileImage: String
    
    enum CodingKeys: String,CodingKey{
        case id
        case name
        case description
        case profileImage = "profile_image_url_https"
    }
    
}
