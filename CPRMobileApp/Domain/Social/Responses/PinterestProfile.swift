//
//  PinterestProfile.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/12/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

struct PinterestProfile: Codable{
    var data: Data
    
    struct Data: Codable{
        var username: String
        var bio: String
        var firstName : String
        var lastName: String
        var url: String
        var id: String
        var image : PinterestProfileImage
        
        enum CodingKeys: String, CodingKey{
            case username
            case bio
            case firstName = "first_name"
            case lastName = "last_name"
            case url
            case id
            case image
        }
        
        struct PinterestProfileImage : Codable{
            var imageComponent: PinterestProfileImageComponent
            
            enum CodingKeys: String, CodingKey{
                case imageComponent = "60x60"
            }
            
            struct PinterestProfileImageComponent: Codable{
                var url: String
                var width: Int
                var height : Int
            }
        }
    }
}

