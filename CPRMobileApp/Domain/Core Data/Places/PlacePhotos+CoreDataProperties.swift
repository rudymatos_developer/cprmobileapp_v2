//
//  PlacePhotos+CoreDataProperties.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/16/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//
//

import Foundation
import CoreData


extension PlacePhotos {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PlacePhotos> {
        return NSFetchRequest<PlacePhotos>(entityName: "PlacePhotos")
    }

    @NSManaged public var height: Int16
    @NSManaged public var width: Int16
    @NSManaged public var photoReference: String?
    @NSManaged public var place: Place?

}
