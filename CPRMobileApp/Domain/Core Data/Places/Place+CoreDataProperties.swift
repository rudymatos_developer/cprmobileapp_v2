//
//  Place+CoreDataProperties.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/16/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//
//

import Foundation
import CoreData


extension Place {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Place> {
        return NSFetchRequest<Place>(entityName: "Place")
    }

    @NSManaged public var placeId: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String?
    @NSManaged public var address: String?
    @NSManaged public var globalCode: String?
    @NSManaged public var googleRating: Double
    @NSManaged public var priceLevel: Int16
    @NSManaged public var photos: NSSet?

}

// MARK: Generated accessors for photos
extension Place {

    @objc(addPhotosObject:)
    @NSManaged public func addToPhotos(_ value: PlacePhotos)

    @objc(removePhotosObject:)
    @NSManaged public func removeFromPhotos(_ value: PlacePhotos)

    @objc(addPhotos:)
    @NSManaged public func addToPhotos(_ values: NSSet)

    @objc(removePhotos:)
    @NSManaged public func removeFromPhotos(_ values: NSSet)

}
