//
//  NewReviewObject.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/23/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

struct NewReviewObject{
    
    var place: DynamicType<CPRPlace>?
    var photos: DynamicType<[UIImage]>?
    var date: DynamicType<Date>?
    var server: DynamicType<String>
    var service: DynamicType<String>
    var reasonOfVisit: DynamicType<String>
    var companion: DynamicType<String>
    var comment: DynamicType<String>
    var rating: DynamicType<Int>
    var waiting: DynamicType<Int>
    
    init(){
        server = DynamicType<String>("")
        service = DynamicType<String>("")
        reasonOfVisit = DynamicType<String>("")
        companion = DynamicType<String>("")
        comment = DynamicType<String>("")
        rating = DynamicType<Int>(2)
        waiting = DynamicType<Int>(2)
    }
    
    init(place: CPRPlace){
        self.init()
        self.place = DynamicType<CPRPlace>(place)
    }
    
    
    func convertToReview() -> CPRReview?{
        return CPRReview(data: [:])
    }
}
