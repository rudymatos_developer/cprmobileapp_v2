//
//  OptionSelectorConfiguration.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/14/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol OptionConfigurationConverter{
    func getOptionSelectorConfigurator() -> OptionSelectorConfiguration
}

protocol OptionSelectorConverter{
    func convertToOption() -> OptionSelectorConfiguration.Option
}

struct OptionSelectorConfiguration{
    
    var title: String
    var description: String
    var icon: String
    var category: Any?
    var options : [Option]
    var loadOptionRequest: (() -> Void)?
    var sortConfiguration: SortConfiguration!
    
    init(title: String, description: String, icon: String, category: Any?){
        self.title = title
        self.description = description
        self.icon = icon
        self.category = category
        options = []
        loadOptionRequest = nil
    }
    
    init(category: MainCategory){
        let description = "Showing results for \(category.getDisplayName())"
        let icon = category.getIconName()
        self.init(title: "Looking for Something?", description: description, icon: icon, category: category)
    }
    
    struct Option{
        var icon: String?
        var name: String
        var description : String
        var object: Any
    }
}
