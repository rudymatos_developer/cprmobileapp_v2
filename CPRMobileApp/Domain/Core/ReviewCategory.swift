//
//  Category.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 5/13/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

class ReviewCategory {
    var name: String
    var isSelected: Bool
    init(name: String, isSelected: Bool) {
        self.name = name
        self.isSelected = isSelected
    }
}
