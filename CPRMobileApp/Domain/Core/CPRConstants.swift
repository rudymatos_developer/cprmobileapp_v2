//
//  FirebaseConstants.swift
//  cpr
//
//  Created by Rudy E Matos on 11/21/17.
//  Copyright Â© 2017 Bearded Gentleman. All rights reserved.
//

import Foundation



enum CPRConstants {
    
    static let logoName = "cprlogo_v4"
    
    enum CustomGOCellIdentifiers{
        static let goIconCell = "goIconCell"
        static let titleCell = "titleCell"
        static let itemDescriptionCell = "itemDescriptionCell"
        static let mainHeader = "mainHeader"
    }
    
    
    enum RegistrationType {
        case personal
        case business
    }
    
    enum PurchaseFields{
        static let firebaseId = "firebaseId"
        static let dateOfPurchase = "dateOfPurchase"
        static let type = "type"
        static let hasReview = "hasReview"
        static let price = "price"
        static let userPurchaseId = "userPurchaseId"
        static let item = "item"
    }
    
    enum UserFields {
        static let email = "email"
        static let fullName = "fullName"
        static let firstName = "firstName"
        static let surname = "surname"
        static let mobile = "mobile"
        static let gender = "gender"
        static let maritalStatus = "maritalStatus"
        static let displayName = "displayName"
        static let countryCode = "countryCode"
        static let dateOfBirth = "dateOfBirth"
        static let profilePicture = "profilePicture"
        static let profilePictureURL = "profilePictureURL"
        static let reviews = "reviews"
        static let purchases = "purchases"
        static let password  = "password"
        static let companyPosition = "companyPosition"
        static let companyDisplayName = "companyDisplayName"
        static let isBusiness = "isBusiness"
        static let loginType = "loginType"
        static let businessPlaces = "businessPlaces"
        static let businessReviews = "businessReviews"
        static let username = "username"
        static let crpAVGRAting = "crpAVGRating"
        static let googleAVGRating = "googleAVGRating"
        static let tokens = "tokens"
        
        enum UserPlaces: String{
            case favoritePlaces = "favoritePlaces"
            case saveForLaterPlaces = "saveForLaterPlaces"
        }
    }
    
    enum BooksFields{
        
        static let id = "id"
        static let title = "title"
        static let genre = "genre"
        static let subtitle = "subtitle"
        static let isbn10 = "isbn10"
        static let shortDescription = "shortDescription"
        static let longDescription = "longDescription"
        static let amazonProductURL = "amazonProductURL"
        static let price = "price"
        static let contributor = "contributor"
        static let author = "author"
        static let category = "category"
        static let publisher = "publisher"
        static let reviewURL = "reviewURL"
        static let googleAPIURL = "googleAPIURL"
        static let publishedDate = "publishedDate"
        static let pageCount = "pageCount"
        static let avgGoogleRating = "avgGoogleRating"
        static let bookCoverImages = "bookCoverImages"
        static let language = "language"
        static let previewURL = "previewURL"
        static let webReaderURL = "webReaderURL"
        static let reviews = "reviews"
        
    }
    
    enum PlaceFields {
        static let businessOwner = "businessOwner"
        static let displayName = "displayName"
        static let name  = "name"
        static let lastReviewDate = "lastReviewDate"
        static let googlePhotoReferences = "googlePhotoReferences"
        static let googlePlaceID  = "googlePlaceID"
        static let coordinates = "coordinate"
        static let phone = "phone"
        static let address = "address"
        static let userID = "userID"
        static let googleRating = "googleRating"
        static let website = "website"
        static let categories = "categories"
        static let numberOfReviews = "numberOfReviews"
        static let reviewsAVG = "reviewsAVG"
        static let reviews = "reviews"
    }
    
    enum BusinessMapping {
        static let businessID = "businessID"
        static let placesIDs = "placesIDs"
    }
    
    enum FieldType {
        case literal
        case numeral
        case datetime
        case invalid
    }
    
    enum ReviewFields: String {
        case title = "title"
        case firebaseId = "firebaseId"
        case when = "when"
        case images = "images"
        case address = "address"
        case archived = "archived"
        case location = "location"
        case locationName = "locationName"
        case placeID = "placeID"
        case place = "place"
        case server = "server"
        case service = "service"
        case reasonOfVisit = "reasonOfVisit"
        case companion = "companion"
        case comments = "comments"
        case rating = "rating"
        case type = "type"
        case category = "category"
        case waiting = "waiting"
        case userID = "userID"
        case userDisplayName = "userDisplayName"
        case imagesDownloadURLs = "downloadURLs"
        case businessDisplayName = "businessDisplayName"
        case userProfilePictureURL = "userProfilePictureURL"
    }
    
    enum Flight{
        
        static let flightSearchParam = "flightSearchParam"
        static let price = "price"
        static let outbound = "outbound"
        static let inbound = "inbound"
        
        enum Record{
            static let duration = "duration"
            static let departsAt = "departsAt"
            static let arrivesAt = "arrivesAt"
            static let originAirport = "originAirport"
            static let destinationAirpot = "destinationAirpot"
            static let flightNumber = "flightNumber"
        }
        
        enum SearchParams{
            static let type = "type"
            static let originAirportIATACode = "originAirportIATACode"
            static let originCityAndCountry = "originCityAndCountry"
            static let destinationAirportIATACode = "destinationAirportIATACode"
            static let destinationCityAndCountry = "destinationCityAndCountry"
            static let departureDate = "departureDate"
            static let returnDate = "returnDate"
            static let travelClass = "travelClass"
            static let adults = "adults"
        }
    }
    
    enum EndPoints {
        static let storageEndPoint = "gs://clippicreview.appspot.com"
        static let usersRoot = "users"
        static let booksRoot = "books"
        static let reviewRoot = "reviews"
        static let placesRoot = "places"
        static let businessMapping = "businessMapping"
        static let imagesRoot = "reviews_images"
        static let profileImages = "profile_images"
    }
}
