//
//  CPRPlace.swift
//  cpr
//
//  Created by Rudy E Matos on 12/10/17.
//  Copyright © 2017 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import GooglePlaces

class CPRPlace {

    var name: String
    var googlePlaceID: String
    var coordinates: GeoPoint
    var phone: String?
    var address: String?
    var displayName: String
    var googleRating: Float
    var website: String?
    var lastReviewDate: Date?
    var categories: [String]
    var googlePhotoReferences: [String]?
    var businessOwner: String
    
    init(placeName: String) {
        self.name = placeName
        self.googlePlaceID = ""
        self.coordinates = GeoPoint(latitude: 0, longitude: 0)
        self.displayName = ""
        self.googleRating = 0
        self.categories = [String]()
        self.businessOwner = ""
    }
    
    init(googlePlaceResult: GooglePlacesResult, category: MainCategory){
        self.name = googlePlaceResult.name
        self.googlePlaceID = googlePlaceResult.placeID
        self.coordinates = GeoPoint(latitude: googlePlaceResult.geometry.location.lat, longitude: googlePlaceResult.geometry.location.lng)
        self.phone = ""
        self.address = googlePlaceResult.vicinity
        self.googleRating = Float(googlePlaceResult.rating)
        self.website = ""
        if let photoReference = googlePlaceResult.photos?.first?.photoReference{
            self.googlePhotoReferences = [photoReference]
        }
        self.categories = googlePlaceResult.types
        self.businessOwner = ""
        self.displayName = ""
    }
    
    init(place: GMSPlace) {
        self.name = place.name
        self.googlePlaceID = place.placeID
        self.coordinates = GeoPoint(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        self.phone = place.phoneNumber ?? ""
        self.address = place.formattedAddress ?? ""
        self.googleRating = place.rating
        self.website = place.website?.absoluteString ?? ""
        self.categories = place.types
        self.businessOwner = ""
        self.displayName = ""
    }
    
    init?(data: [String: Any]?) {
        guard let data = data, let name = data[CPRConstants.PlaceFields.name] as? String, let googlePlaceID = data[CPRConstants.PlaceFields.googlePlaceID] as? String, let coordinates =  data[CPRConstants.PlaceFields.coordinates] as? GeoPoint, let phone = data[CPRConstants.PlaceFields.phone] as? String, let address = data[CPRConstants.PlaceFields.address] as? String, let googleRating  = data[CPRConstants.PlaceFields.googleRating] as? Float, let website = data[CPRConstants.PlaceFields.website] as? String, let categories = data[CPRConstants.PlaceFields.categories] as? [String], let businessOwner = data[CPRConstants.PlaceFields.businessOwner] as? String, let displayName = data[CPRConstants.PlaceFields.displayName] as? String else {
            return nil
        }
        
        self.businessOwner = businessOwner
        self.name = name
        self.googlePlaceID = googlePlaceID
        self.coordinates = coordinates
        self.phone = phone
        self.address = address
        self.googleRating = googleRating
        self.website = website
        self.categories = categories
        self.displayName = displayName
        self.googlePhotoReferences = data[CPRConstants.PlaceFields.googlePhotoReferences] as? [String]
        self.lastReviewDate = (data[CPRConstants.PlaceFields.lastReviewDate] as? Timestamp)?.dateValue()
    }

    init(name: String, googlePlaceID: String, coordinates: GeoPoint, phone: String, address: String, googleRating: Float, website: String, categories: [String], userID: String, businessOwner: String, displayName: String) {
        self.name = name
        self.googlePlaceID = googlePlaceID
        self.coordinates = coordinates
        self.phone = phone
        self.address = address
        self.googleRating = googleRating
        self.website = website
        self.categories = categories
        self.businessOwner = businessOwner
        self.displayName = displayName
    }
    
    func updateInfo(googleObjectResult: GooglePlaceInfoResult){
        let photosReferences = googleObjectResult.photos.compactMap({$0.photoReference})
        googlePhotoReferences = photosReferences
        phone = googleObjectResult.formattedPhoneNumber
        website = googleObjectResult.website
    }
    
    func parse() -> [String: Any] {
        return [
            CPRConstants.PlaceFields.address: address ?? "",
            CPRConstants.PlaceFields.categories: categories,
            CPRConstants.PlaceFields.coordinates: coordinates,
            CPRConstants.PlaceFields.googlePlaceID: googlePlaceID,
            CPRConstants.PlaceFields.googleRating: googleRating,
            CPRConstants.PlaceFields.displayName: displayName,
            CPRConstants.PlaceFields.name: name,
            CPRConstants.PlaceFields.phone: phone ?? "",
            CPRConstants.PlaceFields.lastReviewDate : lastReviewDate ?? Date(),
            CPRConstants.PlaceFields.googlePhotoReferences : googlePhotoReferences ?? [],
            CPRConstants.PlaceFields.businessOwner: businessOwner,
            CPRConstants.PlaceFields.website: website ?? ""
        ]
    }
}

extension CPRPlace: OptionSelectorConverter{
    
    func shouldGetAdditionalInformation() -> Bool{
        return googlePhotoReferences == nil || googlePhotoReferences?.count == 0 || phone == nil || website == nil
    }
    
    func convertToOption() -> OptionSelectorConfiguration.Option{
        var iconURL : String? = nil
        if let photos = googlePhotoReferences , photos.count > 0, let randomPhotoReference = photos.randomElement(){
            iconURL = "\(CPRURLs.GooglePlaces.AdditionalURLS.getPhotoByReferenceURL)?maxwidth=400&photoreference=\(randomPhotoReference)&key=\(CPRURLs.GooglePlaces.key)"
        }
        return OptionSelectorConfiguration.Option(icon: iconURL, name : name, description: address ?? "Address Not Available", object: self)
    }
}



extension CPRPlace: Equatable{
    static func == (lhs: CPRPlace, rhs: CPRPlace) -> Bool {
        return lhs.googlePlaceID == rhs.googlePlaceID && lhs.coordinates == rhs.coordinates && lhs.name == rhs.name
    }
}
