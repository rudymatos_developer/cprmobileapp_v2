//
//  CPRError.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/24/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation

struct CPRError: Error{
    var message: String
    var errorType: ErrorType
}

enum ErrorType {
    case errorUploadindPhoto
    case errorSavingReview
    case errorSavingUserData
    case errorGettingData
    case invalidAccessException
    case invalidUserData
    case missingRequiredData
    case placeAlreadyRegistered
    case errorCreatingMapping
    case errorGettingMapping
    case errorCreatingPlace
    case errorPurchasingItem
    case userIsNotABusinessType
}
