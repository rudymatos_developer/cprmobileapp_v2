//
//  Logoutable.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/21/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol Logoutable:class{
    var logoutCompletion : (()->Void)? {get set}
}
