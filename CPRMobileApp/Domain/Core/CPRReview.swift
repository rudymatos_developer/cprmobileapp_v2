//
//  Review.swift
//  cpr
//
//  Created by Rudy E Matos on 11/18/17.
//  Copyright © 2017 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import GooglePlaces

extension CPRReview: Equatable{
    static func == (lhs: CPRReview, rhs: CPRReview) -> Bool {
        return lhs.firebaseId == rhs.firebaseId && lhs.when == rhs.when
    }
}

class CPRReview {
    
    var title: String? = ""
    var firebaseId: String = ""
    var images: [UIImage?]?
    var imagesNames: [String]?
    var rating: Int = 2
    var waiting: Int = 2
    var when: Date =  Date()
    var location: GeoPoint?
    var locationName: String = ""
    var imagesDownloadURLs : [String]?
    var address: String = ""
    var server: String = ""
    var service: String = ""
    var reasonOfVisit: String = ""
    var companion: String = ""
    var comments: String = ""
    var category: String = ""
    var placeID: String?
    var place : CPRPlace?
    var userID: String = ""
    var userDisplayName: String = ""
    var userProfilePictureURL: String?
    var businessDisplayName: String = ""
    var archived: Bool = false
    
    required init?(data: [String: Any?]) {
        guard  let rating = data[CPRConstants.ReviewFields.rating.rawValue] as? Int,
            let waiting = data[CPRConstants.ReviewFields.waiting.rawValue] as? Int,
            let when = (data[CPRConstants.ReviewFields.when.rawValue] as? Timestamp)?.dateValue(), let locationName = data[CPRConstants.ReviewFields.locationName.rawValue] as? String, let service = data[CPRConstants.ReviewFields.service.rawValue] as? String, let firebaseId = data[CPRConstants.ReviewFields.firebaseId.rawValue] as? String, let userID = data[CPRConstants.ReviewFields.userID.rawValue] as? String,
            let address = data[CPRConstants.ReviewFields.address.rawValue] as? String,
            let server = data[CPRConstants.ReviewFields.server.rawValue] as? String,
            let reasonOfVisit = data[CPRConstants.ReviewFields.reasonOfVisit.rawValue] as? String,
            let companion = data[CPRConstants.ReviewFields.companion.rawValue] as? String,
            let comments = data[CPRConstants.ReviewFields.comments.rawValue] as? String,
            let category = data[CPRConstants.ReviewFields.category.rawValue] as? String
            else {
                return nil
        }
        self.firebaseId = firebaseId
        self.waiting = waiting
        self.rating = rating
        self.when = when
        self.locationName = locationName
        self.archived = data[CPRConstants.ReviewFields.archived.rawValue] as? Bool ?? false
        self.service = service
        self.userID = userID
        self.location = data[CPRConstants.ReviewFields.location.rawValue] as? GeoPoint
        self.address = address
        self.server = server
        self.reasonOfVisit = reasonOfVisit
        self.companion = companion
        self.comments = comments
        self.title = data[CPRConstants.ReviewFields.title.rawValue] as? String ?? ""
        self.category = category
        self.place = CPRPlace(data: data[CPRConstants.ReviewFields.place.rawValue] as? [String:Any])
        self.userProfilePictureURL = data[CPRConstants.ReviewFields.userProfilePictureURL.rawValue] as? String
        self.userDisplayName = data[CPRConstants.ReviewFields.userDisplayName.rawValue] as? String ?? "NO USERNAME"
        self.businessDisplayName = data[CPRConstants.ReviewFields.businessDisplayName.rawValue] as? String ?? "NO BUSINESS NAME"
        self.imagesNames = data[CPRConstants.ReviewFields.images.rawValue] as? [String]
        self.imagesDownloadURLs = data[CPRConstants.ReviewFields.imagesDownloadURLs.rawValue] as? [String]
        self.placeID = data[CPRConstants.ReviewFields.placeID.rawValue] as? String ?? ""
        
    }
    
    required init(){
        
    }
    
    func generateImagesNames(){
        imagesNames = images?.map({_ in generateUUID()}) ?? []
    }
    
    func parse() -> [String: Any] {
        return [
            CPRConstants.ReviewFields.firebaseId.rawValue: firebaseId,
            CPRConstants.ReviewFields.when.rawValue: when,
            CPRConstants.ReviewFields.images.rawValue: imagesNames ?? [],
            CPRConstants.ReviewFields.rating.rawValue: rating,
            CPRConstants.ReviewFields.waiting.rawValue: waiting,
            CPRConstants.ReviewFields.userDisplayName.rawValue: userDisplayName,
            CPRConstants.ReviewFields.userProfilePictureURL.rawValue: userProfilePictureURL ?? "",
            CPRConstants.ReviewFields.archived.rawValue: archived,
            CPRConstants.ReviewFields.title.rawValue: title ?? "",
            CPRConstants.ReviewFields.place.rawValue: place?.parse() ?? [:],
            CPRConstants.ReviewFields.imagesDownloadURLs.rawValue: imagesDownloadURLs ?? [],
            "creationTime": Date(),
            CPRConstants.ReviewFields.businessDisplayName.rawValue: businessDisplayName,
            CPRConstants.ReviewFields.location.rawValue: location != nil ? location as Any : "",
            CPRConstants.ReviewFields.locationName.rawValue: locationName,
            CPRConstants.ReviewFields.address.rawValue: address,
            CPRConstants.ReviewFields.server.rawValue: server,
            CPRConstants.ReviewFields.service.rawValue: service,
            CPRConstants.ReviewFields.reasonOfVisit.rawValue: reasonOfVisit,
            CPRConstants.ReviewFields.companion.rawValue: companion,
            CPRConstants.ReviewFields.comments.rawValue: comments,
            CPRConstants.ReviewFields.category.rawValue: category,
            CPRConstants.ReviewFields.placeID.rawValue: placeID ?? "",
            CPRConstants.ReviewFields.userID.rawValue: userID
        ]
    }
    
}

extension CPRReview: OptionSelectorConverter{
    func convertToOption() -> OptionSelectorConfiguration.Option{
        return OptionSelectorConfiguration.Option(icon: imagesDownloadURLs?.randomElement(), name : locationName, description: address, object: self)
        
    }
}


extension CPRReview{
    func getShareText(encode: Bool = false) -> String{
        let text = "I just gave \(rating) out of 5 stars to \(locationName) using Click Pic Review Mobile App. To see more details you can go to www.clickpicreview.com. Download CPRMobile on the App Store."
        return encode ? text.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? text : text
    }
}

extension CPRReview: CPRDataFormatter {
    
}
