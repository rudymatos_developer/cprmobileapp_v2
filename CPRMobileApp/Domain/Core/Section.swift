//
//  ShopCategory.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 1/13/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol SectionDisplayable{
    func getIconName() -> String
}

struct Section{
 
    enum Status{
        case valid
        case invalid
    }
    var sectionTitle: OptionTitle?
    var options: [Option]
    var category: Any?
    var status : Status = .invalid
    var priority: Int = 0

    init(){
        self.sectionTitle = nil
        self.options = []
        self.category = nil
        self.status = .invalid
    }
    
    init(sectionTitle: OptionTitle, options: [Option], category: Any?, priority: Int){
        self.sectionTitle = sectionTitle
        self.options = options
        self.category = category
        self.status = .valid
        self.priority = priority
    }
    
    struct OptionTitle{
        var title: String
        var object: Any?
        var optionType: OptionType
    }
    
    struct Option{
        var title: String
        var description: String?
        var imagePath: String?
        var bgColor: UIColor?
        var optionType: OptionType
        var object : Any?
        
        init(title: String, description: String?, imagePath: String?, bgColor: UIColor?, optionType: OptionType, object: Any?){
            self.title = title
            self.description = description
            self.imagePath = imagePath
            self.bgColor = bgColor
            self.optionType = optionType
            self.object = object
        }
        
        init(object: Any?){
            self.title = ""
            self.description = ""
            self.imagePath = nil
            self.bgColor = nil
            self.optionType = .withImageTitleAndDescription
            self.object = object
        }
        
    }
    
    enum OptionType{
        case withIcon
        case withImageTitleAndDescription
        case title
    }
    
}

extension Section : OptionConfigurationConverter{
    func getOptionSelectorConfigurator() -> OptionSelectorConfiguration{
        let title = "Looking for Something?"
        let description = "Showing results for \(sectionTitle?.title ?? "Undefined")"
        let icon = (category as? SectionDisplayable)?.getIconName() ?? CPRConstants.logoName
        return OptionSelectorConfiguration(title: title, description: description, icon: icon,category: category)
    }
}
