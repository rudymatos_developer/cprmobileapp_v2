//
//  SocialAccessKeys.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 2/9/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

enum CPRURLs{
    
    enum GooglePlaces{
        static let key = "AIzaSyDSXx43HQgAKZOx3kmOBB6YvlMk5AEaNko"
        static let defaultDistanceInMeters = 20000
        static let placesDetailsFields = "name,rating,formatted_phone_number,photo,url,website"
        
        enum AdditionalURLS{
            static let nearBySearchURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
            static let getPhotoByReferenceURL = "https://maps.googleapis.com/maps/api/place/photo"
            static let getPlaceDetails = "https://maps.googleapis.com/maps/api/place/details/json"
        }
        
    }
    
    enum Unsplash{
        static let requestURL = "https://source.unsplash.com/collection/1877260/400x400"
    }
    
    enum Twitter{
        static let consumerKey = "mljuZvCQq1Va9D687X1WCUETd"
        static let consumerSecret = "6OI7KcQkWzi6SHjoixWoEnsdokncCiOGf36J42aflIFlsYQzdD"
        static let requestTokenUrl = "https://api.twitter.com/oauth/request_token"
        static let authorizeUrl =  "https://api.twitter.com/oauth/authorize"
        static let accessTokenUrl = "https://api.twitter.com/oauth/access_token"
        static let callbackURL = "https://cpr-callback-functions.herokuapp.com/cpr-callback-functions/callback/twitter"
        static let status = "status"
        
        enum AdditionalURLS{
            static let userShow = "https://api.twitter.com/1.1/users/show.json"
            static let postTweetURL = "https://api.twitter.com/1.1/statuses/update.json"
        }
        
        enum ReturnLoginParams{
            static let accessToken = "access_token"
            static let accessTokenSecret = "access_token_secret"
            static let userIdParam = "user_id"
            static let screenName = "screen_name"
        }
    }
    
    enum Pinterest{
        static let consumerKey =  "5013735724529252097"
        static let consumerSecret = "ec63b794a6529c0c612649cf50c5c9ceaf9348c8043ac9579717fbc4bd758e58"
        static let authorizeUrl = "https://api.pinterest.com/oauth/"
        static let accessTokenUrl = "https://api.pinterest.com/v1/oauth/token"
        static let responseType = "token"
        static let scope = "read_public,read_relationships,write_public,write_relationships"
        static let state = "OAuthTesting"
        static let callback = "https://cpr-callback-functions.herokuapp.com/cpr-callback-functions/callback/pinterest"
        
        enum ReturnLoginParams{
            static let accessToken = "pinterest_access_token"
        }
    }
    
}
