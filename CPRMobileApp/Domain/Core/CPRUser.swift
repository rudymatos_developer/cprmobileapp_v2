//
//  User.swift
//  cpr
//
//  Created by Rudy E Matos on 11/29/17.
//  Copyright Â© 2017 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MapKit

protocol HasUser{
    var currentUser: CPRUser? {get set}
}

enum LoginTypes : String{
    case cpr = "cpr"
    case facebook = "facebook"
    case google = "google"
}

protocol CPRItemType{
    func parse() -> [String:Any?]
}

class CPRUser {
    
    var email: String
    
    var fullName: String{
        return "\(firstName) \(surname)"
    }
    var displayName: String?
    var firstName: String
    var surname: String
    var mobile: String?
    var gender: String?
    var maritalStatus: String?
    var countryCode: String?
    var dateOfBirth: Date?
    var profilePicture: String?
    var profilePictureURL: String?
    var reviews: [CPRReview]?
    var currentLocation : CLLocation?
    
    //Mark:- BUSINESS PROFILE ADDITIONAL FIELDS
    var companyPosition: String?
    var companyDisplayName: String?
    var isBusiness: Bool = false
    var businessPlaces: [CPRPlace]?
    var cprAVGRating: Double = 0
    var googleAVGRating: Double = 0
    var loggedInUsing: LoginTypes = .cpr
    
    //MARK:- FAVORITE PLACES AND SFL PLACES
    var favoritePlaces: [CPRPlace]?
    var saveForLaterPlaces: [CPRPlace]?
    
    init?(data: [String: Any]?) {
        guard let data = data, let email = data[CPRConstants.UserFields.email] as? String,
            let firstName = data[CPRConstants.UserFields.firstName] as? String,
            let surname = data[CPRConstants.UserFields.surname] as? String ,
            let isBusiness = data[CPRConstants.UserFields.isBusiness] as? Bool else {
                return nil
        }
        self.email = email
        self.firstName = firstName
        self.surname = surname
        self.gender = data[CPRConstants.UserFields.gender] as? String ?? ""
        self.maritalStatus = data[CPRConstants.UserFields.maritalStatus] as? String ?? ""
        self.displayName = data[CPRConstants.UserFields.displayName] as? String ?? ""
        self.mobile = data[CPRConstants.UserFields.mobile] as? String ?? ""
        self.countryCode = data[CPRConstants.UserFields.countryCode] as? String ?? ""
        self.dateOfBirth = (data[CPRConstants.UserFields.dateOfBirth] as? Timestamp)?.dateValue() ?? nil
        self.profilePicture = data[CPRConstants.UserFields.profilePicture] as? String ?? nil
        self.profilePictureURL = data[CPRConstants.UserFields.profilePictureURL] as? String ?? nil
        self.isBusiness = data[CPRConstants.UserFields.isBusiness] as? Bool ?? false
        self.loggedInUsing = LoginTypes(rawValue: data[CPRConstants.UserFields.loginType] as? String ?? "INVALID_DATA") ?? .cpr
        
        if isBusiness {
            self.googleAVGRating = data[CPRConstants.UserFields.googleAVGRating] as? Double ?? 0
            self.cprAVGRating = data[CPRConstants.UserFields.crpAVGRAting] as? Double ?? 0
            self.companyPosition = data[CPRConstants.UserFields.companyPosition] as? String ?? ""
            self.companyDisplayName = data[CPRConstants.UserFields.companyDisplayName] as? String ?? ""
        }
    }
    
    init(userCreator: Registrator) {
        self.email = userCreator.email ?? ""
        self.firstName = userCreator.firstName ?? ""
        self.surname = userCreator.surname ?? ""
    }
    
    func parse() -> [String: Any] {
        return [
            CPRConstants.UserFields.email: email,
            CPRConstants.UserFields.firstName: firstName,
            CPRConstants.UserFields.surname: surname,
            CPRConstants.UserFields.fullName: fullName,
            CPRConstants.UserFields.mobile: mobile ?? "",
            CPRConstants.UserFields.gender: gender ?? "",
            CPRConstants.UserFields.maritalStatus: maritalStatus ?? "",
            CPRConstants.UserFields.loginType : loggedInUsing.rawValue,
            CPRConstants.UserFields.countryCode: countryCode ?? "",
            CPRConstants.UserFields.dateOfBirth: dateOfBirth ?? "",
            CPRConstants.UserFields.displayName: displayName ?? "",
            CPRConstants.UserFields.profilePicture: profilePicture ?? "",
            CPRConstants.UserFields.companyPosition: companyPosition ?? "",
            CPRConstants.UserFields.companyDisplayName: companyDisplayName ?? "",
            CPRConstants.UserFields.isBusiness: isBusiness
        ]
    }
    
}
