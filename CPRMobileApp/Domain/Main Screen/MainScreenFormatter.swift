//
//  MainScreenFormatter.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/12/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

protocol MainScreenFormatter{
    func getName() -> String
    func getDescription() -> String
    func getRandomImage() -> String?
}

extension CPRReview: MainScreenFormatter{
    func getRandomImage() -> String? {
        return imagesDownloadURLs?.randomElement()
    }
    
    func getName() -> String {
        return locationName
    }
    
    func getDescription() -> String {
        return address
    }
    
}


extension CPRPlace: MainScreenFormatter{
    func getName() -> String {
        return name
    }
    
    func getDescription() -> String {
        return address ?? ""
    }
    
    func getRandomImage() -> String? {
        return googlePhotoReferences?.randomElement()
    }
}
