//
//  MainCategories.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/12/19.
//  Copyright © 2019 Bearded Gentleman. All rights reserved.
//

import Foundation

enum MainCategory: String,CaseIterable{
    
    case restaurants = "restaurants"
    case salons = "salons"
    case bars = "bars"
    case cafes = "cafes"
    case hotels = "hotels"
    case myFavoritePlaces = "favorite"
    case saveForLaterPlaces = "sfl"
    case myReviews = "my_reviews"
    
    func getCategoryName() -> String{
        switch self{
        case .restaurants, .salons, .bars, .cafes:
            return "categories_\(self.rawValue)"
        default:
            return self.rawValue
        }
    }
    
    func isCategorized() -> Bool{
        return self == .restaurants || self == .salons || self == .cafes || self == .bars || self == .hotels
    }
    
    func getPriority() -> Int{
        switch self{
        case .restaurants:
            return 2
        case .salons:
            return 3
        case .bars:
            return 0
        case .cafes:
            return 1
        case .hotels:
            return 4
        case .myFavoritePlaces:
            return 5
        case .saveForLaterPlaces:
            return 6
        case .myReviews:
            return 7
        }
        
    }
    
    func getGooglePlacesName() -> String{
        switch self{
        case .restaurants:
            return "restaurant"
        case .salons:
            return "hair_care"
        case .bars:
            return "bar"
        case .cafes:
            return "cafe"
        case .hotels:
            return "lodging"
        default:
            return ""
        }
    }
    
    func getDisplayName() -> String{
        switch self{
        case .restaurants:
            return "Restaurants"
        case .salons:
            return "Salons and Spas"
        case .bars:
            return "Bars and Taverns"
        case .cafes:
            return "Coffee Shops"
        case .hotels:
            return "Hotels"
        case .myFavoritePlaces:
            return "My Favorite Places"
        case .saveForLaterPlaces:
            return "Places I'll go Later"
        case .myReviews:
            return "My Reviews"
        }
    }
    
}

extension  MainCategory: SectionDisplayable{
    func getIconName() -> String {
        switch self{
        case .restaurants:
            return "main_restaurant"
        case .salons:
            return "main_spa"
        case .bars:
            return "main_bar"
        case .hotels:
            return "main_hotel"
        case .cafes:
            return "main_coffee"
        case .myFavoritePlaces:
            return "main_favorite"
        case .saveForLaterPlaces:
            return "main_sfl"
        case .myReviews:
            return "main_myreviews"
        }
    }
}

extension MainCategory : OptionConfigurationConverter{
    func getOptionSelectorConfigurator() -> OptionSelectorConfiguration{
        let title = "Looking for places near by?"
        let description = "Showing results for \(getDisplayName())"
        let icon = getIconName()
        return OptionSelectorConfiguration(title: title, description: description, icon: icon,category: self)
    }
}
