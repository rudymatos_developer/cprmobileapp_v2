// To parse the JSON, add this file to your project and do:
//
//   let placesCategories = try? newJSONDecoder().decode(PlacesCategories.self, from: jsonData)

import Foundation

typealias PlacesCategories = [PlacesCategory]

struct PlacesCategory: Codable {
    let category: String
    let parent: String
}
