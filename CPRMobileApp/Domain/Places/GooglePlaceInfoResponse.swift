
import Foundation

struct GooglePlaceInfoResponse: Codable {
    let result: GooglePlaceInfoResult
    let status: String
    
    enum CodingKeys: String, CodingKey {
        case result, status
    }
    
}

struct GooglePlaceInfoResult: Codable {
    let formattedPhoneNumber, name: String
    let photos: [GooglePlaceInfoReference]
    let rating: Double
    let url: String?
    let website: String?
    
    enum CodingKeys: String, CodingKey {
        case formattedPhoneNumber = "formatted_phone_number"
        case name, photos, rating, url, website
    }
}

struct GooglePlaceInfoReference: Codable {
    let height: Int
    let htmlAttributions: [String]
    let photoReference: String
    let width: Int
    
    enum CodingKeys: String, CodingKey {
        case height
        case htmlAttributions = "html_attributions"
        case photoReference = "photo_reference"
        case width
    }
}
